UniTech
========================

**Description**: PIDev Symfony.

Requirements
--------------

Commands:

  * composer update --ignore-platform-reqs
  * php bin/console doctrine:schema:update --force
  * php bin/console fos:user:promote username ROLE_**
