/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Entities.User;
import Utils.UserUniTech;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Saief
 */
public class ServiceLogin {

    public boolean existe(String json) {
        boolean existee = false;
        try {
            JSONParser j = new JSONParser();

            Map<String, Object> tasks = j.parseJSON(new CharArrayReader(json.toCharArray()));
            if (tasks != null && tasks.size() > 0) {
                existee = true;
                User u = new User();
                int id = (int) ((double) tasks.get("id"));
                u.setId_user(id);
                int cin = (int) ((double) tasks.get("cIN"));
                u.setCIN(cin);
                u.setFirst_Name(tasks.get("firstName") + "");
                u.setLast_Name(tasks.get("lastName") + "");
                u.setEmail(tasks.get("email") + "");
                UserUniTech.userConnecte = u;
            }

        } catch (IOException ex) {
        }

        System.out.println(existee);
        return existee;

    }

    static boolean ext;

    public boolean existance(String mail, String pwd) {
        ConnectionRequest con = new ConnectionRequest();

        con.setUrl(UserUniTech.baseURL + "/userlogin/" + mail + "/" + pwd + "");
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                ServiceLogin ser = new ServiceLogin();
                ext = ser.existe(new String(con.getResponseData()));
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return ext;
    }

    public void addUser(User r) {
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl(UserUniTech.baseURL + "/add/user");
        con.setPost(true);
        con.addArgument("username", "" + r.getUser_Name());
        con.addArgument("email", "" + r.getEmail());
        con.addArgument("password", "" + r.getPassword());
        con.addArgument("first_name", "" + r.getFirst_Name());
        con.addArgument("last_name", "" + r.getLast_Name());
        con.addArgument("phone_number", "" + r.getPhone_number());
        con.addArgument("CIN", "" + r.getCIN());
        con.addArgument("roles", "" + r.getRole());
        con.addResponseListener((evt) -> {
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }
}
