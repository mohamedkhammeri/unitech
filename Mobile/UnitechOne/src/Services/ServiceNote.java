/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Entities.Classe;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import Entities.Matiere;
import Entities.Note;
import Entities.User;
import Utils.UserUniTech;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author nada kd
 */
public class ServiceNote {

    int k = 0;
    String m;
    public static ServiceNote instance = null;

    public static ServiceNote getInstance() {
        if (instance == null) {
            instance = new ServiceNote();
        }
        return instance;
    }

    /* public void addNote(Note n) {
        
            ConnectionRequest con = new ConnectionRequest();  // création d'une nouvelle demande de connexion
            String Url ="http://localhost:80/unitech/web/app_dev.php/note/ajoutermobile?note_cc="+ n.getNote_cc()+ "&note_ds=" + n.getNote_ds()+ "&note_exam=" + n.getNote_exam() +"&user="+ n.getId_user();// création de l'URL
            con.setUrl(Url);// Insertion de l'URL de notre demande de connexion     
            NetworkManager.getInstance().addToQueueAndWait(con);// Ajout de notre demande de connexion à la file d'attente du NetworkManager
            String data = new String(con.getResponseData());
            JSONParser j = new JSONParser(); } 
            /*khater ma3andoush id =12345
            le rithom ili fil combo hedhoukom cin mahomch na3ref ama enty dakhel fih aala asses id ey voila hedhika l8alta lmochkla linna nitsawr fil add 5atr ena nzid fili wost combo
        try {
            System.out.println(j.parseJSON(new CharArrayReader(data.toCharArray())));
        } catch (IOException ex) {
        
        }
     */
    public void addNote(Note n) {
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl(UserUniTech.baseURL + "/ajoutermobile");
        con.setPost(true);
        // con.addArgument("user", "" + UserUniTech.userConnecte.getId_user());
        con.addArgument("note_cc", "" + n.getNote_cc());
        con.addArgument("note_ds", "" + n.getNote_ds());
        con.addArgument("note_exam", "" + n.getNote_exam());
        //con.addArgument("user", "" + UserUniTech.userConnecte.getId_user());
        //  con.addArgument("user", "" + 1);
        //  con.addArgument("matiere", ""+ n.getMat().getId_matiere());
        con.addArgument("user", "" + n.getUser().getId_user());
        con.addArgument("classe", "" + n.getClasse().getId_classe());
        con.addArgument("mat", "" + n.getMat().getId_mat());
        con.addResponseListener((evt) -> {
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }

    public void updateNote(Note n) {
        ConnectionRequest connReq = new ConnectionRequest();
        //String Url = RESTAURANT_API+"update";
        connReq.setPost(true);
        connReq.setUrl(UserUniTech.baseURL + "/updatemobile/" + n.getId_note());
        connReq.setHttpMethod("POST");
        //connReq.addArgument("id",String.valueOf(n.getId_note()));
        connReq.addArgument("note_cc", String.valueOf(n.getNote_cc()));
        connReq.addArgument("note_ds", String.valueOf(n.getNote_ds()));
        connReq.addArgument("note_exam", String.valueOf(n.getNote_exam()));

        NetworkManager.getInstance().addToQueueAndWait(connReq);
    }

    /*
    public void updateNote(Note n) {  
            ConnectionRequest con = new ConnectionRequest();// création d'une nouvelle demande de connexion
        System.out.println("aaa"+n.getId_note());
            con.setUrl("http://localhost:80/unitech/web/app_dev.php/note/updatemobile/"+n.getId_note()+"&note_cc="+n.getNote_cc()+"&note_ds="+n.getNote_ds()+"&note_exam="+n.getNote_exam());// Insertion de l'URL de notre demande de connexion     
            NetworkManager.getInstance().addToQueueAndWait(con);// Ajout de notre demande de connexion à la file d'attente du NetworkManager
               System.out.println("added Succefully");
            String data = new String(con.getResponseData()); 
            JSONParser j = new JSONParser();
        try {
            System.out.println(j.parseJSON(new CharArrayReader(data.toCharArray())));
            
        } catch (IOException ex) { 
        
        }
    }
     */
    public ArrayList<Note> Note;

    public ArrayList<Note> parseTasks(String jsonText) {
        try {
            Note = new ArrayList<>();
            JSONParser j = new JSONParser();
            Map<String, Object> NoteListJson = j.parseJSON(new CharArrayReader(jsonText.toCharArray()));
            List<Map<String, Object>> list2 = (List<Map<String, Object>>) NoteListJson.get("User");
            List<Map<String, Object>> list = (List<Map<String, Object>>) NoteListJson.get("root");
            for (Map<String, Object> obj : list) {
                Note t = new Note();
                float id = Float.parseFloat(obj.get("id").toString());

                t.setId_note((int) id);

                t.setNote_cc(((int) Float.parseFloat(obj.get("note_cc").toString())));
                t.setNote_ds(Float.parseFloat(obj.get("note_ds").toString()));
                t.setNote_exam(Float.parseFloat(obj.get("note_exam").toString()));
                t.setMoyenne(Float.parseFloat(obj.get("moyenne").toString()));
                Map<String, Object> user = (Map) obj.get("User");
                t.setNom((String) user.get("nom"));
                t.setPrenom((String) user.get("prenom"));
                t.setCin(((int) Float.parseFloat(user.get("CIN").toString())));
                Map<String, Object> matiere = (Map) obj.get("Matiere");
                t.setNom_matiere((String) matiere.get("matiere"));
                t.setCoef(((int) Float.parseFloat(matiere.get("coef").toString())));
                Map<String, Object> classe = (Map) obj.get("Classe");
                t.setId_classe(((int) Float.parseFloat(classe.get("classe").toString())));

                Note.add(t);

            }
        } catch (IOException ex) {

        }
        return Note;
    }

    public ArrayList<Note> getAllNote() {
        ConnectionRequest con = new ConnectionRequest();// création d'une nouvelle demande de connexion
        String Url = UserUniTech.baseURL + "/affiche";// création de l'URL
        con.setUrl(Url);
        con.setPost(false);
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                Note = parseTasks(new String(con.getResponseData()));
                con.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return Note;
    }

    public int getIdNote(int cin) {
        int b = 0;
        ConnectionRequest con = new ConnectionRequest();// création d'une nouvelle demande de connexion
        String Url = UserUniTech.baseURL + "/TakeId/" + cin + "/";// création de l'URL
        con.setUrl(Url);
        con.setPost(false);
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                k = parseTaskId(new String(con.getResponseData()));

                con.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return k;

    }

    public int parseTaskId(String jsonText) {
        int a = 0;
        try {
            user = new ArrayList<>();
            JSONParser j = new JSONParser();
            Map<String, Object> NoteListJson = j.parseJSON(new CharArrayReader(jsonText.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) NoteListJson.get("root");
            for (Map<String, Object> obj : list) {
                User t = new User();
                float id = Float.parseFloat(obj.get("id").toString());
                a = (int) id;

                System.out.println("parse" + a);

            }
        } catch (IOException ex) {

        }
        return a;
    }

    public int getIdclasse(int num) {
        int b = 0;
        ConnectionRequest con = new ConnectionRequest();// création d'une nouvelle demande de connexion
        String Url = UserUniTech.baseURL + "/Takeclasse/" + num + "/";// création de l'URL
        con.setUrl(Url);
        con.setPost(false);
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                k = parseTaskclasse(new String(con.getResponseData()));

                con.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return k;

    }

    public int parseTaskclasse(String jsonText) {
        int a = 0;
        try {
            classe = new ArrayList<>();
            JSONParser j = new JSONParser();
            Map<String, Object> NoteListJson = j.parseJSON(new CharArrayReader(jsonText.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) NoteListJson.get("root");
            for (Map<String, Object> obj : list) {
                Classe t = new Classe();
                float id = Float.parseFloat(obj.get("id").toString());
                a = (int) id;

                System.out.println("parse" + a);

            }
        } catch (IOException ex) {

        }
        return a;
    }

    public ArrayList<User> user;

    public ArrayList<User> parseTasksuser(String jsonText) {
        try {
            user = new ArrayList<>();
            JSONParser j = new JSONParser();
            Map<String, Object> NoteListJson = j.parseJSON(new CharArrayReader(jsonText.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) NoteListJson.get("root");
            for (Map<String, Object> obj : list) {
                User t = new User();
                float id = Float.parseFloat(obj.get("id").toString());

                t.setId_user((int) id);

                float cin = Float.parseFloat(obj.get("cin").toString());

                t.setCIN((int) cin);

                user.add(t);

            }
        } catch (IOException ex) {

        }
        return user;
    }

    public ArrayList<User> getAllUser() {
        ConnectionRequest con = new ConnectionRequest();// création d'une nouvelle demande de connexion
        String Url = UserUniTech.baseURL + "/affichecin";// création de l'URL
        con.setUrl(Url);
        con.setPost(false);
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                user = parseTasksuser(new String(con.getResponseData()));
                con.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return user;
    }

    public ArrayList<Matiere> mat;

    public ArrayList<Matiere> parseTasksmatiere(String jsonText) {
        try {
            mat = new ArrayList<>();
            JSONParser j = new JSONParser();
            Map<String, Object> NoteListJson = j.parseJSON(new CharArrayReader(jsonText.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) NoteListJson.get("root");
            for (Map<String, Object> obj : list) {
                Matiere t = new Matiere();
                float id = Float.parseFloat(obj.get("id").toString());
                t.setId_mat((int) id);
                t.setNom_mat((String) obj.get("matiere"));

                mat.add(t);

            }
        } catch (IOException ex) {

        }
        return mat;
    }

    public ArrayList<Matiere> getAllmatiere() {
        ConnectionRequest con = new ConnectionRequest();// création d'une nouvelle demande de connexion
        String Url = UserUniTech.baseURL + "/affichematiere";// création de l'URL
        con.setUrl(Url);
        con.setPost(false);
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                mat = parseTasksmatiere(new String(con.getResponseData()));
                con.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return mat;
    }

    public ArrayList<Classe> classe;

    public ArrayList<Classe> parseTaskscl(String jsonText) {
        try {
            classe = new ArrayList<>();
            JSONParser j = new JSONParser();
            Map<String, Object> NoteListJson = j.parseJSON(new CharArrayReader(jsonText.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) NoteListJson.get("root");
            for (Map<String, Object> obj : list) {
                Classe t = new Classe();
                float id = Float.parseFloat(obj.get("id").toString());
                t.setId_classe((int) id);

                float num = Float.parseFloat(obj.get("classe").toString());
                t.setNum_classe((int) num);

                classe.add(t);

            }
        } catch (IOException ex) {

        }
        return classe;
    }

    public ArrayList<Classe> getAllclasse() {
        ConnectionRequest con = new ConnectionRequest();// création d'une nouvelle demande de connexion
        String Url = UserUniTech.baseURL + "/afficheclasse";// création de l'URL
        con.setUrl(Url);
        con.setPost(false);
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                classe = parseTaskscl(new String(con.getResponseData()));
                con.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return classe;
    }

    public static List<Note> recherche(String jsonText) {
        ArrayList<Note> Note = new ArrayList<>();
        ConnectionRequest con = new ConnectionRequest();
        String Url = UserUniTech.baseURL + "/recherche";
        con.setUrl(Url);
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                //listTasks = getListTask(new String(con.getResponseData()));
                JSONParser jsonp = new JSONParser();
                try {
                    JSONParser j = new JSONParser();
                    Map<String, Object> NoteListJson = j.parseJSON(new CharArrayReader(jsonText.toCharArray()));
                    List<Map<String, Object>> list2 = (List<Map<String, Object>>) NoteListJson.get("User");
                    List<Map<String, Object>> list = (List<Map<String, Object>>) NoteListJson.get("root");
                    for (Map<String, Object> obj : list) {
                        Note t = new Note();
                        float id = Float.parseFloat(obj.get("id").toString());
                        t.setId_note((int) id);
                        t.setNote_cc(((int) Float.parseFloat(obj.get("note_cc").toString())));
                        t.setNote_ds(Float.parseFloat(obj.get("note_ds").toString()));
                        t.setNote_exam(Float.parseFloat(obj.get("note_exam").toString()));
                        t.setMoyenne(Float.parseFloat(obj.get("moyenne").toString()));
                        Map<String, Object> user = (Map) obj.get("User");
                        t.setNom((String) user.get("nom"));
                        t.setPrenom((String) user.get("prenom"));
                        Map<String, Object> matiere = (Map) obj.get("Matiere");
                        t.setNom_matiere((String) matiere.get("matiere"));
                        Map<String, Object> classe = (Map) obj.get("Classe");
                        t.setId_classe(((int) Float.parseFloat(classe.get("classe").toString())));

                        Note.add(t);

                    }
                } catch (IOException ex) {
                }

            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return Note;
    }

    public int getIdMatiere(String nom_mat) {

        ConnectionRequest con = new ConnectionRequest();// création d'une nouvelle demande de connexion
        String Url = UserUniTech.baseURL + "/Takematiere/" + "'" + nom_mat + "'/";// création de l'URL
        con.setUrl(Url);
        con.setPost(false);
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                k = parseTaskIdm(new String(con.getResponseData()));
                con.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return k;

    }

    public int parseTaskIdm(String jsonText) {
        int a = 0;

        try {
            mat = new ArrayList<>();
            JSONParser j = new JSONParser();
            Map<String, Object> NoteListJson = j.parseJSON(new CharArrayReader(jsonText.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) NoteListJson.get("root");
            for (Map<String, Object> obj : list) {
                float id = Float.parseFloat(obj.get("id").toString());
                a = (int) id;

                System.out.println("parse" + a);

            }
        } catch (IOException ex) {

        }
        return a;
    }

}
