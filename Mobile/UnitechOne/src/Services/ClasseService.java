/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Entities.Classe;
import Entities.Etudiant;
import Utils.UserUniTech;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author admin
 */
public class ClasseService {

    private UserService userService;
    private ConnectionRequest con;

    public ClasseService() {
        con = new ConnectionRequest();
        userService = new UserService();
    }

    public List<Classe> parseListClasseJson() {
        List<Classe> classes = new ArrayList<>();
        con.setUrl(UserUniTech.baseURL + "/ttclasses");
        con.setPost(false);
        con.addResponseListener((evt) -> {
            try {
                JSONParser j = new JSONParser();

                Map<String, Object> ClasseJSON = j.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                List<Map<String, Object>> list = (List<Map<String, Object>>) ClasseJSON.get("root");
                List<Map<String, Object>> list1 = (List<Map<String, Object>>) ClasseJSON.get("classe");
                String username = (String) ClasseJSON.get("classe");

                for (Map<String, Object> obj : list) {
                    Classe classe;

                    classe = new Classe();

                    int id = (int) ((double) obj.get("id"));
                    classe.setId_classe(id);

                    int num = (int) ((double) obj.get("num"));
                    classe.setNum_classe(num);
                    //Map<String, Object> user = (Map) obj.get("classe");
                    String type_classe = (String) (obj.get("type"));
                    classe.setType_classe(type_classe);

                    int niv = (int) ((double) obj.get("niveau"));
                    classe.setNiveau(niv);
                    int cap = (int) ((double) obj.get("capacite"));
                    classe.setCapacité(cap);

                    classes.add(classe);
                }
            } catch (IOException ex) {
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return classes;
    }

    public List<Etudiant> parseListEtudJson(Classe c) {
        List<Etudiant> etudiants = new ArrayList<>();
        con.setUrl(UserUniTech.baseURL + "/ttet/" + c.getId_classe());
        con.setPost(false);
        con.addResponseListener((evt) -> {
            try {
                JSONParser j = new JSONParser();
                Map<String, Object> EtudJSON = j.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                List<Map<String, Object>> list = (List<Map<String, Object>>) EtudJSON.get("root");
                List<Map<String, Object>> list1 = (List<Map<String, Object>>) EtudJSON.get("etudiant");
                String username = (String) EtudJSON.get("etudiant");

                for (Map<String, Object> obj : list) {
                    Etudiant etudiant;

                    etudiant = new Etudiant();
                    String user_Name = (String) (obj.get("username"));
                    etudiant.setUser_Name(user_Name);

                    String first_Name = (String) (obj.get("nom"));
                    etudiant.setFirst_Name(first_Name);

                    String last_Name = (String) (obj.get("prenom"));
                    etudiant.setLast_Name(last_Name);

                    etudiants.add(etudiant);
                    System.err.println("loulaaaa" + etudiant.getUser_Name());
                }
            } catch (IOException ex) {
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return etudiants;
    }

    /*
     public ArrayList<Classe> parseListClasseJson(String json) {

        ArrayList<Classe>  listClasse = new ArrayList<>();

   try {
            JSONParser j = new JSONParser();// Instanciation d'un objet JSONParsr permettant le parsing du résultat jsone

          Map<String, Object>  Classes = j.parseJSON(new CharArrayReader(json.toCharArray()));
           System.out.println(Classes);            
            
       List<Map<String, Object>> list = (List<Map<String, Object>>) Classes.get("root");
       
       

       for (Map<String, Object> o : list){
           Classe cl = new Classe();
           
           cl.setId_classe((Integer.parseInt(o.get("id").toString())));
         cl.setNiveau((Integer.parseInt(o.get("niveau").toString())));
           cl.setCapacité((Integer.parseInt(o.get("capacite").toString())));      
        //   question.setDate_question((String)o.get("date"));
          
           
      
        
   
           
           listClasse.add(cl);
       }
       
       
      } catch (IOException ex) {
       }
        
   
        
        return listClasse;

    }
    
    
        ArrayList<Classe>  listClasse = new ArrayList<>();
    
    
    //hedhy la methode callRequest eli fl tuto
   
    public ArrayList<Classe> getListClasse(){       
        ConnectionRequest con = new ConnectionRequest();//Appel au service web (demande de connexion).
   con.setUrl("http://localhost:80/unitech/web/app_dev.php/classe/ttclasses");        

        con.addResponseListener(new ActionListener<NetworkEvent>() {
           
            @Override
            public void actionPerformed(NetworkEvent evt) {
                ClasseService ser = new ClasseService();
             listClasse = ser.parseListClasseJson(new String(con.getResponseData()));//Récupération de la réponse du serveur
            }

        });

        NetworkManager.getInstance().addToQueueAndWait(con);

        return  listClasse;
    }
     */
}
