/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Entities.Enseignant;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class UserService {

    //Etudiant e = new Etudiant();
    private ConnectionRequest con;
    private JSONParser jSONParser;

    public UserService() {
        con = new ConnectionRequest();
        jSONParser = new JSONParser();
    }

    public List<Enseignant> getAll() {
        List<Enseignant> enseignants = new ArrayList<>();
        con.setUrl("http://localhost:8000/api/enseignants");
        con.setPost(false);
        con.addResponseListener((evt) -> {
            try {
                Map<String, Object> ensJSON = jSONParser.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                List<Map<String, Object>> list = (List<Map<String, Object>>) ensJSON.get("root");
                //System.out.println(list);
                for (Map<String, Object> map : list) {
                    Enseignant enseignant = new Enseignant();
                    enseignant.setFirst_Name(map.get("nom") + "");
                    enseignant.setLast_Name(map.get("prenom") + "");
                    int id = (int) ((double) map.get("id"));
                    enseignant.setId_user(id);
                    enseignants.add(enseignant);
                }
            } catch (IOException ex) {
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return enseignants;
    }

}
