/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Entities.Enseignant;
import Entities.Etudiant;
import Entities.Note;
import Entities.User;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author KattaX
 */
public class NoteService {

    private ConnectionRequest con;
    private JSONParser jSONParser;

    public NoteService() {
        con = new ConnectionRequest();
        jSONParser = new JSONParser();
    }

    public List<Note> getAll(User u) {
        List<Note> notes = new ArrayList<>();
        con.setUrl("http://localhost:8000/api/notes");
        con.setPost(false);
        System.out.println("user: "+u.getId_user());
        con.addArgument("etudiant", "" + u.getId_user());
        con.addResponseListener((evt) -> {
            try {
                Map<String, Object> notesJSON = jSONParser.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                List<Map<String, Object>> list = (List<Map<String, Object>>) notesJSON.get("root");
                //System.out.println(list);
                if (list != null) {
                    for (Map<String, Object> map : list) {
                        Note note = new Note();
                        note.setNom_matiere(map.get("matiere") + "");
                        note.setNote_exam(Float.valueOf(map.get("exam") + ""));
                        note.setNote_cc(Float.valueOf(map.get("cc") + ""));
                        note.setNote_ds(Float.valueOf(map.get("ds") + ""));
                        int id = (int) ((double) map.get("id"));
                        note.setId_note(id);
                        notes.add(note);
                    }
                }
            } catch (IOException ex) {
            }
        });
        con.addExceptionListener((evt) -> {
            if (con.getResponseCode() == 404) {
                System.out.println("not found");
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return notes;
    }

}
