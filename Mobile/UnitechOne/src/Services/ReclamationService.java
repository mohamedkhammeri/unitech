/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Entities.Reclamation;
import Entities.ReclamationNote;
import Entities.ReclamationProf;
import Utils.EtatReclamation;
import Utils.UserUniTech;
import Views.HiddenObj;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author KattaX
 */
public class ReclamationService {

    private UserService userService;
    private ConnectionRequest con;
    private JSONParser jSONParser;

    public ReclamationService() {
        con = new ConnectionRequest();
        userService = new UserService();

        jSONParser = new JSONParser();
    }

    public List<Reclamation> getAll() {
        List<Reclamation> reclamations = new ArrayList<>();
        con.setUrl("http://localhost:8000/api/reclamations");
        con.setPost(false);
        con.addArgument("userCnt", UserUniTech.userConnecte.getId_user() + "");
        con.addResponseListener((evt) -> {
            try {
                JSONParser j = new JSONParser();

                Map<String, Object> reclamationsJSON = j.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                List<Map<String, Object>> list = (List<Map<String, Object>>) reclamationsJSON.get("root");

                for (Map<String, Object> obj : list) {
                    Reclamation reclamation;
                    if (obj.get("type").toString().equalsIgnoreCase("Reclamation Prof")) {
                        reclamation = new ReclamationProf();
                    } else if (obj.get("type").toString().equalsIgnoreCase("Reclamation Note")) {
                        reclamation = new ReclamationNote();
                    } else {
                        reclamation = new Reclamation();
                    }

                    int id = (int) ((double) obj.get("id"));
                    reclamation.setId(id);
                    reclamation.setDesc((String) obj.get("desc"));
                    reclamation.setSujet((String) obj.get("subject"));
                    reclamation.setCodeSuivie(obj.get("codeSuivie").toString());
                    int status = (int) ((double) obj.get("status"));
                    reclamation.setEtat(EtatReclamation.values()[status]);
                    reclamations.add(reclamation);
                }
            } catch (IOException ex) {
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return reclamations;
    }

    public void addReclamation(Reclamation r) {
        con.setUrl("http://localhost:8000/api/reclamation");
        con.setPost(true);
        con.addArgument("user", "" + UserUniTech.userConnecte.getId_user());
        con.addArgument("desc", "" + r.getDesc());
        con.addArgument("sujet", "" + r.getSujet());
        if (r instanceof ReclamationProf) {
            con.addArgument("type", "enseignant");
            ReclamationProf rp = (ReclamationProf) r;
            con.addArgument("prof", rp.getProf().getId_user() + "");
        } else if (r instanceof ReclamationNote) {
            con.addArgument("type", "note");
            ReclamationNote rn = (ReclamationNote) r;
            con.addArgument("note", rn.getNote().getId_note() + "");
            con.addArgument("type_note", rn.getType_note());
        }
        con.addResponseListener((evt) -> {
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }

    public int countReclamationResolu() {
        HiddenObj ho = new HiddenObj(0, true);
        ConnectionRequest con2 = new ConnectionRequest();
        con2.setUrl("http://localhost:8000/api/reclamations/countResolu");
        con2.setPost(false);
        con2.addArgument("userCnt", UserUniTech.userConnecte.getId_user() + "");
        con2.addResponseListener((evt) -> {
            ho.setObject(Integer.valueOf(new String(con2.getResponseData())));
        });
        NetworkManager.getInstance().addToQueueAndWait(con2);
        return (int) ho.getObject();
    }

    private Reclamation reclamationResult = null;

    public Reclamation searchByCode(String codeSuivie) {
        reclamationResult = null;
        con.setUrl("http://localhost:8000/api/reclamation/search/" + codeSuivie);
        con.setPost(false);
        con.addResponseListener((evt) -> {
            try {
                Map<String, Object> reclamationJSON = jSONParser.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));

                if (!reclamationJSON.isEmpty()) {
                    reclamationResult = new Reclamation();
                    int id = (int) ((double) reclamationJSON.get("id"));
                    reclamationResult.setId(id);
                    reclamationResult.setDesc((String) reclamationJSON.get("desc"));
                    reclamationResult.setSujet((String) reclamationJSON.get("subject"));
                    reclamationResult.setCodeSuivie(reclamationJSON.get("codeSuivie").toString());
                    int status = (int) ((double) reclamationJSON.get("status"));
                    reclamationResult.setEtat(EtatReclamation.values()[status]);
                } else {
                    throw new Exception("not found");
                }
            } catch (IOException ex) {
            } catch (Exception ex) {
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return reclamationResult;
    }

    public void deleteReclamation(Reclamation r) {
        ConnectionRequest conDel = new ConnectionRequest();
        conDel.setUrl("http://localhost:8000/api/reclamation");
        conDel.setHttpMethod("DELETE");
        conDel.addArgument("id", "" + r.getId());
        conDel.addResponseListener((evt) -> {
        });
        NetworkManager.getInstance().addToQueueAndWait(conDel);
    }

    public void updateReclamation(Reclamation r) {
        ConnectionRequest conUpdate = new ConnectionRequest();
        conUpdate.setUrl("http://localhost:8000/api/reclamation");
        conUpdate.setHttpMethod("PUT");
        conUpdate.addArgument("id", "" + r.getId());
        conUpdate.addArgument("desc", "" + r.getDesc());
        conUpdate.addArgument("sujet", "" + r.getSujet());
        conUpdate.addResponseListener((evt) -> {
        });
        NetworkManager.getInstance().addToQueueAndWait(conUpdate);

        NetworkManager.getInstance().addErrorListener((evt) -> {
            System.out.println("error: " + evt);
        });
    }
}
