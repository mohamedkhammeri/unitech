/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import Entities.Absence;
import Entities.Classe;
import Entities.Matiere;
import Entities.Seance;
import Entities.User;
import Utils.UserUniTech;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ServiceAbsence {

    int k = 0;
    public ArrayList<Absence> Absence;

    public static ServiceAbsence instance = null;
    public boolean resultOK;
    private ConnectionRequest con;

    public ServiceAbsence() {
        con = new ConnectionRequest();
    }

    public static ServiceAbsence getInstance() {
        if (instance == null) {
            instance = new ServiceAbsence();
        }
        return instance;
    }

    /*public boolean addAbsence(Absence a) {
        String url = Statics.BASE_URL + "/Absence/" + a.getAbsence();
        req.setUrl(url);
        req.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                resultOK = req.getResponseCode() == 200; //Code HTTP 200 OK
                req.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(req);
        return resultOK;
    }*/
    public void addabs(Absence n) {
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl(UserUniTech.baseURL + "/ajouterabsencemobile");
        con.setPost(true);
        // con.addArgument("user", "" + UserUniTech.userConnecte.getId_user());
        System.out.println("" + n.getAbsence() + ", u:" + n.getUser().getId_user() + ", c:" + n.getClasse1().getId_classe() + "|" + n.getId_classe()
                + ", mat:" + n.getMat().getId_mat() + ", s:" + n.getId_seance() + "|" + n.getSeance().getId_seance());
        con.addArgument("abs", "" + n.getAbsence());
        con.addArgument("user", "" + n.getUser().getId_user());
        con.addArgument("classe", "" + n.getClasse1().getId_classe());
        con.addArgument("sceance", "" + n.getSeance().getId_seance());
        con.addArgument("mat", "" + n.getMat().getId_mat());
        con.addResponseListener((evt) -> {
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }

    public ArrayList<Absence> parseAbsences(String jsonText) {
        try {
            Absence = new ArrayList<>();
            JSONParser j = new JSONParser();
            Map<String, Object> AbsenceListJson = j.parseJSON(new CharArrayReader(jsonText.toCharArray()));

            List<Map<String, Object>> list = (List<Map<String, Object>>) AbsenceListJson.get("root");
            for (Map<String, Object> obj : list) {
                Absence a = new Absence();

                float absence = Float.parseFloat(obj.get("abs").toString());
                a.setAbsence((int) absence);
                float id = Float.parseFloat(obj.get("id").toString());
                a.setIdabsence((int) id);
                Map<String, Object> user = (Map) obj.get("user");
                a.setNom((String) user.get("nom"));
                a.setCIN(((int) Float.parseFloat(user.get("CIN").toString())));
                Map<String, Object> matiere = (Map) obj.get("matiere");
                a.setMatiere((String) matiere.get("matiere"));
                Map<String, Object> sceance = (Map) obj.get("sceance");
                a.setNum(((int) Float.parseFloat(sceance.get("num").toString())));

                Map<String, Object> classe = (Map) obj.get("classe");
                a.setId_classe(((int) Float.parseFloat(classe.get("classe").toString())));
                Absence.add(a);
            }

        } catch (IOException ex) {

        }
        return Absence;
    }

    public ArrayList<Absence> getAllAbsence() {
        ConnectionRequest con = new ConnectionRequest();
        String url = UserUniTech.baseURL + "/all";
        con.setUrl(url);
        con.setPost(false);
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                Absence = parseAbsences(new String(con.getResponseData()));
                con.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return Absence;
    }

    public ArrayList<User> user;

    public ArrayList<User> parseTasksuser(String jsonText) {
        try {
            user = new ArrayList<>();
            JSONParser j = new JSONParser();
            Map<String, Object> NoteListJson = j.parseJSON(new CharArrayReader(jsonText.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) NoteListJson.get("root");
            for (Map<String, Object> obj : list) {
                User t = new User();
                float id = Float.parseFloat(obj.get("id").toString());

                t.setId_user((int) id);

                float cin = Float.parseFloat(obj.get("CIN").toString());

                t.setCIN((int) cin);

                user.add(t);

            }
        } catch (IOException ex) {

        }
        return user;
    }

    public ArrayList<User> getAllUser() {
        ConnectionRequest con = new ConnectionRequest();// création d'une nouvelle demande de connexion
        String Url = UserUniTech.baseURL + "/affichecin";// création de l'URL
        con.setUrl(Url);
        con.setPost(false);
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                user = parseTasksuser(new String(con.getResponseData()));
                con.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return user;
    }

    public void updateAbsence(Absence a) {
        ConnectionRequest connReq = new ConnectionRequest();
        //String Url = RESTAURANT_API+"update";
        connReq.setPost(true);
        connReq.setUrl(UserUniTech.baseURL + "/updatemobileabs/" + a.getIdabsence());
        connReq.setHttpMethod("POST");
        //connReq.addArgument("id",String.valueOf(a.getIdabsence()));
        connReq.addArgument("absence", String.valueOf(a.getAbsence()));

        NetworkManager.getInstance().addToQueueAndWait(connReq);
    }
    public ArrayList<Matiere> mat;

    public ArrayList<Matiere> parseTasksmatiere(String jsonText) {
        try {
            mat = new ArrayList<>();
            JSONParser j = new JSONParser();
            Map<String, Object> NoteListJson = j.parseJSON(new CharArrayReader(jsonText.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) NoteListJson.get("root");
            for (Map<String, Object> obj : list) {
                Matiere t = new Matiere();
                float id = Float.parseFloat(obj.get("id").toString());
                t.setId_mat((int) id);
                t.setNom_mat((String) obj.get("matiere"));

                mat.add(t);

            }
        } catch (IOException ex) {

        }
        return mat;
    }

    public ArrayList<Matiere> getAllmatiere() {
        ConnectionRequest con = new ConnectionRequest();// création d'une nouvelle demande de connexion
        String Url = UserUniTech.baseURL + "/afficheMat";// création de l'URL
        con.setUrl(Url);
        con.setPost(false);
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                mat = parseTasksmatiere(new String(con.getResponseData()));
                con.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return mat;
    }

    public ArrayList<Classe> classe;

    public int getIdclasse(int num) {
        int b = 0;
        ConnectionRequest con = new ConnectionRequest();// création d'une nouvelle demande de connexion
        String Url = UserUniTech.baseURL + "/Takeclasse/" + num + "/";// création de l'URL
        con.setUrl(Url);
        con.setPost(false);
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                k = parseTaskclasse(new String(con.getResponseData()));

                con.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return k;

    }

    public int parseTaskclasse(String jsonText) {
        int a = 0;
        try {
            classe = new ArrayList<>();
            JSONParser j = new JSONParser();
            Map<String, Object> NoteListJson = j.parseJSON(new CharArrayReader(jsonText.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) NoteListJson.get("root");
            for (Map<String, Object> obj : list) {
                Classe t = new Classe();
                float id = Float.parseFloat(obj.get("id").toString());
                a = (int) id;

                System.out.println("parse" + a);

            }
        } catch (IOException ex) {

        }
        return a;
    }

    public ArrayList<Classe> parseTaskscl(String jsonText) {
        try {
            classe = new ArrayList<>();
            JSONParser j = new JSONParser();
            Map<String, Object> NoteListJson = j.parseJSON(new CharArrayReader(jsonText.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) NoteListJson.get("root");
            for (Map<String, Object> obj : list) {
                Classe t = new Classe();
                float id = Float.parseFloat(obj.get("id").toString());
                t.setId_classe((int) id);

                float num = Float.parseFloat(obj.get("classe").toString());
                t.setNum_classe((int) num);

                classe.add(t);
            }
        } catch (IOException ex) {

        }
        return classe;
    }

    public ArrayList<Classe> getAllclasse() {
        ConnectionRequest con = new ConnectionRequest();// création d'une nouvelle demande de connexion
        String Url = UserUniTech.baseURL + "/afficheClasse";// création de l'URL
        con.setUrl(Url);
        con.setPost(false);
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                classe = parseTaskscl(new String(con.getResponseData()));
                con.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return classe;
    }

    public void deleteAb(int id) {
        ConnectionRequest connReq = new ConnectionRequest();
        //String Url = RESTAURANT_API+"update";
        connReq.setPost(true);
        connReq.setUrl(UserUniTech.baseURL + "/deleteM/" + id);
        connReq.setHttpMethod("POST");
        //connReq.addArgument("id",String.valueOf(a.getIdabsence()));
        connReq.addArgument("absence", String.valueOf(id));

        NetworkManager.getInstance().addToQueueAndWait(connReq);

    }

    // shnua? wini mta seance ili fi service
    public int getIdNote(int cin) {
        int b = 0;
        ConnectionRequest con = new ConnectionRequest();// création d'une nouvelle demande de connexion
        String Url = UserUniTech.baseURL + "/TakeId/" + cin + "/";// création de l'URL
        con.setUrl(Url);
        con.setPost(false);
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                k = parseTaskId(new String(con.getResponseData()));
                con.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return k;

    }

    public int parseTaskId(String jsonText) {
        int a = 0;
        try {
            user = new ArrayList<>();
            JSONParser j = new JSONParser();
            Map<String, Object> NoteListJson = j.parseJSON(new CharArrayReader(jsonText.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) NoteListJson.get("root");
            for (Map<String, Object> obj : list) {
                User t = new User();
                float id = Float.parseFloat(obj.get("id").toString());
                a = (int) id;
                System.out.println("parse" + a);

            }
        } catch (IOException ex) {

        }
        return a;
    }

    public ArrayList<Seance> seance;

    public ArrayList<Seance> parseTasksSc(String jsonText) {
        try {
            seance = new ArrayList<>();
            JSONParser j = new JSONParser();
            Map<String, Object> NoteListJson = j.parseJSON(new CharArrayReader(jsonText.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) NoteListJson.get("root");
            for (Map<String, Object> obj : list) {
                Seance t = new Seance();
                float id = Float.parseFloat(obj.get("id").toString());
                t.setId_seance((int) id);

                float num = Float.parseFloat(obj.get("num").toString());
                t.setNum((int) num);

                seance.add(t);
            }
        } catch (IOException ex) {

        }
        return seance;
    }

    public ArrayList<Seance> getAllseance() {
        ConnectionRequest con = new ConnectionRequest();// création d'une nouvelle demande de connexion
        String Url = UserUniTech.baseURL + "/afficheSc";// création de l'URL
        con.setUrl(Url);
        con.setPost(false);
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                seance = parseTasksSc(new String(con.getResponseData()));
                con.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return seance;
    }

    public int getIdMatiere(String nom_mat) {

        ConnectionRequest con = new ConnectionRequest();// création d'une nouvelle demande de connexion
        String Url = UserUniTech.baseURL + "/Takematiere/'" + nom_mat + "'/";// création de l'URL
        con.setUrl(Url);
        con.setPost(false);
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                k = parseTaskIdm(new String(con.getResponseData()));
                con.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return k;

    }

    public int parseTaskIdm(String jsonText) {
        int a = 0;

        try {
            mat = new ArrayList<>();
            JSONParser j = new JSONParser();
            Map<String, Object> NoteListJson = j.parseJSON(new CharArrayReader(jsonText.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) NoteListJson.get("root");
            for (Map<String, Object> obj : list) {
                float id = Float.parseFloat(obj.get("id").toString());
                a = (int) id;

                System.out.println("parse" + a);

            }
        } catch (IOException ex) {

        }
        return a;
    }

    public int getIdseance(int num) {
        int b = 0;
        ConnectionRequest con = new ConnectionRequest();// création d'une nouvelle demande de connexion
        String Url = UserUniTech.baseURL + "/TakeSeance/" + num + "/";// création de l'URL
        con.setUrl(Url);
        con.setPost(false);
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                k = parseTaskseance(new String(con.getResponseData()));

                con.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return k;

    }

    public int parseTaskseance(String jsonText) {
        int a = 0;
        try {
            seance = new ArrayList<>();
            JSONParser j = new JSONParser();
            Map<String, Object> NoteListJson = j.parseJSON(new CharArrayReader(jsonText.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) NoteListJson.get("root");
            for (Map<String, Object> obj : list) {
                Seance t = new Seance();
                float id = Float.parseFloat(obj.get("id_seance").toString());
                a = (int) id;

                System.out.println("parse" + a);

            }
        } catch (IOException ex) {

        }
        return a;
    }

}
