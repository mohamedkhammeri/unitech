/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Entities.ListeStage;
import Entities.Stage;
import Utils.UserUniTech;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author KattaX
 */
public class StageService {

    private UserService userService;
    private ConnectionRequest con;

    public StageService() {
        con = new ConnectionRequest();
        userService = new UserService();
    }

    public List<Stage> getAll() {
        List<Stage> stages = new ArrayList<>();
        con.setUrl(UserUniTech.baseURL + "/consulter/stage");
        con.setPost(false);
        con.addResponseListener((NetworkEvent evt) -> {
            try {
                JSONParser j = new JSONParser();

                Map<String, Object> stagesJSON = j.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                List<Map<String, Object>> list = (List<Map<String, Object>>) stagesJSON.get("root");
                List<Map<String, Object>> list1 = (List<Map<String, Object>>) stagesJSON.get("stage");

                for (Map<String, Object> obj : list) {
                    Stage stage;
                    stage = new Stage();

                    // int id = (int) ((double) obj.get("id"));
                    //stage.setId_stage(id);
                    stage.setSujet((String) obj.get("sujet"));
                    stage.setDescription((String) obj.get("description"));
                    stage.setBranche((String) obj.get("branche"));
                    stages.add(stage);
                }
            } catch (IOException ex) {
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return stages;
    }

    public void PostulerStage(Stage s) {
        con.setUrl(UserUniTech.baseURL + "/postuler/stagee/" + s.getId_stage());
        Stage stage = new Stage();
        String obj;
        stage.setSujet("sujet");
        stage.setDescription("Description");
        stage.setBranche("branche");

        con.addResponseListener((evt) -> {
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }

    public void addDemande(ListeStage s) {
        con.setUrl(UserUniTech.baseURL + "/envoyer/stage");
        con.setPost(true);
        System.out.println(s.getCV());
        System.out.println("" + s.getSujet() + ", desc:" + s.getDescription() + ", br:" + s.getBranche() + ", lM:" + s.getLettreMotivation() + ", adr:" + s.getAdresse());
        con.addArgument("sujet", "" + s.getSujet());
        con.addArgument("description", "" + s.getDescription());
        con.addArgument("branche", "" + s.getBranche());
        con.addArgument("CV", "" + s.getCV());
        con.addArgument("LettreMotivation", "" + s.getLettreMotivation());
        con.addArgument("adresse", "" + s.getAdresse());
        con.addResponseListener((evt) -> {
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }
}
