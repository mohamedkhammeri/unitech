/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Entities.Enseignant;
import Entities.Note;
import Entities.Reclamation;
import Entities.ReclamationNote;
import Entities.ReclamationProf;
import Services.NoteService;
import Services.ReclamationService;
import Services.UserService;
import Utils.UserUniTech;
import com.codename1.components.MultiButton;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.FontImage;
import com.codename1.ui.Label;
import com.codename1.ui.TextArea;
import com.codename1.ui.TextComponent;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.util.Resources;
import java.util.List;

/**
 *
 * @author KattaX
 */
public class ReclamerForm extends OtherForm {

    private UserService userService;
    private NoteService noteService;
    private ReclamationNote reclamationNote = new ReclamationNote();
    private ReclamationProf reclamationProf = new ReclamationProf();

    public ReclamerForm(Resources res) {
        super(res, "  New  ", "reclamation");

        userService = new UserService();
        noteService = new NoteService();

        //container
        TextComponent sujet = new TextComponent().labelAndHint("Sujet");
        FontImage.setMaterialIcon(sujet.getField().getHintLabel(), FontImage.MATERIAL_PERSON);

        TextComponent desc = new TextComponent().labelAndHint("Description").constraint(TextArea.EMAILADDR);
        FontImage.setMaterialIcon(desc.getField().getHintLabel(), FontImage.MATERIAL_EMAIL);

        MultiButton bChoixNote = new MultiButton("Choix Note");
        bChoixNote.addActionListener(e -> {
            Dialog d = new Dialog();
            d.setLayout(BoxLayout.y());
            d.getContentPane().setScrollableY(true);
            Note n = reclamationNote.getNote();
            MultiButton mbDS = new MultiButton("DS: " + n.getNote_ds());
            d.add(mbDS);
            MultiButton mbCC = new MultiButton("CC: " + n.getNote_cc());
            d.add(mbCC);
            MultiButton mbExam = new MultiButton("EXAM: " + n.getNote_exam());
            d.add(mbExam);

            mbDS.addActionListener((evt) -> {
                reclamationNote.setType_note("DS");
                bChoixNote.setTextLine1("DS: "+n.getNote_ds());
                d.dispose();
                bChoixNote.revalidate();
            });
            mbCC.addActionListener((evt) -> {
                reclamationNote.setType_note("CC");
                bChoixNote.setTextLine1("CC: "+n.getNote_cc());
                d.dispose();
                bChoixNote.revalidate();
            });
            mbExam.addActionListener((evt) -> {
                reclamationNote.setType_note("EXAM");
                bChoixNote.setTextLine1("EXAM: "+n.getNote_exam());
                d.dispose();
                bChoixNote.revalidate();
            });

            d.showPopupDialog(bChoixNote);
        });

        Container choiceNote = new Container(new BoxLayout(BoxLayout.X_AXIS));
        choiceNote.add("note à reclamer");
        choiceNote.add(bChoixNote);
        choiceNote.setHidden(true);

        HiddenObj typeSelected = new HiddenObj(new String(), false);
        Label lChoixRec = new Label("");
        MultiButton bChoixRec = new MultiButton("Choix Rec");
        List elementsList = null;
        HiddenObj elementsObj = new HiddenObj(elementsList, false);
        bChoixRec.addActionListener(e -> {
            Dialog d = new Dialog();
            d.setLayout(BoxLayout.y());
            d.getContentPane().setScrollableY(true);
            List elements = (List) elementsObj.getObject();
            for (Object element : elements) {
                MultiButton mb = new MultiButton();
                String selectedItem = (String) typeSelected.getObject();
                if (selectedItem != null && selectedItem.equals("Enseignant")) {
                    Enseignant enseignant = (Enseignant) element;
                    mb.setTextLine1(enseignant.getFirst_Name());
                } else if (selectedItem != null && selectedItem.equals("Note")) {
                    Note note = (Note) element;
                    mb.setTextLine1(note.getNom_matiere());
                }
                d.add(mb);
                mb.addActionListener(ee -> {
                    bChoixRec.setTextLine1(mb.getTextLine1());
                    if (selectedItem != null && selectedItem.equals("Enseignant")) {
                        reclamationProf.setProf((Enseignant) element);
                    } else if (selectedItem != null && selectedItem.equals("Note")) {
                        reclamationNote.setNote((Note) element);
                        choiceNote.setHidden(false);
                        choiceNote.getParent().animateLayout(200);
                    }

                    d.dispose();
                    bChoixRec.revalidate();
                });
            }
            d.showPopupDialog(bChoixRec);
        });

        Container choiceRec = new Container(new BoxLayout(BoxLayout.X_AXIS));
        choiceRec.add(lChoixRec);
        choiceRec.add(bChoixRec);
        choiceRec.setHidden(true);

        MultiButton b = new MultiButton("Autre");
        String[] characters = {"Enseignant", "Note", "Autre"};
        b.addActionListener(e -> {
            Dialog d = new Dialog();
            d.setLayout(BoxLayout.y());
            d.getContentPane().setScrollableY(true);
            for (int iter = 0; iter < characters.length; iter++) {
                MultiButton mb = new MultiButton(characters[iter]);
                d.add(mb);
                mb.addActionListener(ee -> {
                    choiceNote.setHidden(true);
                    choiceNote.getParent().animateLayout(200);
                    b.setTextLine1(mb.getTextLine1());
                    d.dispose();
                    b.revalidate();
                    if (mb.getTextLine1().equalsIgnoreCase("Enseignant")) {
                        elementsObj.setObject(userService.getAll());
                        typeSelected.setObject("Enseignant");
                        lChoixRec.setText("Enseignants");
                        bChoixRec.setTextLine1("Enseignants");
                        choiceRec.setHidden(false);
                        choiceRec.getParent().animateLayout(200);
                    } else if (mb.getTextLine1().equalsIgnoreCase("Note")) {
                        elementsObj.setObject(noteService.getAll(UserUniTech.userConnecte));
                        lChoixRec.setText("Notes");
                        bChoixRec.setTextLine1("Notes");
                        typeSelected.setObject("Note");
                        choiceRec.setHidden(false);
                        choiceRec.getParent().animateLayout(200);
                    } else {
                        choiceRec.setHidden(true);
                        choiceRec.getParent().animateLayout(200);
                    }
                    bChoixRec.revalidate();
                });
            }
            d.showPopupDialog(b);
        });
        TextComponent choiceLabel = new TextComponent().labelAndHint("Type");
        FontImage.setMaterialIcon(choiceLabel.getField().getHintLabel(), FontImage.MATERIAL_EMAIL);
        choiceLabel.getField().setText("Autre");

        Container choice = new Container(new BoxLayout(BoxLayout.X_AXIS));
        choice.add("Type");
        choice.add(b);
        choice.setUIID("Back_White");

        Button createButton = new Button("Create");
        createButton.setUIID("LoginButton");

        Container enclosure = BorderLayout.center(BoxLayout.encloseY(
                sujet, desc, choice, choiceRec, choiceNote, createButton
        ));

        ReclamationService reclamationService = new ReclamationService();

        createButton.addActionListener((evt) -> {
            Reclamation r = null;
            String choix = (String) typeSelected.getObject();
            if (choix.equals("Enseignant")) {
                r = reclamationProf;
            } else if (choix.equals("Note")) {
                r = reclamationNote;
            } else {
                r = new Reclamation();
            }
            r.setDesc(desc.getText());
            r.setSujet(sujet.getText());
            try {
                reclamationService.addReclamation(r);
                boolean returnVal = Dialog.show("Ajout reclamation", "avec sucess...", "OK", null);
                new ProfileForm(res).show();
            } catch (Exception e) {
                Dialog.show("Ajout reclamation", "echec...", "OK", null);
            }

        });

        add(BorderLayout.CENTER,
                enclosure);
        setupSideMenu(res);
    }

    @Override
    protected void showOtherForm(Resources res) {
    }

}
