/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

/**
 *
 * @author KattaX
 */
public class HiddenObj {

    private Object object;
    private boolean b;

    public HiddenObj(Object object, boolean b) {
        this.object = object;
        this.b = b;
    }

    public boolean isB() {
        b = !b;
        return b;
    }

    public void setB(boolean b) {
        this.b = b;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }
    
    
}
