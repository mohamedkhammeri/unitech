/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Services.ReclamationService;
import Utils.UserUniTech;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.FontImage;
import com.codename1.ui.TextArea;
import com.codename1.ui.TextComponent;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.util.Resources;

/**
 *
 * @author KattaX
 */
public class UpdateReclamation extends OtherForm {

    ReclamationService reclamationService;

    public UpdateReclamation(Resources res, String label1, String label2) {
        super(res, label1, label2);
        reclamationService = new ReclamationService();
        //container
        TextComponent sujet = new TextComponent().labelAndHint("Sujet");
        FontImage.setMaterialIcon(sujet.getField().getHintLabel(), FontImage.MATERIAL_PERSON);

        TextComponent desc = new TextComponent().labelAndHint("Description").constraint(TextArea.EMAILADDR);
        FontImage.setMaterialIcon(desc.getField().getHintLabel(), FontImage.MATERIAL_EMAIL);

        Button updateButton = new Button("Update");
        updateButton.setUIID("LoginButton");

        sujet.getField().setText(UserUniTech.reclamation.getSujet());
        desc.getField().setText(UserUniTech.reclamation.getDesc());

        updateButton.addActionListener((evt) -> {
            UserUniTech.reclamation.setSujet(sujet.getText());
            UserUniTech.reclamation.setDesc(desc.getText());
            reclamationService.updateReclamation(UserUniTech.reclamation);
            new ProfileForm(res).show();
        });

        Container enclosure = BorderLayout.center(BoxLayout.encloseY(
                sujet, desc, updateButton
        ));

        add(BorderLayout.CENTER,
                enclosure);
        setupSideMenu(res);
    }

    @Override
    protected void showOtherForm(Resources res) {
    }

}
