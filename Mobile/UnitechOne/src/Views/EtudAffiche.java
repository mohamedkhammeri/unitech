/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Entities.Classe;
import Entities.Etudiant;
import Services.ClasseService;
import com.codename1.components.MultiButton;
import com.codename1.components.SpanLabel;
import com.codename1.ui.Container;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.util.Resources;
import java.util.List;

/**
 *
 * @author admin
 */
public class EtudAffiche extends Form  {
     public EtudAffiche(Resources res, Classe classe) {
         getToolbar().addCommandToLeftBar("back", null, e -> new ClasseAffiche(res).show());

        ClasseService cs = new ClasseService();
         
        List<Etudiant> etu = cs.parseListEtudJson(classe);
    if (etu != null && !etu.isEmpty()) {
            for (Etudiant et : etu) {
                Container C1 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
                Container C2 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
                Container C3 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
                Container C4 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
                Container C5 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
                Container C6 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
               
             
              int n=0;
              MultiButton mb=null;
              for (n=0;n<etu.size();n++){
                   FontImage placeholderImage =FontImage.createMaterial(FontImage.MATERIAL_PERSON,"label", 6);
                   mb= new MultiButton("Etudiant ");
                   
                                         mb.setIcon(placeholderImage);
    
                          

              }
        
          
                SpanLabel cont = new SpanLabel("Utilisateur    : " + et.getUser_Name());
                SpanLabel cont1 = new SpanLabel("Prénom    : " + et.getFirst_Name());
                SpanLabel cont2 = new SpanLabel("Nom    : " + et.getLast_Name());
                
                                C1.add(mb);

                C2.add(cont);
                C3.add(cont1); 
                C4.add(cont2);
                C5.add(C2);
                C5.add(C3);
                C5.add(C4);
                add(C1);
                add(C5);
               
     }
    }  
     }
}
