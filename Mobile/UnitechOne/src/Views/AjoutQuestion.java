/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import com.codename1.ui.Button;
import com.codename1.ui.Component;
import com.codename1.ui.Dialog;
import com.codename1.ui.Form;
import com.codename1.ui.TextField;
import com.codename1.ui.layouts.FlowLayout;
import Services.ServiceQuestion;
import Entities.Question;
import com.codename1.ui.util.Resources;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Date;

/**
 *
 * @author bhk
 */
public class AjoutQuestion {

    Form f;
    TextField ttitre;
    TextField tdesc;
    Button btnajout;

    public AjoutQuestion(Resources res) {
        f = new Form("Ajout de question", new FlowLayout(Component.CENTER));
        ttitre = new TextField("","titre");
        tdesc = new TextField("","description");
        btnajout = new Button("ajouter");
     
        
        f.add(ttitre);
        f.add(tdesc);
        f.add(btnajout);
       
       
        Date actuelle= new Date();
        DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd");
        String date=dateFormat.format(actuelle);
        String dc= date;
     
        

        btnajout.addActionListener((e) -> {
            ServiceQuestion ser = new ServiceQuestion();
          Question q = new Question(ttitre.getText(), tdesc.getText(),Utils.UserUniTech.userConnecte.getFirst_Name(),dc,Utils.UserUniTech.userConnecte.getId_user());

          ser.ajoutQuestion(q);
          
                Dialog.show("Succée", "question ajouté avec succée!", "Ok", null);
       new PremierAffichageForm(res).show();
        });
       
    }
    //n3ayet lel forme hedha f blgetter hedha
    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }

    public TextField getTtitre() {
        return ttitre;
    }

    public void setTtitre(TextField ttitre) {
        this.ttitre = ttitre;
    }
 public TextField getTdesc() {
        return tdesc;
    }

    public void setTdesc(TextField tdesc) {
        this.tdesc = tdesc;
    }
}
