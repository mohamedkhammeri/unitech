/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.     att naaml copi
 */
package Views;

import com.codename1.ui.Button;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Command;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.FontImage;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextComponent;
import com.codename1.ui.Toolbar;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.util.Resources;
import Entities.Absence;
import Entities.Classe;
import Entities.Matiere;
import Entities.Seance;
import Entities.User;
import Services.ServiceAbsence;
import java.util.List;
import java.util.Random;

/**
 *
 * @author HPAY104-I5-1TR
 */
public class AddabsenceForm extends BaseForm {

    public AddabsenceForm(Resources res) {
        super(new BorderLayout());
        Button myButton = new Button("Retour                                              ");
        myButton.setIcon(FontImage.createMaterial(FontImage.MATERIAL_ARROW_BACK, myButton.getUnselectedStyle()));        //myButton.setUIID("btn-primary");
        //myButton.getAllStyles().setFont(Font.createSystemFont(Font.FACE_MONOSPACE,Font.STYLE_PLAIN,12));
        myButton.addActionListener(e -> {

            new ListeAbsenceForm(res).show();
        });
        Toolbar tb = getToolbar();
        tb.setTitleCentered(false);

        Image profilePic = res.getImage("user-picture.jpg");
        Image tintedImage = Image.createImage(profilePic.getWidth(), profilePic.getHeight());
        Graphics g = tintedImage.getGraphics();
        g.drawImage(profilePic, 0, 0);
        g.drawImage(res.getImage("gradient-overlay.png"), 0, 0, profilePic.getWidth(), profilePic.getHeight());

        tb.getUnselectedStyle().setBgImage(tintedImage);

        Button menuButton = new Button("");
        menuButton.setUIID("Title");

        Button settingsButton = new Button("");
        settingsButton.setUIID("Title");
        FontImage.setMaterialIcon(settingsButton, FontImage.MATERIAL_SETTINGS);

        Label space = new Label("", "TitlePictureSpace");
        space.setShowEvenIfBlank(true);
        Container titleComponent
                = BorderLayout.north(
                        BorderLayout.west(menuButton).add(BorderLayout.EAST, settingsButton)
                ).
                        add(BorderLayout.CENTER, space).
                        add(BorderLayout.SOUTH,
                                FlowLayout.encloseIn(
                                        new Label("Ajout ", "WelcomeBlue"),
                                        new Label("absence", "WelcomeWhite")
                                ));
        titleComponent.setUIID("BottomPaddingContainer");
        tb.setTitleComponent(titleComponent);

        Label separator = new Label("", "BlueSeparatorLine");
        separator.setShowEvenIfBlank(true);
        add(BorderLayout.NORTH, separator);

        //container
        TextComponent absence = new TextComponent().labelAndHint("Absence");
        //FontImage.setMaterialIcon(absence.getField().getHintLabel(), FontImage.MATERIAL_PERSON);

        Button createButton = new Button("Create");
        createButton.setUIID("LoginButton");

        ServiceAbsence reclamationService = new ServiceAbsence();

        ServiceAbsence Service = new ServiceAbsence();
        List<User> no = Service.getAllUser();
        List<Matiere> mat = Service.getAllmatiere();
        List<Classe> classe = Service.getAllclasse();
        List<Seance> seance = Service.getAllseance();
        tb.setTitleComponent(titleComponent);

        Label cin = new Label(" choisir Cin etudiant       ");
        ComboBox combo = new ComboBox();
        if (no != null && !no.isEmpty()) {
            Random r = new Random();
            //combo.addItem("Choisir Cin");
            combo.addItem("0");
            for (User n : no) {

                combo.addItem(n.getCIN());

            };
        }
        int x = Integer.valueOf((String) combo.getSelectedItem());
        Label a = new Label("                   choisir Matiére                ");
        ComboBox combomat = new ComboBox();
        if (no != null && !no.isEmpty()) {
            Random r = new Random();
            combomat.addItem("0");
            for (Matiere m : mat) {

                combomat.addItem(m.getNom_mat());

            };
        }
        String y = (String) combomat.getSelectedItem();

        //Container ctn2;
        Label b = new Label("                    choisir classe               ");
        ComboBox combocl = new ComboBox();
        if (no != null && !no.isEmpty()) {
            Random r = new Random();
            //combocl.addItem("Choisir Classe");
            combocl.addItem("0");
            for (Classe c : classe) {

                combocl.addItem(c.getNum_classe());

            };

        }
        Label l = new Label("                     choisir seance              ");
        ComboBox comboSc = new ComboBox();
        if (no != null && !no.isEmpty()) {
            Random r = new Random();
            //comboSc.addItem("Choisir Seance");
            comboSc.addItem("0");
            for (Seance s : seance) {

                comboSc.addItem(s.getNum());

            };

        }

        createButton.addActionListener((evt) -> {

            User u = new User();
            Classe c = new Classe();

            Seance s = new Seance();
            Matiere m = new Matiere();
            Integer abs = Integer.parseInt(absence.getText());
            Integer n = (Integer) combo.getSelectedItem();
            Integer n1 = (Integer) combocl.getSelectedItem();
            Integer n2 = (Integer) comboSc.getSelectedItem();
            String n3 = (String) combomat.getSelectedItem();
            u.setId_user(reclamationService.getIdNote(n)); //winou combo seance
            c.setId_classe(reclamationService.getIdclasse(n1));
            s.setId_seance(reclamationService.getIdseance(n2));
            m.setId_mat(reclamationService.getIdMatiere(n3));
            Absence r = new Absence(abs, u, c, s, m);
            reclamationService.getInstance().addabs(r);

            Dialog.show("Succés", "ajouté avec succés", new Command("OK"));

        });
        int z = Integer.valueOf((String) combocl.getSelectedItem());

        //   int CIN=Integer.parseInt(x);  asmaani nahi akl x w z li zdhum salah mich mochkla menich ni5dim biha heki
        //   int CL=Integer.parseInt(z);nchl yi5dim taw z5aaalma  5alini njarb liclasse 
        Container enclosure = BorderLayout.center(FlowLayout.encloseCenter(
                myButton, cin, combo, b, combocl, l, comboSc, a, combomat, absence, createButton
        ));
//, combomat, combocl
        add(BorderLayout.CENTER,
                enclosure);
        //setupSideMenu(res);
    }

}
