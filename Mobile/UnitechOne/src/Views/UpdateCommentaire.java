/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import com.codename1.ui.Button;
import com.codename1.ui.Dialog;
import com.codename1.ui.Form;
import com.codename1.ui.TextField;
import Entities.Reponse;
import Services.ServiceQuestion;
import Services.ServiceReponse;
import com.codename1.ui.util.Resources;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author abbes
 */
public class UpdateCommentaire {
        Form f;
   
    TextField tdesc;
    Button btnajout;
public UpdateCommentaire(int id_q, int id_rep,int id_u,String titre,String contenu){
    f = new Form("page de modification de commentaire");
        tdesc = new TextField("","description");
        btnajout = new Button("Modifier");
         f.add(tdesc);
          f.add(btnajout);
          
      
          
             Date actuelle= new Date();
        DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd");
        String date=dateFormat.format(actuelle);
        String dc= date;
     
            btnajout.addActionListener((e)->{
                            ServiceReponse ser = new ServiceReponse();

                            
                            
                            
                Reponse rep = new Reponse( id_rep,id_q,tdesc.getText(),dc,id_u);
if(Dialog.show("enregistrer changement?", "", "oui", "Non")) {
                    ser.updateReponse(rep);
                }
        

                  DetailQuestion dq=new DetailQuestion(titre,contenu);
    dq.getF().show();
                
        });
}
    public UpdateCommentaire(Resources res,int id_q, int id_rep,int id_u,String titre,String contenu) {
          f = new Form("page de modification de commentaire");
        tdesc = new TextField("","description");
        btnajout = new Button("Modifier");
         f.add(tdesc);
          f.add(btnajout);
          
      
          
             Date actuelle= new Date();
        DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd");
        String date=dateFormat.format(actuelle);
        String dc= date;
     
            btnajout.addActionListener((e)->{
                            ServiceReponse ser = new ServiceReponse();

                            
                            
                            
                Reponse rep = new Reponse( id_rep,id_q,tdesc.getText(),dc,id_u);
if(Dialog.show("enregistrer changement?", "", "oui", "Non")) {
                    ser.updateReponse(rep);
                }
        

                  DetailQuestion dq=new DetailQuestion((Resources) res,titre,contenu);
    dq.getF().show();
                
        });
    }
   
    
       public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }

    
 public TextField getTdesc() {
        return tdesc;
    }

    public void setTdesc(TextField tdesc) {
        this.tdesc = tdesc;
    }
    
}
