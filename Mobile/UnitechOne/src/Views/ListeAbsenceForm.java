/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import com.codename1.components.FloatingActionButton;
import com.codename1.components.MultiButton;
import com.codename1.ui.Button;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Container;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.Toolbar;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;
import Entities.Absence;
import Entities.Matiere;
import Services.ServiceAbsence;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author HPAY104-I5-1TR
 */
public class ListeAbsenceForm extends SideMenuBaseForm {

    Absence absence = new Absence();
    //Matiere matiere = new Matiere();
    ServiceAbsence service = new ServiceAbsence();
    Container C3;
    Container C1;

    int[] colors = {0xd997f1, 0x5ae29d, 0x4dc2ff, 0xffc06f};
    ArrayList<Absence> listAbsence = new ArrayList<>();

    public ListeAbsenceForm(Resources res) {
        /*setTitle("Liste Absence");
    SpanLabel sp =new SpanLabel();
    sp.setText(ServiceAbsence.getInstance().getAllAbsence().toString());
    add(sp);

    }*/

        //super(BoxLayout.y());
        Form hi = new Form(BoxLayout.y());
        Toolbar tb = getToolbar();
        Toolbar.setGlobalToolbar(true);
        tb.setTitleCentered(false);
        getContentPane().setScrollVisible(false);
        Image profilePic = res.getImage("user-picture.jpg");
        Image mask = res.getImage("round-mask.png");
        profilePic = profilePic.fill(mask.getWidth(), mask.getHeight());
        Label profilePicLabel = new Label(profilePic, "ProfilePicTitle");
        profilePicLabel.setMask(mask.createMask());
        Button menuButton = new Button("");
        menuButton.setUIID("Title");
        menuButton.getAllStyles().setBgColor(0xffc06f);
        FontImage.setMaterialIcon(menuButton, FontImage.MATERIAL_MENU);
        menuButton.addActionListener(e -> getToolbar().openSideMenu());

        List<Absence> no = service.getAllAbsence();

        Container titleCmp = BoxLayout.encloseY(
                FlowLayout.encloseIn(menuButton),
                BorderLayout.centerAbsolute(
                        BoxLayout.encloseY(
                                //new Label(UserUniTech.userConnecte.getFirst_Name(), "Title"),
                                new Label("", "")
                        )
                ).add(BorderLayout.WEST, profilePicLabel)
        );

        FloatingActionButton fab = FloatingActionButton.createFAB(FontImage.MATERIAL_ADD);
        fab.getAllStyles().setMarginUnit(Style.UNIT_TYPE_PIXELS);
        //fab.getAllStyles().setMargin(BOTTOM, titleCmp.getPreferredH() - fab.getPreferredH() / 2);
        tb.setTitleComponent(fab.bindFabToContainer(titleCmp, CENTER, BOTTOM));
        fab.addActionListener((evt) -> {
            new AddabsenceForm(res).show();
        });

        C1 = new Container(BoxLayout.y());
        C3 = new Container(BoxLayout.y());
        tb.addSearchCommand(e -> {

            String text = (String) e.getSource();

            ArrayList<Absence> filtred_absence = new ArrayList<>();
            for (Absence q : listAbsence) {
                String cin = String.valueOf(q.getCIN());
                if (cin.startsWith(text)) {
                    filtred_absence.add(q);

                }
            }
            C1.removeAll();
            setAbsences(filtred_absence, res);

            revalidate();
        });

        List<Matiere> mat = service.getAllmatiere();

        ComboBox combomat = new ComboBox();
        if (no != null && !no.isEmpty()) {
            Random r = new Random();
            combomat.addItem("Choisir Matiere");
            for (Matiere m : mat) {

                combomat.addItem(m.getNom_mat());

            };
        }
        combomat.addSelectionListener((type, index) -> {
            ArrayList<Absence> filtred_absence = new ArrayList<>();
            for (Absence q : listAbsence) {
                String nommatiere = String.valueOf(q.getMatiere());
                if (nommatiere.startsWith((String) combomat.getSelectedItem())) {
                    filtred_absence.add(q);

                }
            }

            C1.removeAll();
            setAbsences(filtred_absence, res);

            revalidate();
        });

        C3.add(combomat);

        //add(new Label(" ", "TodayTitle"));
        //FontImage arrowDown = FontImage.createMaterial(FontImage.MATERIAL_KEYBOARD_ARROW_DOWN, "Label", 3);
        setupSideMenu(res);
        listAbsence = service.getAllAbsence();
        setAbsences(listAbsence, res);
        add(C3);
        add(C1);

    }

    private int idnn(int id) {
        return id;
    }

    private void setAbsences(ArrayList<Absence> listQuestions, Resources res) {
        Image profilePic = res.getImage("etud.PNG");
        for (Absence a : listQuestions) {
            MultiButton finishLandingPage = new MultiButton(" ");

            finishLandingPage.setUIID("Container");
            finishLandingPage.setUIIDLine1("TodayEntry");
            //finishLandingPage.setIcon(createCircleLine(color, finishLandingPage.getPreferredH(), first));

            finishLandingPage.setIconUIID("Container");
            HiddenObj hiddenObj = new HiddenObj(a, true);
            finishLandingPage.addActionListener((evt) -> {
                // C1.setVisible(true); 
                C1.setHidden(hiddenObj.isB());
                C1.getParent().animateLayout(200);

                //System.out.println("" + ((Label) C1.getComponentAt(0)).getText());
            });
            //  C1.setVisible(false); 
            //C1.setHidden(false);
            C1.add(new Label("Nom etudiant: " + a.getNom()));
            //C1.add(profilePic1); 
            //C1.add(FlowLayout.encloseIn(finishLandingPage));
            C1.add(new Label("Matiére: " + a.getMatiere()));
            C1.add(new Label("Seance: " + a.getNum()));
            C1.add(new Label("Absence: " + a.getAbsence()));
            Label separator = new Label("", "SeparatorLine1");
            separator.setShowEvenIfBlank(true);
            Button myButton = new Button("Modifier");
            myButton.setIcon(FontImage.createMaterial(FontImage.MATERIAL_UPDATE, myButton.getUnselectedStyle()));        //myButton.setUIID("btn-primary");
            //myButton.getAllStyles().setFont(Font.createSystemFont(Font.FACE_MONOSPACE,Font.STYLE_PLAIN,12));
            myButton.addActionListener(e -> {
                absence.setIdabsence(a.getIdabsence());
                absence.setAbsence(a.getAbsence());
                new updateabsenceForm(res, absence).show();
            });

            Button myButton1 = new Button("Supprimer");
            myButton1.setIcon(FontImage.createMaterial(FontImage.MATERIAL_DELETE, myButton.getUnselectedStyle()));

            myButton1.addActionListener(e -> {
                ServiceAbsence sa = new ServiceAbsence();

                sa.deleteAb(a.getIdabsence());
                new ListeAbsenceForm(res).show();
            });

            separator.setShowEvenIfBlank(true);
            C1.add(myButton);
            C1.add(myButton1);

            C1.add(separator);
//
        }

    }

    @Override
    protected void showOtherForm(Resources res) {

    }

}
