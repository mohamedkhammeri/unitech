package Views;

import com.codename1.ui.Container;
import com.codename1.ui.Form;
import com.codename1.ui.Dialog;
import com.codename1.ui.util.Resources;


public class nnform extends Form  {
    public nnform(com.codename1.ui.util.Resources resourceObjectInstance) {
        initGuiBuilderComponents(resourceObjectInstance);
    }

//-- DON'T EDIT BELOW THIS LINE!!!
    protected com.codename1.components.SpanLabel gui_Span_Label = new com.codename1.components.SpanLabel();
    protected com.codename1.ui.Button gui_Button = new com.codename1.ui.Button();
    protected com.codename1.ui.CheckBox gui_Check_Box = new com.codename1.ui.CheckBox();
    protected com.codename1.ui.TextField gui_Text_Field = new com.codename1.ui.TextField();


// <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initGuiBuilderComponents(com.codename1.ui.util.Resources resourceObjectInstance) {
        setLayout(new com.codename1.ui.layouts.LayeredLayout());
        setInlineStylesTheme(resourceObjectInstance);
        setScrollableY(true);
                setInlineStylesTheme(resourceObjectInstance);
        setTitle("nnform");
        setName("nnform");
        gui_Span_Label.setPreferredSizeStr("53.174603mm inherit");
        gui_Span_Label.setText("Span Label");
                gui_Span_Label.setInlineStylesTheme(resourceObjectInstance);
        gui_Span_Label.setName("Span_Label");
        gui_Button.setText("Button");
                gui_Button.setInlineStylesTheme(resourceObjectInstance);
        gui_Button.setName("Button");
        gui_Check_Box.setText("CheckBox");
                gui_Check_Box.setInlineStylesTheme(resourceObjectInstance);
        gui_Check_Box.setName("Check_Box");
        gui_Text_Field.setText("TextField");
                gui_Text_Field.setInlineStylesTheme(resourceObjectInstance);
        gui_Text_Field.setName("Text_Field");
        addComponent(gui_Span_Label);
        addComponent(gui_Button);
        addComponent(gui_Check_Box);
        addComponent(gui_Text_Field);
        ((com.codename1.ui.layouts.LayeredLayout)gui_Span_Label.getParent().getLayout()).setInsets(gui_Span_Label, "17.173252% auto auto 14.049589%").setReferenceComponents(gui_Span_Label, "-1 -1 -1 -1").setReferencePositions(gui_Span_Label, "0.0 0.0 0.0 0.0");
        ((com.codename1.ui.layouts.LayeredLayout)gui_Button.getParent().getLayout()).setInsets(gui_Button, "-2.6455026mm 91.0816% auto -1.8518524mm").setReferenceComponents(gui_Button, "2 -1 -1 0 ").setReferencePositions(gui_Button, "0.0 0.0 0.0 1.0");
        ((com.codename1.ui.layouts.LayeredLayout)gui_Check_Box.getParent().getLayout()).setInsets(gui_Check_Box, "30.395138% 15.230225% auto auto").setReferenceComponents(gui_Check_Box, "-1 -1 -1 -1").setReferencePositions(gui_Check_Box, "0.0 0.0 0.0 0.0");
        ((com.codename1.ui.layouts.LayeredLayout)gui_Text_Field.getParent().getLayout()).setInsets(gui_Text_Field, "0.0mm auto auto 27.508854%").setReferenceComponents(gui_Text_Field, "0 -1 -1 -1").setReferencePositions(gui_Text_Field, "0.0 0.0 0.0 0.0");
    }// </editor-fold>
//-- DON'T EDIT ABOVE THIS LINE!!!
}
