/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Entities.Reclamation;
import Services.ReclamationService;
import com.codename1.components.SpanLabel;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.FontImage;
import com.codename1.ui.Label;
import com.codename1.ui.TextComponent;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.util.Resources;

/**
 *
 * @author KattaX
 */
public class SearchByCode extends OtherForm {

    public SearchByCode(Resources res) {
        super(res, "  Search  ", "reclamation");

        TextComponent code = new TextComponent().labelAndHint("Code");
        FontImage.setMaterialIcon(code.getField().getHintLabel(), FontImage.MATERIAL_SEARCH);

        Button searchBtn = new Button("Search");
        searchBtn.setUIID("LoginButton");

        Container enclosure = BorderLayout.center(FlowLayout.encloseCenter(
                code, searchBtn
        ));

        ReclamationService reclamationService = new ReclamationService();
        Container C1 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        C1.setHidden(true);

        searchBtn.addActionListener((evt) -> {
            try {
                Reclamation rec = reclamationService.searchByCode(code.getText().trim());

                C1.removeAll();
                if (rec != null) {
                    C1.setHidden(false);
                    C1.getParent().animateLayout(200);
                    C1.add(new Label("Titre: " + rec.getSujet()));
                    C1.add(new Label("Description: " + rec.getDesc()));
                    C1.add(new SpanLabel("Etat: " + rec.getEtat()));
                } else {
                    Dialog.show("Reclamation", "Not found", "OK", null);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        add(BorderLayout.CENTER,
                FlowLayout.encloseCenter(enclosure, C1));
        setupSideMenu(res);
    }

    @Override
    protected void showOtherForm(Resources res) {
    }
}
