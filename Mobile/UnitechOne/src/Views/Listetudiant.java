/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Utils.UserUniTech;
import com.codename1.components.FloatingActionButton;
import com.codename1.components.MultiButton;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.FontImage;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;
import Entities.Absence;
import Services.OpinionDAO;
import Services.ServiceAbsence;
import java.util.List;
import java.util.Random;

public class Listetudiant extends SideMenuBaseForm {

    Absence absence = new Absence();

    int[] colors = {0xd997f1, 0x5ae29d, 0x4dc2ff, 0xffc06f};

    public Listetudiant(Resources res) {
        super(BoxLayout.y());
        Toolbar tb = getToolbar();
        tb.setTitleCentered(false);
        Image profilePic = res.getImage("user-picture.jpg");
        Image mask = res.getImage("round-mask.png");
        profilePic = profilePic.fill(mask.getWidth(), mask.getHeight());
        Label profilePicLabel = new Label(profilePic, "ProfilePicTitle");
        profilePicLabel.setMask(mask.createMask());
        Button menuButton = new Button("");
        menuButton.setUIID("Title");
        FontImage.setMaterialIcon(menuButton, FontImage.MATERIAL_MENU);
        menuButton.addActionListener(e -> getToolbar().openSideMenu());

        ServiceAbsence reclamationService = new ServiceAbsence();
        List<Absence> recs = reclamationService.getAllAbsence();

        Container titleCmp = BoxLayout.encloseY(
                FlowLayout.encloseIn(menuButton),
                BorderLayout.centerAbsolute(
                        BoxLayout.encloseY(
                                new Label(UserUniTech.userConnecte.getFirst_Name(), "Title"),
                                new Label("UI/UX Designer", "SubTitle")
                        )
                ).add(BorderLayout.WEST, profilePicLabel)
        );

        FloatingActionButton fab = FloatingActionButton.createFAB(FontImage.MATERIAL_ADD);
        fab.getAllStyles().setMarginUnit(Style.UNIT_TYPE_PIXELS);
        fab.getAllStyles().setMargin(BOTTOM, titleCmp.getPreferredH() - fab.getPreferredH() / 2);
        tb.setTitleComponent(fab.bindFabToContainer(titleCmp, CENTER, BOTTOM));
        fab.addActionListener((evt) -> {
            new AddabsenceForm(res).show();
        });
        //add(new Label(" ", "TodayTitle"));
        FontImage arrowDown = FontImage.createMaterial(FontImage.MATERIAL_KEYBOARD_ARROW_DOWN, "Label", 3);
        if (recs != null && !recs.isEmpty()) {
            Random r = new Random();
            for (Absence a : recs) {

                addButtonBottom(arrowDown, a, true, 0x4dc2ff, res);

            };
        }

        setupSideMenu(res);
    }

    private void addButtonBottom(Image arrowDown, Absence a, boolean first, int color, Resources res) {
        Image profilePic = res.getImage("etud.PNG");
        MultiButton finishLandingPage = new MultiButton(a.getNom() + " ");
        finishLandingPage.setEmblem(arrowDown);
        //finishLandingPage.setEmblem(imagenote);
        // finishLandingPage.setUIID("imagenote1");
        finishLandingPage.setUIID("Container");
        finishLandingPage.setUIIDLine1("TodayEntry");
        //finishLandingPage.setIcon(createCircleLine(color, finishLandingPage.getPreferredH(), first));

        finishLandingPage.setIconUIID("Container");
        Container C1 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        HiddenObj hiddenObj = new HiddenObj(a, true);
        finishLandingPage.addActionListener((evt) -> {
            // C1.setVisible(true); 
            C1.setHidden(hiddenObj.isB());
            C1.getParent().animateLayout(200);
            System.out.println("" + ((Label) C1.getComponentAt(0)).getText());
        });
        add(FlowLayout.encloseIn(finishLandingPage));
        //  C1.setVisible(false);
        C1.setHidden(false);
        C1.add(new Label("Matiére: " + a.getMatiere()));
        C1.add(new Label("Seance: " + a.getNum()));
        C1.add(new Label("Absence: " + a.getAbsence()));
        Button myButton = new Button("Modifier");
        myButton.setIcon(FontImage.createMaterial(FontImage.MATERIAL_UPDATE, myButton.getUnselectedStyle()));

        //myButton.setUIID("btn-primary");
        //myButton.getAllStyles().setFont(Font.createSystemFont(Font.FACE_MONOSPACE,Font.STYLE_PLAIN,12));
        myButton.addActionListener(e -> {
            absence.setIdabsence(a.getIdabsence());
            absence.setAbsence(a.getAbsence());
            new updateabsenceForm(res, absence).show();
        });
        Button myButton1 = new Button("Supprimer");
        myButton1.setIcon(FontImage.createMaterial(FontImage.MATERIAL_UPDATE, myButton.getUnselectedStyle()));

        //myButton.setUIID("btn-primary");
        //myButton.getAllStyles().setFont(Font.createSystemFont(Font.FACE_MONOSPACE,Font.STYLE_PLAIN,12));
        myButton1.addActionListener(e -> {
            ServiceAbsence sa = new ServiceAbsence();

            sa.deleteAb(a.getIdabsence());
            new ListeAbsenceForm(res).show();
        });

        Label separator = new Label("", "SeparatorLine1");
        separator.setShowEvenIfBlank(true);
        Button btnValider = new Button("envoyer email!");

        btnValider.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                new OpinionDAO();
            }
        });

        C1.add(separator);
        C1.add(btnValider);
        add(C1);

    }

    private boolean getInverted(Boolean b) {
        b = new Boolean(!b);
        return b;
    }

    /* private Image createCircleLine(int color, int height, boolean first) {
        Image img = Image.createImage(height, height, 0);
        Graphics g = img.getGraphics();
        g.setAntiAliased(true);
        g.setColor(0xcccccc);
        int y = 0;
        if (first) {
            y = height / 6 + 1;
        }
        g.drawLine(height / 2, y, height / 2, height);
        g.drawLine(height / 2 - 1, y, height / 2 - 1, height);
        g.setColor(color);
        g.fillArc(height / 2 - height / 4, height / 6, height / 2, height / 2, 0, 360);
        return img;
    }*/
    @Override
    protected void showOtherForm(Resources res) {
        new StatsForm(res).show();
    }

}
