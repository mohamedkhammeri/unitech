/*
 * Copyright (c) 2016, Codename One
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package Views;

import Entities.Reclamation;
import Services.ReclamationService;
import Utils.UserUniTech;
import com.codename1.components.FloatingActionButton;
import com.codename1.components.MultiButton;
import com.codename1.components.SpanLabel;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.FontImage;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.Toolbar;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;
import java.util.List;
import java.util.Random;

/**
 * Represents a user profile in the app, the first form we open after the
 * walkthru
 *
 * @author Shai Almog
 */
public class ProfileForm extends SideMenuBaseForm {
    
    int[] colors = {0xd997f1, 0x5ae29d, 0x4dc2ff, 0xffc06f};
    ReclamationService reclamationService;
    List<Reclamation> recs;
    Resources resources;
    Label recSize, recResoluSize;
    
    public ProfileForm(Resources res) {
        super(BoxLayout.y());
        resources = res;
        reclamationService = new ReclamationService();
        Toolbar tb = getToolbar();
        tb.setTitleCentered(false);
        Image profilePic = res.getImage("user-picture.jpg");
        Image mask = res.getImage("round-mask.png");
        profilePic = profilePic.fill(mask.getWidth(), mask.getHeight());
        Label profilePicLabel = new Label(profilePic, "ProfilePicTitle");
        profilePicLabel.setMask(mask.createMask());
        
        Button menuButton = new Button("");
        menuButton.setUIID("Title");
        FontImage.setMaterialIcon(menuButton, FontImage.MATERIAL_MENU);
        menuButton.addActionListener(e -> getToolbar().openSideMenu());
        
        recs = reclamationService.getAll();
        
        recSize = new Label("" + recs.size(), "CenterTitle");
        recResoluSize = new Label("" + reclamationService.countReclamationResolu(), "CenterTitle");
        
        Container remainingTasks = BoxLayout.encloseY(
                recSize,
                new Label("reclamations", "CenterSubTitle")
        );
        remainingTasks.setUIID("RemainingTasks");
        Container completedTasks = BoxLayout.encloseY(
                recResoluSize,
                new Label("reclamations résolu", "CenterSubTitle")
        );
        completedTasks.setUIID("CompletedTasks");
        
        Container titleCmp = BoxLayout.encloseY(
                FlowLayout.encloseIn(menuButton),
                BorderLayout.centerAbsolute(
                        BoxLayout.encloseY(
                                new Label(UserUniTech.userConnecte.getFirst_Name(), "Title"),
                                new Label("UI/UX Designer", "SubTitle")
                        )
                ).add(BorderLayout.WEST, profilePicLabel),
                GridLayout.encloseIn(2, remainingTasks, completedTasks)
        );
        
        FloatingActionButton fab = FloatingActionButton.createFAB(FontImage.MATERIAL_ADD);
        fab.getAllStyles().setMarginUnit(Style.UNIT_TYPE_PIXELS);
        fab.getAllStyles().setMargin(BOTTOM, completedTasks.getPreferredH() - fab.getPreferredH() / 2);
        tb.setTitleComponent(fab.bindFabToContainer(titleCmp, CENTER, BOTTOM));
        
        fab.addActionListener((evt) -> {
            new ReclamerForm(res).show();
        });
        
        add(new Label("Reclamation", "TodayTitle"));
        FontImage arrowDown = FontImage.createMaterial(FontImage.MATERIAL_KEYBOARD_ARROW_DOWN, "Label", 3);
        
        if (recs != null && !recs.isEmpty()) {
            Random r = new Random(123456789);
            for (Reclamation rec : recs) {
                int random = r.nextInt(colors.length);
                //first ?
                if (recs.get(0).equals(rec)) {
                    addButtonBottom(arrowDown, rec, colors[random], true);
                } else {
                    addButtonBottom(arrowDown, rec, colors[random], false);
                }
            };
        }
        
        setupSideMenu(res);
    }
    
    private void addButtonBottom(Image arrowDown, Reclamation rec, int color, boolean first) {
        MultiButton finishLandingPage = new MultiButton(rec.getSujet() != null ? rec.getSujet() : "reclamation" + rec.getId());
        finishLandingPage.setEmblem(arrowDown);
        finishLandingPage.setUIID("Container");
        finishLandingPage.setUIIDLine1("TodayEntry");
        finishLandingPage.setIcon(createCircleLine(color, finishLandingPage.getPreferredH(), first));
        finishLandingPage.setIconUIID("Container");
        Container C1 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        HiddenObj hiddenObj = new HiddenObj(rec, true);
        finishLandingPage.addActionListener((evt) -> {
            //C1.setVisible(true);
            C1.setHidden(hiddenObj.isB());
            C1.getParent().animateLayout(200);
            System.out.println("" + ((Label) C1.getComponentAt(0)).getText());
        });
        add(FlowLayout.encloseIn(finishLandingPage));
        //C1.setVisible(false);
        C1.setHidden(true);
        C1.add(new Label("Description: " + rec.getDesc()));
        C1.add(new SpanLabel("Etat: " + rec.getEtat()));
        Container cBtns = new Container(new FlowLayout());
        Button bSupp = new Button("supp");
        Button bUpdate = new Button("modifier");
        cBtns.add(bSupp);
        cBtns.add(bUpdate);
        
        bSupp.addActionListener((evt) -> {
            System.out.println("del " + rec);
            reclamationService.deleteReclamation(rec);
            Container p = finishLandingPage.getParent();
            Container pC = C1.getParent();
            finishLandingPage.remove();
            recs = reclamationService.getAll();
            recSize.setText(recs.size() + "");
            recResoluSize.setText(reclamationService.countReclamationResolu() + "");
            C1.remove();
            p.repaint();
            pC.repaint();
        });
        bUpdate.addActionListener((evt) -> {
            System.out.println("update " + rec);
            UserUniTech.reclamation = rec;
            new UpdateReclamation(resources, "  Update  ", "reclamation").show();
        });
        
        C1.add(cBtns);
        add(C1);
    }
    
    private boolean getInverted(Boolean b) {
        b = new Boolean(!b);
        return b;
    }
    
    private Image createCircleLine(int color, int height, boolean first) {
        Image img = Image.createImage(height, height, 0);
        Graphics g = img.getGraphics();
        g.setAntiAliased(true);
        g.setColor(0xcccccc);
        int y = 0;
        if (first) {
            y = height / 6 + 1;
        }
        g.drawLine(height / 2, y, height / 2, height);
        g.drawLine(height / 2 - 1, y, height / 2 - 1, height);
        g.setColor(color);
        g.fillArc(height / 2 - height / 4, height / 6, height / 2, height / 2, 0, 360);
        return img;
    }
    
    @Override
    protected void showOtherForm(Resources res) {
        new StatsForm(res).show();
    }
}
