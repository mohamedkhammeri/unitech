/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import com.codename1.components.ImageViewer;
import com.codename1.components.SpanLabel;
import com.codename1.messaging.Message;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import Entities.Reponse;
import Services.ServiceReponse;
import com.codename1.ui.util.Resources;
import java.util.ArrayList;

/**
 *
 * @author abbes
 */
public class DetailQuestion {

    Form f;
    ImageViewer img;
    SpanLabel description_question, titre_question;

    Container ctnq, ctnr;
    ArrayList<Reponse> ListReponses = new ArrayList<>();

    public DetailQuestion(String titre, String contenu) {

        Resources res = null;

        f = new Form("Detail du question", new FlowLayout(Component.CENTER));
        f.getAllStyles().setBgColor(0xfff2e6);
        ServiceReponse serviceReponse = new ServiceReponse();
        ctnq = new Container();
        //ctnq.getStyle().setBgColor(0xA8FB9E);

        // ctnq.getStyle().setBgTransparency(255);
        ListReponses = serviceReponse.getDetailQuestion();

        // System.out.println("hedhy listet reponse"+serviceReponse.getDetailQuestion().toString());
        Label texttitre = new Label();

        texttitre.setText("question  : " + titre);
        Label textdescription = new Label();
        textdescription.setText("description  : " + contenu);
        Label textdate = new Label();
        String datequ = ListReponses.get(0).getDateReponse().substring(0, 10);
        String timequ = ListReponses.get(0).getDateReponse().substring(11, 19);
        textdate.setText("date  : " + datequ + "/" + timequ);
        Button btn_commenter = new Button("commenter");

        btn_commenter.addActionListener((evt) -> {
            int id_q = ListReponses.get(0).getId_quest();
            int id_u = Utils.UserUniTech.userConnecte.getId_user();

            AjoutCommentaire ajtCm = new AjoutCommentaire(id_q, id_u, titre, contenu);
            ajtCm.getF().show();

        });

        ctnq.add(texttitre);
        ctnq.add(textdescription);
        ctnq.add(textdate);
        ctnq.add(btn_commenter);
        f.add(ctnq);

        for (Reponse r : ListReponses) {

            ctnr = new Container(new BoxLayout(BoxLayout.X_AXIS_NO_GROW));
            Container ctn_verticale = new Container(new BoxLayout(BoxLayout.Y_AXIS_BOTTOM_LAST));
            Label description_rep = new Label();

            description_rep.setText(r.getDescriptionReponse());

            Label date_rep = new Label();

            String date = r.getDateReponse();

            date_rep.setText(date);
            ctnr.add(date_rep);
            ctnr.add(description_rep);

            Button deleteBtn = new Button("supprimer");
            if (Utils.UserUniTech.userConnecte.getId_user() == r.getUser_id()) {
                ctn_verticale.add(deleteBtn);
            }

            deleteBtn.addActionListener((evt) -> {
                if (Dialog.show("voulez vous supprimer ce commentaire?", "", "oui", "Non")) {
                    serviceReponse.deleteReponse(r.getId_rep());
                    //methode envoie mail
                    Message m = new Message("votre commentaire à été supprimer de la part de l'admin");

                    Display.getInstance().sendMessage(new String[]{Utils.UserUniTech.userConnecte.getEmail()}, "Alert", m);
                }

                DetailQuestion dq = new DetailQuestion(titre, contenu);
                dq.getF().show();

            });

            Button editBtn = new Button("modifier");

            if (Utils.UserUniTech.userConnecte.getId_user() == r.getUser_id()) {
                ctnr.add(editBtn);
            }
            editBtn.addActionListener((evt) -> {
                int id_q = ListReponses.get(0).getId_quest();
                int id_u = Utils.UserUniTech.userConnecte.getId_user();

                UpdateCommentaire updatepage = new UpdateCommentaire(id_q, r.getId_rep(), id_u, titre, contenu);
                updatepage.getF().show();

            });
            f.add(ctnr);
            f.add(ctn_verticale);

            f.getToolbar().addCommandToRightBar("retour", null, (ev) -> {
                new PremierAffichageForm(res).show();
            });

        }

    }

    public DetailQuestion(Resources res, String titre, String contenu) {

        f = new Form("Detail du question", new FlowLayout(Component.CENTER));
        f.getAllStyles().setBgColor(0xfff2e6);
        ServiceReponse serviceReponse = new ServiceReponse();
        ctnq = new Container();
        //ctnq.getStyle().setBgColor(0xA8FB9E);

        // ctnq.getStyle().setBgTransparency(255);
        ListReponses = serviceReponse.getDetailQuestion();

        // System.out.println("hedhy listet reponse"+serviceReponse.getDetailQuestion().toString());
        Label texttitre = new Label();

        texttitre.setText("question  : " + (ListReponses.get(0).getTitreQuestion()));
        Label textdescription = new Label();
        textdescription.setText("description  : " + (ListReponses.get(0).getDescriptionQuestion()));
        Label textdate = new Label();
        String datequ = ListReponses.get(0).getDateReponse().substring(0, 10);
        String timequ = ListReponses.get(0).getDateReponse().substring(11, 19);
        textdate.setText("date  : " + datequ + "/" + timequ);
        Button btn_commenter = new Button("commenter");

        btn_commenter.addActionListener((evt) -> {
            int id_q = ListReponses.get(0).getId_quest();
            int id_u = Utils.UserUniTech.userConnecte.getId_user();
            AjoutCommentaire ajtCm = new AjoutCommentaire(id_q, id_u, titre, contenu);
            ajtCm.getF().show();

        });

        ctnq.add(texttitre);
        ctnq.add(textdescription);
        ctnq.add(textdate);
        ctnq.add(btn_commenter);
        f.add(ctnq);

        for (Reponse r : ListReponses) {

            ctnr = new Container(new BoxLayout(BoxLayout.Y_AXIS));
            Container ctn_verticale = new Container(new BoxLayout(BoxLayout.Y_AXIS_BOTTOM_LAST));
            Label description_rep = new Label();

            description_rep.setText(r.getDescriptionReponse());

            Label date_rep = new Label();

            String date = r.getDateReponse().substring(0, 10);
            String time = r.getDateReponse().substring(11, 19);

            System.out.println("aaaaaaaa" + r.getUser_id());

            date_rep.setText(date + "/" + time);
            ctnr.add(description_rep);
            ctnr.add(date_rep);

            Button deleteBtn = new Button("supprimer");
            if (!(Utils.UserUniTech.userConnecte.getId_user() == r.getUser_id())) {
                ctn_verticale.add(deleteBtn);
            } else {
                System.out.println("pas mm user");
            }
            deleteBtn.addActionListener((evt) -> {
                if (Dialog.show("voulez vous supprimer ce commentaire?", "", "oui", "Non")) {
                    serviceReponse.deleteReponse(r.getId_rep());
                    //methode envoie mail
                    Message m = new Message("votre commentaire à été supprimer de la part de l'admin");

                    Display.getInstance().sendMessage(new String[]{Utils.UserUniTech.userConnecte.getEmail()}, "Alert", m);
                }

                DetailQuestion dq = new DetailQuestion(res, titre, contenu);
                dq.getF().show();

            });

            Button editBtn = new Button("modifier");
            System.out.println("aaaaaaaa" + r.getUser_id());
            if (Utils.UserUniTech.userConnecte.getId_user() == r.getUser_id()) {
                ctn_verticale.add(editBtn);
            }
            editBtn.addActionListener((evt) -> {
                int id_q = ListReponses.get(0).getId_quest();
                int id_u = Utils.UserUniTech.userConnecte.getId_user();
//ListReponses.get(0).getUser_id();
                UpdateCommentaire updatepage = new UpdateCommentaire(id_q, r.getId_rep(), id_u, titre, contenu);
                updatepage.getF().show();

            });

            f.getToolbar().addCommandToRightBar("retour", null, (ev) -> {
                new PremierAffichageForm(res).show();
            });

        }

    }

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }

}
