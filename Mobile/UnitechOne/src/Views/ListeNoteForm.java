/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Entities.Classe;
import Entities.Matiere;
import Entities.Note;
import Services.ServiceNote;
import Utils.UserUniTech;
import com.codename1.components.FloatingActionButton;
import com.codename1.components.ImageViewer;
import com.codename1.components.MultiButton;
import com.codename1.components.SpanLabel;
import com.codename1.ui.Button;
import static com.codename1.ui.CN.getDisplayHeight;
import static com.codename1.ui.CN.getDisplayWidth;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Component;
import static com.codename1.ui.Component.BOTTOM;
import static com.codename1.ui.Component.CENTER;
import com.codename1.ui.Container;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.Tabs;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import static com.codename1.ui.layouts.BoxLayout.y;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javafx.event.ActionEvent;
import javafx.print.PrinterJob;
import javafx.scene.Node;
import javafx.stage.Window;


/**
 *
 * @author nada kd
 */
public  class ListeNoteForm extends SideMenuBaseForm {  
     private Node barChart;
    Note note=new Note(); 
    ServiceNote sn=new ServiceNote();
    Container C3;
        Container C1 ;
    int[] colors = {0xd997f1, 0x5ae29d, 0x4dc2ff, 0xffc06f};
    ArrayList<Note>  listNote = new ArrayList<>();
    public ListeNoteForm (Resources res) {
   // super("", BoxLayout.y()); 
   //Form hi = new Form(BoxLayout.y());
     Toolbar tb = getToolbar();
     Toolbar.setGlobalToolbar(true);
     tb.setTitleCentered(false);
           
    
      getContentPane().setScrollVisible(false);
   
        Image profilePic = res.getImage("user-picture.jpg");
        Image mask = res.getImage("round-mask.png");
        profilePic = profilePic.fill(mask.getWidth(), mask.getHeight());
        Label profilePicLabel = new Label(profilePic, "ProfilePicTitle");
        profilePicLabel.setMask(mask.createMask());

        Button menuButton = new Button("");
        menuButton.setUIID("Title");
        FontImage.setMaterialIcon(menuButton, FontImage.MATERIAL_MENU);
        menuButton.addActionListener(e -> getToolbar().openSideMenu());

       
        List<Note> no = sn.getAllNote();
        Label lbnote=new Label("");
       
        Container titleCmp = BoxLayout.encloseY(
                FlowLayout.encloseIn(menuButton),
                BorderLayout.centerAbsolute(
                        BoxLayout.encloseY(
                            
                                new Label("", "")
                        )
                ) 
                       .add(BorderLayout.EAST, lbnote)
               
        );
         FloatingActionButton.setIconDefaultSize(5);
         Component C4 =getContentPane();
       
     //FloatingActionButton.createFAB(FontImage.MATERIAL_ADD).bindFabToContainer(hi.getContentPane());
    FloatingActionButton fab = FloatingActionButton.createFAB(FontImage.MATERIAL_ADD);
    fab.bindFabToContainer(C4);
    
    // fab.getAllStyles().setMarginUnit(Style.BACKGROUND_GRADIENT_LINEAR_HORIZONTAL);
     //fab.getAllStyles().setMargin(BOTTOM, completedTasks.getPreferredH() - fab.getPreferredH() / 2);
       tb.setTitleComponent(titleCmp);

        //fab.getAllStyles().setMargin(BOTTOM, completedTasks.getPreferredH() - fab.getPreferredH() / 2);
      // tb.setTitleComponent(fab.bindFabToContainer(titleCmp, CENTER, BOTTOM));

      fab.addActionListener((evt) -> {
            new AddNoteForm(res).show();
        });

    
    C1 = new Container(BoxLayout.y()); 
        C3 = new Container(BoxLayout.y());
       
       //TextField txtclasse = new TextField("","Chercher par Classe ");
         
        //code recherche par cin
     
        
            tb.addSearchCommand(e -> {
             String text = (String)e.getSource();
            ArrayList<Note > filtred_note = new ArrayList<>();
            for (Note q : listNote){
             String cin= String.valueOf(q.getCin());
                 if (cin.startsWith(text)){
            
                      filtred_note.add(q);
              
                 }
              } C1.removeAll(); 
              setNotes(filtred_note,res);
             
              
              revalidate();
          });
  
         //code matiere
          List<Matiere> mat = sn.getAllmatiere();
            ComboBox combomat = new ComboBox();
           if (no != null && !no.isEmpty()) {
            Random r = new Random();
            combomat.addItem("Choisir Matiere");
            for (Matiere m : mat) {
          
            combomat.addItem(m.getNom_mat());
                
            };
        }
             List<Classe> cl = sn.getAllclasse();
            ComboBox combocl = new ComboBox();
           if (no != null && !no.isEmpty()) {
            Random r = new Random();
            combocl.addItem("Choisir Classe");
            for (Classe c : cl) {
          
            combocl.addItem(c.getNum_classe());
                
            };
        }
            combomat.addSelectionListener((type, index) -> {
              ArrayList<Note > filtred_note = new ArrayList<>();
              for (Note q : listNote){
                 String nommatiere= String.valueOf(q.getNom_matiere());
                  String classe= String.valueOf(q.getId_classe());
                   if(nommatiere.startsWith((String)combomat.getSelectedItem())){
                      filtred_note.add(q);
                   
                   }
              }
               
              C1.removeAll(); 
              setNotes(filtred_note,res);
              
              revalidate();
          });
          
        TextField l1=new TextField("");
          C3.add(l1);
             //C3.setUIID("border");
             
             C3.add(combomat);

        //add(new Label(" ", "TodayTitle"));
    
  // FontImage arrowDown = FontImage.createMaterial(FontImage.MATERIAL_KEYBOARD_ARROW_DOWN, "Label", 3);
      

        setupSideMenu(res);
         listNote=sn.getAllNote();
        setNotes(listNote,res);
       
        add(C3);
        add(C1);
     
    
         
    }
    
    
 private int idnn(int id){
     return id;
 }
  //  C1.setVisible(false); 

   private void setNotes(ArrayList<Note> listQuestions,Resources res) {
    Image Pic = res.getImage("noteetud.PNG");
       for (Note n :listQuestions ){
     MultiButton finishLandingPage = new MultiButton(n.getNom() +" "+n.getPrenom());
  
      finishLandingPage.setUIID("Container");

     finishLandingPage.setIcon(Pic);
        finishLandingPage.setIconUIID("Container");

        HiddenObj hiddenObj = new HiddenObj(n, true);
         finishLandingPage.addActionListener((evt) -> {
           // C1.setVisible(true);
            C1.setHidden(hiddenObj.isB());
            C1.getParent().animateLayout(200);
            
        });
        
         Button myButton = new Button("Modifier");
myButton.setIcon(FontImage.createMaterial(FontImage.MATERIAL_UPDATE, myButton.getUnselectedStyle()));
  
         myButton.addActionListener(e->{
            note.setId_note(n.getId_note());
            note.setNote_cc( n.getNote_cc());
            note.setNote_ds(n.getNote_ds());
            note.setNote_exam(n.getNote_exam());
            new UpdateNoteForm(res, note).show();
        });
        // Image edit = res.getImage("edit.png");
         Label separator = new Label("", "SeparatorLine1");
        separator.setShowEvenIfBlank(true);
          // mb.setTextLine4(q.getDate_question());
   /*
       String date =q.getDate_question() ;
    String delimiter = "T";
    StringTokenizer fruitsTokenizer = new StringTokenizer(date, delimiter);
    while (fruitsTokenizer.hasMoreTokens()) {
            String newdate = fruitsTokenizer.toString();
         
    }
       */   C1.add(FlowLayout.encloseIn(finishLandingPage));
   

     C1.add(new Label("Matiére: " + n.getNom_matiere()));
        C1.add(new Label("Note cc: " + n.getNote_cc()));
        C1.add(new SpanLabel("Note ds: " + n.getNote_ds()));
        C1.add(new SpanLabel("Note Examen: " + n.getNote_exam()));
        SpanLabel sp=new SpanLabel("Moyenne: " + n.getMoyenne());
        C1.add(sp); 
           Container C2 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        if((n.getMoyenne()>=16)){
            C2.add(new SpanLabel("Mention: Très Bien  " ));}
        else if ((n.getMoyenne()>=14) && (n.getMoyenne()<16)){
            C2.add(new SpanLabel("Mention:   Bien" ));}
         else if((n.getMoyenne()>=10) && (n.getMoyenne()<14)){
            C2.add(new SpanLabel("Mention: Assez Bien" ));}
        else{
            C2.add(new SpanLabel("Mention: Passable" ));
        }
        C1.add(C2);
        C1.add(myButton);
        C1.add(separator);

    }
   }
   
  

   
   

    @Override
    protected void showOtherForm(Resources res) {
       
    }
}
