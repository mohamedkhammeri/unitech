/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Services.ServiceLogin;
import com.codename1.ui.Button;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.util.Resources;
import java.io.IOException;

/**
 * zz
 *
 * @author admin
 */
public class Login {

    static boolean exist = false;
    EncodedImage imgenc;
    Form login;

    public Login(Resources theme) throws IOException {

        login = new Form("LogIn", BoxLayout.y());
        Label user = new Label("Entrez Votre Email");
        TextField mail = new TextField("", "Email");
        Label pass = new Label("Entrez Votre Mot de Passe");
        TextField password = new TextField("", "Password", 20, TextField.PASSWORD);
        imgenc = EncodedImage.create("/login.png");
        Button log = new Button(imgenc);
        login.addAll(user, mail, pass, password, log);
        login.show();
        ServiceLogin ser = new ServiceLogin();
        log.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                boolean exist = ser.existance(mail.getText(), password.getText());
                if (exist == true) {
                    Views.ClasseAffiche pgui;

                    pgui = new Views.ClasseAffiche(theme);
                    //pgui.getF().show(); 

                } else {
                    ClasseAffiche l;
                    new ClasseAffiche(theme).show();
                }
            }
        });
    }

    public Form getLogin() {
        return login;
    }

}
