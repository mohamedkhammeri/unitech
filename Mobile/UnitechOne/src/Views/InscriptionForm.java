/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Entities.User;
import Services.ServiceLogin;
import com.codename1.ui.Button;
import com.codename1.ui.CheckBox;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Container;
import com.codename1.ui.FontImage;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextArea;
import com.codename1.ui.TextComponent;
import com.codename1.ui.Toolbar;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.util.Resources;

/**
 *
 * @author Saief
 */

public class InscriptionForm extends SideMenuBaseForm {

    public InscriptionForm(Resources res) {
        super(new BorderLayout());
        Toolbar tb = getToolbar();
        tb.setTitleCentered(false);
        Image profilePic = res.getImage("user-picture.jpg");
        Image tintedImage = Image.createImage(profilePic.getWidth(), profilePic.getHeight());
        Graphics g = tintedImage.getGraphics();
        g.drawImage(profilePic, 0, 0);
        g.drawImage(res.getImage("gradient-overlay.png"), 0, 0, profilePic.getWidth(), profilePic.getHeight());

        tb.getUnselectedStyle().setBgImage(tintedImage);

        Button menuButton = new Button("");
        menuButton.setUIID("Title");
        FontImage.setMaterialIcon(menuButton, FontImage.MATERIAL_MENU);
        menuButton.addActionListener(e -> getToolbar().openSideMenu());

        Button settingsButton = new Button("");
        settingsButton.setUIID("Title");
        FontImage.setMaterialIcon(settingsButton, FontImage.MATERIAL_SETTINGS);

        Label space = new Label("", "TitlePictureSpace");
        space.setShowEvenIfBlank(true);
        Container titleComponent
                = BorderLayout.north(
                        BorderLayout.west(menuButton).add(BorderLayout.EAST, settingsButton)
                ).
                        add(BorderLayout.CENTER, space).
                        add(BorderLayout.SOUTH,
                                FlowLayout.encloseIn(
                                        new Label("  New ", "WelcomeBlue"),
                                        new Label("User", "WelcomeWhite")
                                ));
        titleComponent.setUIID("BottomPaddingContainer");
        tb.setTitleComponent(titleComponent);

        Label separator = new Label("", "BlueSeparatorLine");
        separator.setShowEvenIfBlank(true);
        add(BorderLayout.NORTH, separator);

        TextComponent username = new TextComponent().labelAndHint("Username");
        FontImage.setMaterialIcon(username.getField().getHintLabel(), FontImage.MATERIAL_PERSON);

        TextComponent email = new TextComponent().labelAndHint("Email").constraint(TextArea.EMAILADDR);
        FontImage.setMaterialIcon(email.getField().getHintLabel(), FontImage.MATERIAL_EMAIL);

        TextComponent firstname = new TextComponent().labelAndHint("Firstname").constraint(TextArea.EMAILADDR);
        FontImage.setMaterialIcon(firstname.getField().getHintLabel(), FontImage.MATERIAL_PERSON);
        
        TextComponent lastname = new TextComponent().labelAndHint("Lastname").constraint(TextArea.EMAILADDR);
        FontImage.setMaterialIcon(lastname.getField().getHintLabel(), FontImage.MATERIAL_PERSON);
        
        
        TextComponent phonenumber = new TextComponent().labelAndHint("Phonenumber").constraint(TextArea.EMAILADDR);
        FontImage.setMaterialIcon(phonenumber.getField().getHintLabel(), FontImage.MATERIAL_PHONE);
        
        TextComponent cin = new TextComponent().labelAndHint("CIN").constraint(TextArea.EMAILADDR);
        FontImage.setMaterialIcon(cin.getField().getHintLabel(), FontImage.MATERIAL_CARD_GIFTCARD);
        
        TextComponent password = new TextComponent().labelAndHint("Password").constraint(TextArea.EMAILADDR);
        FontImage.setMaterialIcon(password.getField().getHintLabel(), FontImage.MATERIAL_KEYBOARD);
        CheckBox c=new CheckBox("ROLE_ENSEIGNANT");
       CheckBox c1=new CheckBox("ROLE_STUDENT");
       c.setSelected(false);

       c1.setSelected(false);

/* ComboBox<String> combo = new ComboBox(
          createListEntry("ROLE_ENSEIGNANT"),
          createListEntry(""));
        
  */
 // combo.setRenderer(new GenericListCellRenderer<>(new MultiButton(), new MultiButton()));
        
        
        
        Button createButton = new Button("Create");
        createButton.setUIID("LoginButton");

        Container enclosure = BorderLayout.center(FlowLayout.encloseCenter(
                 c,c1,username ,email, firstname,  lastname, phonenumber, cin, password, createButton
        ));

            User user = new User();
        createButton.addActionListener((evt) -> {
            User u = new User();
            u.setUser_Name(username.getText());
            u.setEmail(email.getText());
            u.setFirst_Name(firstname.getText());

            u.setLast_Name(lastname.getText());

            u.setCIN(Integer.parseInt(cin.getText().toString()));

            u.setPhone_number(Integer.parseInt(phonenumber.getText().toString()));
            u.setPassword(password.getText());
            if (c.isSelected()){
                            u.setRole(c.getText());

            }
            else if (c1.isSelected()){
                                            u.setRole(c1.getText());

            }

            ServiceLogin sl=new ServiceLogin();
            sl.addUser(u);
            
        });

        add(BorderLayout.CENTER,
                enclosure);
        setupSideMenu(res);
    }

private String createListEntry(String name) {
    String entry =name;
    return entry;
}

    @Override
    protected void showOtherForm(Resources res) {
    }

}
