/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Entities.Classe;
import Services.ClasseService;
import com.codename1.components.MultiButton;
import com.codename1.components.SpanLabel;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.util.Resources;
import java.util.List;

/**
 *
 * @author admin
 */
public class ClasseAffiche extends Form {

    Classe classe = new Classe();

    public ClasseAffiche(Resources theme) {

        setTitle("Classe");
        //getToolbar().addCommandToLeftBar("back", null, e -> new (res).show());
        setLayout(new BoxLayout(BoxLayout.Y_AXIS));

        ClasseService cs = new ClasseService();
        List<Classe> cls = cs.parseListClasseJson();
        if (cls != null && !cls.isEmpty()) {
            for (Classe cl : cls) {
                Container C1 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
                Container C2 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
                Container C3 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
                Container C4 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
                Container C5 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
                Container C6 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
                final FontImage placeholderImage = FontImage.createMaterial(FontImage.MATERIAL_PERSON, "label", 6);
                MultiButton mb = new MultiButton("Classe");
                mb.setIcon(placeholderImage);

                Label titreE = new Label(" Classes");
                Font font = titreE.getUnselectedStyle().getFont();
                Font newFont = Font.createSystemFont(font.getFace(), Font.STYLE_BOLD, font.getSize());
                titreE.getUnselectedStyle().setFont(newFont);
                SpanLabel contenu = new SpanLabel("Numéro : " + cl.getNum_classe());
                SpanLabel contenu1 = new SpanLabel("Type : " + cl.getType_classe());
                SpanLabel contenu2 = new SpanLabel("Niveau : " + cl.getNiveau());
                SpanLabel contenu3 = new SpanLabel("Capacité : " + cl.getCapacité());
                Button btnA = new Button("Afficher");

                btnA.addActionListener((ActionListener) (ActionEvent o) -> {
                    classe.setId_classe(cl.getId_classe());

                    new EtudAffiche(theme, classe).show();

                });

                C1.add(titreE);

                C3.add(btnA);
                C2.add(contenu);
                C4.add(contenu1);
                C5.add(contenu2);
                C6.add(contenu3);
                add(mb);
                add(C5);
                add(C4);
                add(C2);
                add(C6);
                add(C3);
            }
        }
    }

}
