/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import com.codename1.components.FloatingHint;
import com.codename1.components.ToastBar;
import com.codename1.ui.Button;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Command;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.util.Resources;
import Entities.Note;
import Services.ServiceNote;
import com.codename1.ui.Component;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.PickerComponent;
import com.codename1.ui.Tabs;
import com.codename1.ui.TextArea;
import com.codename1.ui.TextComponent;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.layouts.TextModeLayout;
import com.codename1.ui.validation.LengthConstraint;
import com.codename1.ui.validation.NumericConstraint;
import com.codename1.ui.validation.Validator;
import static java.lang.Float.parseFloat;
import static java.lang.Integer.parseInt;
import java.util.Date;
import javafx.geometry.Pos;
import javafx.scene.image.ImageView;

/**
 *
 * @author nada kd
 */
public class UpdateNoteForm extends SideMenuBaseForm {

    ComboBox combo = new ComboBox();

    public UpdateNoteForm(Resources res, Note note) {

        super(new BorderLayout());
        Toolbar tb = getToolbar();
        setTitle("Modifier Note");
        tb.setTitleCentered(false);

        Image profilePic = res.getImage("user-picture.jpg");
        Image tintedImage = Image.createImage(profilePic.getWidth(), profilePic.getHeight());
        Graphics g = tintedImage.getGraphics();
        g.drawImage(profilePic, 0, 0);
        g.drawImage(res.getImage("gradient-overlay.png"), 0, 0, profilePic.getWidth(), profilePic.getHeight());

        tb.getUnselectedStyle().setBgImage(tintedImage);

        Button menuButton = new Button("");
        menuButton.setUIID("Title");
        FontImage.setMaterialIcon(menuButton, FontImage.MATERIAL_MENU);
        menuButton.addActionListener(e -> getToolbar().openSideMenu());

        Button settingsButton = new Button("");
        settingsButton.setUIID("Title");
        FontImage.setMaterialIcon(settingsButton, FontImage.MATERIAL_SETTINGS);

        Label space = new Label("", "TitlePictureSpace");
        space.setShowEvenIfBlank(true);
        Container titleComponent
                = BorderLayout.north(
                        BorderLayout.west(menuButton).add(BorderLayout.EAST, settingsButton)
                ).
                        add(BorderLayout.CENTER, space).
                        add(BorderLayout.SOUTH,
                                FlowLayout.encloseIn(
                                        new Label("  ", "WelcomeBlue"),
                                        new Label("", "WelcomeWhite")
                                ));
        //titleComponent.setUIID("BottomPaddingContainer");
        tb.setTitleComponent(titleComponent);

        TextModeLayout tl = new TextModeLayout(3, 2);
        Form f = new Form("Modifier Note ", tl);
        ;
        //TextField tfNotecc=new TextField((int)note.getNote_cc()); 
        TextComponent tfNotecc = new TextComponent().label("Note cc : ").text(String.valueOf(note.getNote_cc()));
        TextComponent tfNoteds = new TextComponent().label("Note ds : ").text(String.valueOf(+note.getNote_ds()));
        TextComponent tfExam = new TextComponent().label("Note Examen :").text(String.valueOf(+note.getNote_exam()));
//Validator val = new Validator();
//val.addConstraint(tfNotecc, new LengthConstraint(4));
//val.addConstraint(tfNoteds, new NumericConstraint(true));
//f.add(tl.createConstraint().widthPercentage(60), tfNoteds);
        f.add(tfNotecc);
        f.add(tfNoteds);
        f.add(tfExam);
//f.setEditOnShow(tfNotecc.getField());
//f.setEditOnShow(tfNoteds.getField());
//f.setEditOnShow(tfExam.getField());

        //container
        /* TextComponent sujet = new TextComponent().labelAndHint("Sujet");
        FontImage.setMaterialIcon(sujet.getField().getHintLabel(), FontImage.MATERIAL_PERSON);

        TextComponent desc = new TextComponent().labelAndHint("Description").constraint(TextArea.EMAILADDR);
        FontImage.setMaterialIcon(desc.getField().getHintLabel(),rmage.MATERIAL_EMAIL);
         */
        Button createButton = new Button("Modifier");
        createButton.setUIID("Buttonnote");

        Container enclosure = BorderLayout.center(FlowLayout.encloseCenter(
                f, createButton
        ));

        createButton.addActionListener((ActionEvent evt) -> {
            if (tfNotecc.getText().equals("") || tfNoteds.getText().equals("") || tfExam.getText().equals("")) {
                Dialog.show("erreur", "Veuillez remplir tous les champs ", "ok", null);
            } else {
                ServiceNote ser = new ServiceNote();

                float cc = parseFloat(tfNotecc.getText());
                float ds = parseFloat(tfNoteds.getText());
                float exam = parseFloat(tfExam.getText());
                Note t = new Note(note.getId_note(), cc, ds, exam);

                ser.getInstance().updateNote(t);

                Dialog.show("Success", "Modifier avec succès", new Command("OK"));

                new ListeNoteForm(res).show();
            }

        });

        add(BorderLayout.CENTER,
                enclosure);
        setupSideMenu(res);
    }

    @Override
    protected void showOtherForm(Resources res) {
    }
}
