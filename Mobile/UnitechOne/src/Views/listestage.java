/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Entities.Stage;
import Services.StageService;
import com.codename1.components.ImageViewer;
import com.codename1.components.SpanLabel;
import com.codename1.ui.Button;
import com.codename1.ui.ComboBox;
import static com.codename1.ui.Component.CENTER;
import static com.codename1.ui.Component.TOP;
import com.codename1.ui.Container;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.util.Resources;

import java.util.List;

/**
 *
 * @author ACER
 */
public class listestage extends Form {

    EncodedImage imc;
    Image img;
    ImageViewer imv;
    public static String TITRE;
    public static String TITREimage;
    public static String TITREcontenue;
    public static String TITREDATE;
    Form previous;
    ComboBox<String> cb1 = new ComboBox();

    public listestage(Resources theme) {
        EncodedImage imc;
        Image img;
        ImageViewer imv;
        getToolbar().addMaterialCommandToLeftBar("",
                FontImage.MATERIAL_ARROW_BACK,
                e -> new ProfileForm(theme).show());

        setTitle("les stages");
        //etToolbar().addCommandToLeftBar("back", null, e -> new WalkthruForm(res).show());
        // getToolbar().addCommandToRightBar("Reclamer", null, e -> new HomeReclamation(theme).show());
        setLayout(new BoxLayout(BoxLayout.Y_AXIS));
        Container C1 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        Label titre = new Label("Liste des stages", "WelcomeBlue");
        setLayout(new FlowLayout(CENTER, TOP));
        C1.add(titre);
        add(C1);

        StageService stageService = new StageService();
        List<Stage> stages = stageService.getAll();
        if (stages != null && !stages.isEmpty()) {
            for (Stage stage : stages) {

                Container C2 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
                Container C3 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
                Container C4 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
                Container C5 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
                Container C6 = new Container(new BoxLayout(BoxLayout.Y_AXIS));

                Font font = titre.getUnselectedStyle().getFont();
                Font newFont = Font.createSystemFont(font.getFace(), Font.STYLE_BOLD, font.getSize());
                titre.getUnselectedStyle().setFont(newFont);
                SpanLabel sujet = new SpanLabel("Sujet:   " + stage.getSujet());
                SpanLabel desc = new SpanLabel("Description:   " + stage.getDescription());
                SpanLabel branche = new SpanLabel("branche:    " + stage.getBranche());
                //  SpanLabel branche = new SpanLabel("Branche" + stage.getBranche());

                // System.out.println("");
                Button postuler = new Button("Postuler");
                SpanLabel ligne = new SpanLabel("__________________________________________   ", "BlueSeparatorLine");
                postuler.addActionListener((ActionListener) (ActionEvent o) -> {
                    new PostulerStage(theme, stage).show();

                });

                //  C1.add(titre);
                C2.add(sujet);
                C3.add(desc);
                //    C1.add(branche);
                C6.add(postuler);
                C6.add(ligne);
                C4.add(branche);
                //  C5.add(C1);
                C5.add(C2);
                C5.add(C3);
                C5.add(C4);
                C5.add(C6);

                add(C5);

            }
        }
    }
}
