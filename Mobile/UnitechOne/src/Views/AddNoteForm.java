/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;


import Entities.Classe;
import Entities.Matiere;
import com.codename1.components.FloatingHint;
import com.codename1.components.ToastBar;
import com.codename1.ui.Button;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Command;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.util.Resources;
import Entities.Note;
import Entities.User;
import Services.ServiceNote;
import com.codename1.ui.Component;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.TextArea;
import com.codename1.ui.TextComponent;
import com.codename1.ui.layouts.TextModeLayout;
import com.codename1.ui.plaf.Style;
import static java.lang.Float.parseFloat;
import static java.lang.Integer.parseInt;
import java.util.List;
import java.util.Random;

/**
 *
 * @author nada kd
 */
public class AddNoteForm extends SideMenuBaseForm {
    ComboBox combo = new ComboBox();
    
     public AddNoteForm(Resources res) {
         
        super(new BorderLayout());
        Toolbar tb = getToolbar();
        setTitle("Ajout Note");
        tb.setTitleCentered(false);
        Image profilePic = res.getImage("user-picture.jpg");
        Image tintedImage = Image.createImage(profilePic.getWidth(), profilePic.getHeight());
        Graphics g = tintedImage.getGraphics();
        g.drawImage(profilePic, 0, 0);
        g.drawImage(res.getImage("gradient-overlay.png"), 0, 0, profilePic.getWidth(), profilePic.getHeight());

        tb.getUnselectedStyle().setBgImage(tintedImage);

        Button menuButton = new Button("");
        menuButton.setUIID("Title");
        FontImage.setMaterialIcon(menuButton, FontImage.MATERIAL_MENU);
        menuButton.addActionListener(e -> getToolbar().openSideMenu());

       /* Button settingsButton = new Button("");
        settingsButton.setUIID("Title");
        FontImage.setMaterialIcon(settingsButton, FontImage.MATERIAL_SETTINGS);
*/
        Label space = new Label("", "TitlePictureSpace");
        space.setShowEvenIfBlank(true);
        Container titleComponent
                = BorderLayout.north(
                        BorderLayout.west(menuButton)
                ).
                        add(BorderLayout.CENTER, space).
                        add(BorderLayout.SOUTH,
                                FlowLayout.encloseIn(
                                        new Label("  ", "WelcomeBlue"),
                                        new Label("", "WelcomeWhite")
                                ));
      
       /* getToolbar().addCommandToLeftBar("Back", null, ev -> {
            
            ListeNoteForm hi = new ListeNoteForm(res);
            hi.show();
        });*/
        //titleComponent.setUIID("BottomPaddingContainer");
            
        
        ServiceNote Service = new ServiceNote();
           List<User> no = Service.getAllUser();
            List<Matiere> mat = Service.getAllmatiere();
            List<Classe> classe = Service.getAllclasse();
      tb.setTitleComponent(titleComponent);
      Container cnt = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        //Label cin =new Label("Cin etudiant");
          ComboBox combo = new ComboBox();
           if (no != null && !no.isEmpty()) {
            Random r = new Random();
            combo.addItem("Choisir Cin");
            for (User n : no) {
         
            combo.addItem(n.getCIN());

            
            };
        }
      
     
          
           //Label matiere =new Label("Matiere");
          ComboBox combomat = new ComboBox();
           if (no != null && !no.isEmpty()) {
            Random r = new Random();
            combomat.addItem("Choisir Matiere");
            for (Matiere m : mat) {
          
            combomat.addItem(m.getNom_mat());
                
            };
        }
          
        
            Container cnt2 = new Container(new BoxLayout(BoxLayout.X_AXIS));
         //  Label lbcl =new Label("Classe");
          ComboBox combocl = new ComboBox();
           if (no != null && !no.isEmpty()) {
            Random r = new Random();
            combocl.addItem("Choisir Classe");
            for (Classe c : classe) {
          
            combocl.addItem(c.getNum_classe());
                
            };
        } 
           
           TextField espace0=new TextField();
           
           TextField espace=new TextField();
        TextField espace1=new TextField();
        TextField espace2=new TextField();
        TextModeLayout tl = new TextModeLayout(3, 2);
    
            //Container espace = new Container(new BoxLayout(BoxLayout.X_AXIS));
        // FlowLayout espace=new FlowLayout(Component.CENTER);
           //Container cntt = new Container(new FlowLayout(Component.CENTER));
          
        //  TextField tfNotecc = new TextField("","Note cc  : *");
       
        TextComponent tfNotecc = new TextComponent().label("Note cc : *" );
        tfNotecc.setUIID("txtfieldnada");
       
        TextComponent tfNoteds = new TextComponent().label("Note ds : *");
        tfNoteds.setUIID("txtfieldnada");
        TextComponent tfExam = new TextComponent().label("Note Examen : *" );
         tfExam.setUIID("txtfieldnada");
         
            //tfNotecc.putClientProperty("iosHideToolbar", Boolean.TRUE);
             Form f = new Form("Ajouter Note ", tl);
    f.add(espace0);
    f.add(combo);
     f.add(espace2);
    f.add(combomat);
    f.add(espace);
     f.add(combocl);
     f.add(espace1);
       f.add(tfNotecc);
      f.add(tfNoteds);
      f.add(tfExam);
      
      
        Button createButton = new Button("Ajouter");
        createButton.setUIID("Buttonnote");

        Container enclosure = BorderLayout.center(FlowLayout.encloseCenter(
         f, createButton
        ));
   createButton.addActionListener((ActionEvent evt) -> {
            if ( tfNotecc.getText().equals("")|| tfNoteds.getText().equals("")|| tfExam.getText().equals("")){
                Dialog.show("erreur", "Veuillez remplir tous les champs ", "ok", null);
                        }
                else{
            ServiceNote ser = new ServiceNote();
                      //float c= parseFloat(combo.getText());
                     Integer n = (Integer)combo.getSelectedItem();
                    Integer n1 = (Integer)combocl.getSelectedItem();
                    String n2 = (String)combomat.getSelectedItem(); 
                     ser.getIdNote(n);
                     float cc= parseFloat(tfNotecc.getText());
                     float ds= parseFloat(tfNoteds.getText());
                     float exam= parseFloat(tfExam.getText());
                     User u =new User();
                     Classe c=new Classe();
                     Matiere m=new Matiere();
                     u.setId_user(ser.getIdNote(n));
                     c.setId_classe(ser.getIdclasse(n1));
                      m.setId_mat(ser.getIdMatiere(n2));
                     Note t = new Note( cc, ds,exam,u,c,m);
                     ser.getInstance().addNote(t);
                     Dialog.setDefaultBlurBackgroundRadius(8);
                     Dialog.show("Success","Ajouté avec succès",new Command("OK") );
            new ListeNoteForm(res).show();
            }
          
        });        add(BorderLayout.CENTER,
                enclosure);
        setupSideMenu(res);
    }
    @Override
    protected void showOtherForm(Resources res) {
    }
  
}
  