/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;



import Entities.Note;
import Services.ServiceNote;
import Utils.UserUniTech;
import com.codename1.components.FloatingActionButton;
import com.codename1.components.ImageViewer;
import com.codename1.components.MultiButton;
import com.codename1.components.SpanLabel;
import com.codename1.ui.Button;
import static com.codename1.ui.CN.getDisplayHeight;
import static com.codename1.ui.CN.getDisplayWidth;
import com.codename1.ui.ComboBox;
import static com.codename1.ui.Component.BOTTOM;
import static com.codename1.ui.Component.CENTER;
import com.codename1.ui.Container;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import static com.codename1.ui.layouts.BoxLayout.y;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;
import java.util.List;
import java.util.Random;
import javafx.event.ActionEvent;
import javafx.print.PrinterJob;
import javafx.scene.Node;
import javafx.scene.control.Separator;
import javafx.stage.Window;
/**
 *
 * @author nada kd
 */
    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author nada kd
 */
public  class ReleveNoteForm extends SideMenuBaseForm { 
    Note note=new Note();
    ActionEvent event;
     private Node barChart;

    public ReleveNoteForm (Resources res) {
        super(BoxLayout.y());
       Toolbar tb = getToolbar();
      tb.setTitleCentered(false);
           Toolbar.setGlobalToolbar(true);
     tb.setTitleCentered(false);
      // setUIID("backgroundblanc");
      //tb.setUIID("backgroundblanc");
     
        Button menuButton = new Button("");
       menuButton.setUIID("Relevenote");
        FontImage.setMaterialIcon(menuButton, FontImage.MATERIAL_MENU);
        menuButton.addActionListener(e -> getToolbar().openSideMenu());
 
        ServiceNote Service = new ServiceNote();
        List<Note> no = Service.getAllNote();
        Label Relevé=new Label("Relevé de notes");
        Style s = Relevé.getAllStyles();
        byte TEXT_DECORATION_3D = s.TEXT_DECORATION_3D;
        Relevé.setUIID("Relevenote");
        Container titleCmp = BoxLayout.encloseY(
                FlowLayout.encloseIn(menuButton),
                BorderLayout.centerAbsolute(
                        BoxLayout.encloseY(
                                Relevé
                                
                                //new Label(UserUniTech.userConnecte.getFirst_Name()+" "+UserUniTech.userConnecte.getLast_Name())
                                
                           
                        )
                )
               
        );
       add(titleCmp);
 
        if (no != null && !no.isEmpty()) {
            Random r = new Random();
            for (Note n : no) {
             
              addButtonBottom(n,true,res);
                
            };
        }

        setupSideMenu(res);
    }
 private int idnn(int id){
     return id;
 }
 
 
 
 
 private void addButtonBottom( Note n, boolean first,Resources res) {
        
        MultiButton finishLandingPage = new MultiButton(n.getNom_matiere());
      
    //  finishLandingPage.setEmblem(imagenote);
       // finishLandingPage.setUIID("imagenote1");
     finishLandingPage.setUIID("Container");

     // finishLandingPage.setUIIDLine1("TodayEntry");
        finishLandingPage.setIconUIID("Container");
        Container C1 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        HiddenObj hiddenObj = new HiddenObj(n, true);
       
        Container C3 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
       // Container C4 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
         Container C4 = BorderLayout.centerAbsolute(finishLandingPage);
       
           C1.add(C4);
       //  C1.setVisible(false); 
       //   C4 .add(finishLandingPage);
         
        C1.add(new Label("Coefficient: " + n.getCoef()));
        C1.add(new Label("Note cc: " + n.getNote_cc()));
        C1.add(new SpanLabel("Note ds: " + n.getNote_ds()));
        C1.add(new SpanLabel("Note Examen: " + n.getNote_exam()));
        Label sp=new Label("Moyenne: " + n.getMoyenne());
        C1.setUIID("border");
        C1.add(sp); 
             Container C2 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
             Label lb1=new Label(" Mention Très Bien " );
             lb1.setUIID("Labelred");
              Label lb2=new Label(" Mention Bien " );
               lb2.setUIID("Labelred");
              Label lb3=new Label(" Mention Assez Bien " );
               lb3.setUIID("Labelred");
              Label lb4=new Label(" Mention Passable " );
               lb4.setUIID("Labelred");
        if((n.getMoyenne()>=16)){
            C2.add(lb1);}
        else if ((n.getMoyenne()>=14) && (n.getMoyenne()<16)){
            C2.add(lb2);}
         else if((n.getMoyenne()>=10) && (n.getMoyenne()<14)){
            C2.add(lb3);}
        else{
            C2.add(lb4);
        }
       C2.setUIID("Labelred");
     
        // Image edit = res.getImage("edit.png");
      
           //Label separator = new Label("", "SeparatorLine1");
              
             
              C1.add(C2);
              
               //  C1.add(separator);
                //  C1.add(combo);
        TextField z=new TextField();
        add(C1);
        C3.add(z);
          add(C3);
         
         
    }
 
   
  

    @Override
    protected void showOtherForm(Resources res) {
        new StatsForm(res).show();
    }

   
}


      
      
      


