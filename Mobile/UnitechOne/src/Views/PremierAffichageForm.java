/*
 * Copyright (c) 2016, Codename One
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package Views;

import Entities.Question;
import Services.ServiceQuestion;
import Services.varGlobales;
import Utils.UserUniTech;
import com.codename1.components.FloatingActionButton;
import com.codename1.components.MultiButton;
import com.codename1.components.SpanLabel;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a user profile in the app, the first form we open after the
 * walkthru
 *
 * @author Shai Almog
 */
public class PremierAffichageForm extends SideMenuBaseForm {

    Form f;
    int[] colors = {0xd997f1, 0x5ae29d, 0x4dc2ff, 0xffc06f};
    SpanLabel lb;
    Container ctn, ctn1;
    ArrayList<Question> listQuestions = new ArrayList<>();

    public PremierAffichageForm(Resources res) {

        Toolbar tb1 = getToolbar();
        Toolbar tb = getToolbar();
        tb.setTitleCentered(false);
        Image profilePic = res.getImage("user-picture.jpg");
        Image mask = res.getImage("round-mask.png");
        profilePic = profilePic.fill(mask.getWidth(), mask.getHeight());
        Label profilePicLabel = new Label(profilePic, "ProfilePicTitle");
        profilePicLabel.setMask(mask.createMask());

        Button menuButton = new Button("");
        menuButton.setUIID("Title");
        FontImage.setMaterialIcon(menuButton, FontImage.MATERIAL_MENU);
        menuButton.addActionListener(e -> getToolbar().openSideMenu());

        ServiceQuestion sq = new ServiceQuestion();
        List<Question> Lq = sq.getListQuestion();

        Container titleCmp = BoxLayout.encloseY(
                FlowLayout.encloseIn(menuButton),
                BorderLayout.centerAbsolute(
                        BoxLayout.encloseY(
                                new Label(UserUniTech.userConnecte.getFirst_Name(), "Title")
                        )
                ).add(BorderLayout.WEST, profilePicLabel));
        ctn = new Container(BoxLayout.y());
        Container ctn4 = new Container(BoxLayout.y());
        Button bb = new Button("Add");
        FloatingActionButton fab = FloatingActionButton.createFAB(FontImage.MATERIAL_SWIPE);
        fab.getAllStyles().setMarginUnit(Style.BACKGROUND_GRADIENT_RADIAL);
        fab.getAllStyles().setMargin(BOTTOM, fab.getPreferredH() / 2);
        tb.setTitleComponent(fab.bindFabToContainer(titleCmp, RIGHT, TOP));
        bb.addActionListener((evt) -> {
            AjoutQuestion dq = new AjoutQuestion(res);
            dq.getF().show();
        });
        fab.addActionListener((evt) -> {
            PieChartMobile dq1 = new PieChartMobile();
            dq1.getF().show();

        });

        add(new Label("Sujet", "TodayTitle"));
        FontImage arrowDown = FontImage.createMaterial(FontImage.MATERIAL_KEYBOARD_ARROW_DOWN, "Label", 3);

        setupSideMenu(res);

        getAllStyles().setBgColor(0xfff2e6);

        ctn1 = new Container(BoxLayout.y());
        ctn.setScrollableY(false);
        ctn1.setScrollableY(false);
        TextField filter = new TextField();

        filter.addDataChangedListener((type, index) -> {
            ArrayList<Question> filtred_questions = new ArrayList<>();
            for (Question q : listQuestions) {
                if (q.getTitre_question().startsWith(filter.getText())) {
                    filtred_questions.add(q);
                }
            }
            ctn.removeAll();
            setQuestions(filtred_questions);

            revalidate();
        });
        ctn1.add(filter);

        ServiceQuestion serviceQuestion = new ServiceQuestion();

        //   lb.setText(serviceQuestion.getListQuestion().toString());//hatina fl lb resultat mtaa lmethode getList2()
        listQuestions = serviceQuestion.getListQuestion();
        setQuestions(listQuestions);
        add(bb);
        add(ctn1);
        add(ctn);
    }

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }

    private void setQuestions(ArrayList<Question> listQuestions) {

        for (Question q : listQuestions) {

            MultiButton mb = new MultiButton(q.getTitre_question());

            final FontImage placeholderImage = FontImage.createMaterial(FontImage.MATERIAL_PERSON, "label", 6);

            mb.setIcon(placeholderImage);

            mb.setTextLine3(q.getDescription_question());
            // mb.setTextLine4(q.getDate_question());
            /*
       String date =q.getDate_question() ;
    String delimiter = "T";
    StringTokenizer fruitsTokenizer = new StringTokenizer(date, delimiter);
    while (fruitsTokenizer.hasMoreTokens()) {
            String newdate = fruitsTokenizer.toString();
         
    }
             */

            String date = q.getDate_question().substring(0, 10);

            mb.setTextLine4(date);

            //methode oncklick aal element de liste
            mb.addActionListener((evt) -> {

                varGlobales.setId(q.getId());

                DetailQuestion dq = new DetailQuestion(q.getTitre_question(), q.getDescription_question());
                dq.getF().show();

            }
            );
            ctn.add(mb);
            Button btn_comment = new Button("commenter");
            ctn.add(btn_comment);

            btn_comment.addActionListener((evt) -> {
                int id_q = q.getId();
                int id_u = q.getUser_id();
                AjoutCommentaire ajtCm = new AjoutCommentaire(id_q, id_u, q.getTitre_question(), q.getDescription_question());
                ajtCm.getF().show();

            });

        }
    }

    @Override
    protected void showOtherForm(Resources res) {
        new StatsForm(res).show();
    }
}
