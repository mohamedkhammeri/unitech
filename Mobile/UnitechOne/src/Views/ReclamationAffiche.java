/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Entities.Reclamation;
import Entities.ReclamationNote;
import Entities.ReclamationProf;
import Services.ReclamationService;
import com.codename1.components.ImageViewer;
import com.codename1.components.SpanLabel;
import com.codename1.ui.Button;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Container;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Font;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.util.Resources;
import java.util.List;

/**
 *
 * @author ACER
 */
public class ReclamationAffiche extends Form {
    
    EncodedImage imc;
    Image img;
    ImageViewer imv;
    public static String TITRE;
    public static String TITREimage;
    public static String TITREcontenue;
    public static String TITREDATE;
    ComboBox<String> cb1 = new ComboBox();
    
    public ReclamationAffiche(Resources theme) {
        EncodedImage imc;
        Image img;
        ImageViewer imv;
        
        setTitle("reclamation");
        //etToolbar().addCommandToLeftBar("back", null, e -> new WalkthruForm(res).show());
        getToolbar().addCommandToRightBar("Reclamer", null, e -> new HomeReclamation(theme).show());
        setLayout(new BoxLayout(BoxLayout.Y_AXIS));
        
        ReclamationService reclamationService = new ReclamationService();
        List<Reclamation> recs = reclamationService.getAll();
        if (recs != null && !recs.isEmpty()) {
            for (Reclamation rec : recs) {
                Container C1 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
                Container C2 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
                Container C3 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
                Container C4 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
                Container C5 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
                
                Label titreE = new Label("Sujet: " + rec.getSujet());
                Font font = titreE.getUnselectedStyle().getFont();
                Font newFont = Font.createSystemFont(font.getFace(), Font.STYLE_BOLD, font.getSize());
                titreE.getUnselectedStyle().setFont(newFont);
                SpanLabel contenu = new SpanLabel("etat: " + rec.getEtat());
                Label nameCat = new Label("Categorie: ");
                Button btnA = new Button("afficher");
                
                btnA.addActionListener((ActionListener) (ActionEvent o) -> {
                });
                
                if (!(rec instanceof ReclamationNote || rec instanceof ReclamationProf)) {
                    C1.add(titreE);
                }
                C2.add(contenu);
                C4.add(btnA);
                C3.add(C1);
                C3.add(C2);
                C3.add(C4);
                
                add(C3);
            }
        }
    }
}
