/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import com.codename1.ui.Button;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Command;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextComponent;
import com.codename1.ui.Toolbar;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.TextModeLayout;
import com.codename1.ui.util.Resources;
import Entities.Absence;
import Services.ServiceAbsence;

/**
 *
 * @author HPAY104-I5-1TR
 */
public class updateabsenceForm extends SideMenuBaseForm {

    ComboBox combo = new ComboBox();

    public updateabsenceForm(Resources res, Absence absence) {
        super(new BorderLayout());
        Toolbar tb = getToolbar();
        tb.setTitleCentered(false);
        Image profilePic = res.getImage("user-picture.jpg");
        Image tintedImage = Image.createImage(profilePic.getWidth(), profilePic.getHeight());
        Graphics g = tintedImage.getGraphics();
        g.drawImage(profilePic, 0, 0);
        g.drawImage(res.getImage("gradient-overlay.png"), 0, 0, profilePic.getWidth(), profilePic.getHeight());

        tb.getUnselectedStyle().setBgImage(tintedImage);

        Button menuButton = new Button("");
        menuButton.setUIID("Title");
        FontImage.setMaterialIcon(menuButton, FontImage.MATERIAL_MENU);
        menuButton.addActionListener(e -> getToolbar().openSideMenu());

        Button settingsButton = new Button("");
        settingsButton.setUIID("Title");
        FontImage.setMaterialIcon(settingsButton, FontImage.MATERIAL_SETTINGS);

        Label space = new Label("", "TitlePictureSpace");
        space.setShowEvenIfBlank(true);
        Container titleComponent
                = BorderLayout.north(
                        BorderLayout.west(menuButton).add(BorderLayout.EAST, settingsButton)
                ).
                        add(BorderLayout.CENTER, space).
                        add(BorderLayout.SOUTH,
                                FlowLayout.encloseIn(
                                        new Label("  ", "WelcomeBlue"),
                                        new Label("", "WelcomeWhite")
                                ));
        //titleComponent.setUIID("BottomPaddingContainer");
        tb.setTitleComponent(titleComponent);

        TextModeLayout tl = new TextModeLayout(3, 2);
        Form f = new Form(" ", tl);
        // TextField tfNotecc=new TextField((int)note.getNote_cc()); 
        TextComponent abs = new TextComponent().label("absence : ").text(String.valueOf(absence.getAbsence()));

//Validator val = new Validator();
//val.addConstraint(tfNotecc, new LengthConstraint(4));
//val.addConstraint(tfNoteds, new NumericConstraint(true));
//f.add(tl.createConstraint().widthPercentage(60), tfNoteds);
        f.add(abs);

//f.setEditOnShow(tfNotecc.getField());
//f.setEditOnShow(tfNoteds.getField());
//f.setEditOnShow(tfExam.getField());
        //container
        /* TextComponent sujet = new TextComponent().labelAndHint("Sujet");
        FontImage.setMaterialIcon(sujet.getField().getHintLabel(), FontImage.MATERIAL_PERSON);

        TextComponent desc = new TextComponent().labelAndHint("Description").constraint(TextArea.EMAILADDR);
        FontImage.setMaterialIcon(desc.getField().getHintLabel(),rmage.MATERIAL_EMAIL);
         */
        Button createButton = new Button("Modifier");
        createButton.setUIID("Buttonnote");

        Container enclosure = BorderLayout.center(FlowLayout.encloseCenter(
                f, createButton
        ));

        createButton.addActionListener((evt) -> {
            if (abs.getText().equals("")) {
                Dialog.show("erreur", "Veuillez remplir tous les champs ", "ok", null);
            } else {
                ServiceAbsence ser = new ServiceAbsence();

                int abs1 = Integer.parseInt(abs.getText());
                absence.setAbsence(abs1);
                Absence t = new Absence(absence.getIdabsence());
                ser.getInstance().updateAbsence(absence);
                Dialog.setDefaultBlurBackgroundRadius(8);
                Dialog.show("Success", "Modifier avec succès", new Command("OK"));
                new ListeAbsenceForm(res).show();
            }

        });

        add(BorderLayout.CENTER,
                enclosure);
        setupSideMenu(res);
    }

    @Override
    protected void showOtherForm(Resources res) {
    }
}
