/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Entities.Stage;
import Entities.ListeStage;
import Services.StageService;
import com.codename1.capture.Capture;
import com.codename1.components.SpanLabel;
import com.codename1.ui.Button;
import com.codename1.ui.Command;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.util.Resources;
import com.codename1.ui.TextArea;
import rest.file.uploader.tn.FileUploader;

/**
 *
 * @author ACER
 */
public class PostulerStage extends Form {

    private FileUploader file;
    String fileNameInServer;
    private String imgPath;
    Form previous;
    ListeStage pass = new ListeStage();

    public PostulerStage(Resources theme, Stage s) {
//                      this.previous = previous;
        getToolbar().addMaterialCommandToLeftBar("",
                FontImage.MATERIAL_ARROW_BACK,
                e -> new ProfileForm(theme).show());
        StageService stageService = new StageService();
        stageService.PostulerStage(s);
        Container C1 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        Container C2 = new Container(new BoxLayout(BoxLayout.X_AXIS));
        Container C3 = new Container(new BoxLayout(BoxLayout.X_AXIS));
        Container C4 = new Container(new BoxLayout(BoxLayout.X_AXIS));
        Container C5 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        Container C6 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        Container C7 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        Label l1 = new Label("Sujet:  ");
        SpanLabel sujet = new SpanLabel(s.getSujet());
        Label l2 = new Label("Description:   ");
        SpanLabel desc = new SpanLabel(s.getDescription());
        Label l3 = new Label("Branche:   ");
        SpanLabel branche = new SpanLabel(s.getBranche());
        Label l4 = new Label("Tapez votre lettre de motivation");
        TextArea LM = new TextArea(10, 25);
        Label CV = new Label("Telecharger votre CV");
        Button TCV = new Button("CV");

        Label l5 = new Label("Tapez vtre adresse");
        TextArea adresse = new TextArea(1, 15);
        Button Envoyer = new Button("Envoyer");
        //////////

        C2.add(l1);
        C2.add(sujet);
        C1.add(C2);
        C3.add(l2);
        C3.add(desc);
        C1.add(C3);
        C4.add(l3);
        C4.add(branche);
        C1.add(C4);
        C5.add(l4);
        C5.add(LM);
        C1.add(C5);
        C6.add(l5);
        C6.add(adresse);
        C1.add(C6);
        C7.add(CV);
        C7.add(TCV);
        C1.add(C7);
        C1.add(Envoyer);
        add(C1);
//                TCV.addActionListener((evt) -> {
//                String i =Capture.capturePhoto(Display.getInstance().getDisplayWidth(), -1);
//                
//                if (i !=null){
//                    findPh
//                
//                
//                
//                }

//               });
        TCV.addActionListener((evt) -> {
            String il = Capture.capturePhoto();
            String i = Capture.capturePhoto().toString();
            int num = i.indexOf("/", 2);
            String news = i.substring(num + 2, i.length());

            System.out.println("" + news);
            System.out.println("" + i);

            FileUploader fu = new FileUploader("http://localhost/PiDevSymf/web/upload");
            try {
                fileNameInServer = fu.upload(news);
                System.out.println("aaa" + fileNameInServer);
                pass.setCV(fileNameInServer);

            } catch (Exception ex) {

            }
            //Component  c;

        });
        Envoyer.addActionListener((evt) -> {

            StageService ss = new StageService();

            String Sujet = sujet.getText();
            String Description = desc.getText();
            String Branche = branche.getText();
            String LettreMotivation = LM.getText();
            String Adresse = adresse.getText();
            System.out.println("name" + pass.getCV());
            ListeStage ls = new ListeStage(Sujet, Description, Branche, LettreMotivation, "/uploads/" + pass.getCV(), Adresse);
            ss.addDemande(ls);
            Dialog.setDefaultBlurBackgroundRadius(8);
            Dialog.show("Success", "Votre Demande a été envoyée avec succes", new Command("OK"));
            new listestage(theme).show();
        }
        );

    }

}
