/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

/**
 *
 * @author HPAY104-I5-1TR
 */
public class Absence {

    private int idabsence;
    private int id_user;
    private int id_seance;
    private int id_classe;
    private int id_matiere;
    private int absence;
    private String nom;
    private int CIN;
    private int num;
    private String matiere;
    private int classe;
    private String sceance;
    User user;
    Classe classe1;
    Seance seance;
    Matiere mat;

    public int getCIN() {
        return CIN;
    }

    public void setCIN(int CIN) {
        this.CIN = CIN;
    }

    public Absence(int idabsence) {

    }

    public Absence(int absence, User user, Classe classe1, Seance seance, Matiere mat) {
        this.absence = absence;
        this.user = user;
        this.classe1 = classe1;
        this.seance = seance;
        this.mat = mat;
    }

    public Matiere getMat() {
        return mat;
    }

    public void setMat(Matiere mat) {
        this.mat = mat;
    }

    public Seance getSeance() {
        return seance;
    }

    public void setSeance(Seance seance) {
        this.seance = seance;
    }

    public Classe getClasse1() {
        return classe1;
    }

    public void setClasse1(Classe classe1) {
        this.classe1 = classe1;
    }

    public int getNum() {
        return num;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public Absence() {
    }

    public Absence(int absence, int id_user, String matiere, int id_classe) {
        this.absence = absence;
        this.id_user = id_user;
        this.matiere = matiere;
        this.id_classe = id_classe;

    }

    public Absence(int idabsence, int id_user, int id_seance, int id_classe, int id_matiere, int absence, int CIN) {
        this.idabsence = idabsence;
        this.id_user = id_user;
        this.id_seance = id_seance;
        this.id_classe = id_classe;
        this.id_matiere = id_matiere;
        this.absence = absence;
        this.CIN = CIN;
    }

    public String getMatiere() {
        return matiere;
    }

    public void setMatiere(String matiere) {
        this.matiere = matiere;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getIdabsence() {
        return idabsence;
    }

    public void setIdabsence(int idabsence) {
        this.idabsence = idabsence;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public int getId_seance() {
        return id_seance;
    }

    public void setId_seance(int id_seance) {
        this.id_seance = id_seance;
    }

    public int getId_classe() {
        return id_classe;
    }

    public void setId_classe(int id_classe) {
        this.id_classe = id_classe;
    }

    public int getId_matiere() {
        return id_matiere;
    }

    public void setId_matiere(int id_matiere) {
        this.id_matiere = id_matiere;
    }

    public int getAbsence() {
        return absence;
    }

    public void setAbsence(int absence) {
        this.absence = absence;
    }

    @Override
    public String toString() {
        return "Absence{" + "idabsence=" + idabsence + ", absence=" + absence + ", nom=" + nom + ", matiere=" + matiere + ", id_classe=" + id_classe + ", num=" + num + ", CIN=" + CIN + ", user=" + user + ", seance=" + seance + '}';
    }

}
