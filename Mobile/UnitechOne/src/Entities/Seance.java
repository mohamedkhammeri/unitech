package Entities;

/**
 *
 * @author HPAY104-I5-1TR
 */
public class Seance {

    private int id_seance;
    //private Time heure;
    private String jour;
    private int num;

    public Seance(int id_seance, String jour, int num) {
        this.id_seance = id_seance;
        //this.heure = heure;
        this.jour = jour;
        this.num = num;
    }

    public Seance() {
    }

    public int getId_seance() {
        return id_seance;
    }

    public void setId_seance(int id_seance) {
        this.id_seance = id_seance;
    }

    public String getJour() {
        return jour;
    }

    public void setJour(String jour) {
        this.jour = jour;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    @Override
    public String toString() {
        return "Seance{" + "id_seance=" + id_seance + ", jour=" + jour + ", num=" + num + '}';
    }

}
