<?php

namespace ClasseBundle\Entity;

use AppBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Mgilet\NotificationBundle\Annotation\Notifiable;
use Mgilet\NotificationBundle\NotifiableInterface;

/**
 * Etudiant
 *
 * @ORM\Table(name="etudiant")
 * @ORM\Entity(repositoryClass="ClasseBundle\Repository\EtudiantRepository")
 * @Notifiable(name="student_notify")
 */
class Etudiant extends User implements NotifiableInterface, \JsonSerializable
{

    public function __construct()
    {
        parent::__construct();
        $this->addRole(User::ROLE_ETUDIANT);
    }

    /**
     * @var Classe
     * @ORM\ManyToOne(targetEntity="ClasseBundle\Entity\Classe", inversedBy="etudiants", cascade={"all"})
     * @ORM\JoinColumn(name="classe_id",referencedColumnName="id_classe", onDelete="SET NULL")
     */
    private $classe;

    /**
     * @ORM\OneToMany(targetEntity="NoteBundle\Entity\Note", mappedBy="user")
     */
    protected $notes;

    /**
     * @ORM\ManyToMany(targetEntity="NoteBundle\Entity\Matiere", mappedBy="etudiants")
     */
    protected $matieres;

    /**
     * @ORM\OneToMany(targetEntity="ReclamationBundle\Entity\Reclamation", mappedBy="etudiant")
     */
    protected $reclamations;

    /**
     * @return ArrayCollection
     */
    public function getReclamations()
    {
        return $this->reclamations;
    }

    /**
     * @param ArrayCollection $reclamations
     */
    public function setReclamations($reclamations)
    {
        $this->reclamations = $reclamations;
    }

    /**
     * @return mixed
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * @param mixed $notes
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    }

    /**
     * @return Classe
     */
    public function getClasse()
    {
        return $this->classe;
    }

    /**
     * @param mixed $classe
     */
    public function setClasse($classe)
    {
        $this->classe = $classe;
    }

    /**
     * @return mixed
     */
    public function getMatieres()
    {
        return $this->matieres;
    }

    /**
     * @param mixed $matieres
     */
    public function setMatieres($matieres)
    {
        $this->matieres = $matieres;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
            "id" => $this->id,
            "CIN" => $this->getCIN(),
            "nom" => $this->getFirstName(),
            "prenom" => $this->getLastName(),
        ];
    }
}

