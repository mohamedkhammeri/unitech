<?php

namespace ClasseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Permutation
 *
 * @ORM\Table(name="permutation")
 * @ORM\Entity(repositoryClass="ClasseBundle\Repository\PermutationRepository")
 */
class Permutation
{

    public function __construct()
    {
        $this->dateCreation = new \DateTime("now");
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $dateCreation;

    /**
     * @var Etudiant
     * @ORM\OneToOne(targetEntity="ClasseBundle\Entity\Etudiant")
     * @ORM\JoinColumn(nullable=false)
     */
    private $student1;

    /**
     * @var Etudiant
     * @ORM\OneToOne(targetEntity="ClasseBundle\Entity\Etudiant")
     * @ORM\JoinColumn(nullable=false)
     */
    private $student2;

    /**
     * @ORM\Column(type="boolean")
     */
    private $accepted = false;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * @param \DateTime $dateCreation
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;
    }

    /**
     * @return mixed
     */
    public function getStudent1()
    {
        return $this->student1;
    }

    /**
     * @param mixed $student1
     */
    public function setStudent1($student1)
    {
        $this->student1 = $student1;
    }

    /**
     * @return mixed
     */
    public function getStudent2()
    {
        return $this->student2;
    }

    /**
     * @param mixed $student2
     */
    public function setStudent2($student2)
    {
        $this->student2 = $student2;
    }

    /**
     * @return bool
     */
    public function isAccepted()
    {
        return $this->accepted;
    }

    /**
     * @param bool $accepted
     */
    public function setAccepted($accepted)
    {
        $this->accepted = $accepted;
    }

    public function __toString()
    {
        return "" . $this->student1 . "->" . $this->student2->getClasse() . " with " . $this->student2 . " from " . $this->student1->getClasse() . " at " . $this->dateCreation;
    }
}

