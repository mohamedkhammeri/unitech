<?php

namespace ClasseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TypeClasse
 *
 * @ORM\Table(name="type_classe")
 * @ORM\Entity(repositoryClass="ClasseBundle\Repository\TypeClasseRepository")
 */
class TypeClasse
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type_classe", type="string")
     */
    private $type;

    /**
     * typeclasse has many classes
     * @ORM\OneToMany(targetEntity="ClasseBundle\Entity\Classe", mappedBy="type_classe")
     */
    private $classes;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getClasses()
    {
        return $this->classes;
    }

    /**
     * @param mixed $classes
     */
    public function setClasses($classes)
    {
        $this->classes = $classes;
    }

    public function __toString()
    {
        return "" . $this->type;
    }


}

