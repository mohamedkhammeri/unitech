<?php

namespace ClasseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classe
 *
 * @ORM\Table(name="classe")
 * @ORM\Entity(repositoryClass="ClasseBundle\Repository\ClasseRepository")
 */
class Classe implements \JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_classe", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(name="num_classe",type="integer")
     */
    private $num_classe;

    /**
     * @var int
     * @ORM\Column(name="niveau",type="integer")
     */
    private $niveau;

    /**
     * @var int
     * @ORM\Column(name="capacité",type="integer")
     */
    private $capacite;

    /**
     * classe belongs to many students
     * @ORM\OneToMany(targetEntity="ClasseBundle\Entity\Etudiant", mappedBy="classe")
     */
    private $etudiants;

    /**
     * classe has many enseignants
     * @ORM\ManyToMany(targetEntity="ClasseBundle\Entity\Enseignant", mappedBy="classes")
     */
    private $enseignants;

    /**
     * classe belongs to one type_classe
     * @ORM\ManyToOne(targetEntity="ClasseBundle\Entity\TypeClasse", inversedBy="classes")
     */
    private $type_classe;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */

    public function getNumClasse()
    {
        return $this->num_classe;
    }

    /**
     * @param int $num_classe
     */
    public function setNumClasse($num_classe)
    {
        $this->num_classe = $num_classe;
    }

    /**
     * @return int
     */
    public function getNiveau()
    {
        return $this->niveau;
    }

    /**
     * @param int $niveau
     */
    public function setNiveau($niveau)
    {
        $this->niveau = $niveau;
    }

    /**
     * @return int
     */
    public function getCapacite()
    {
        return $this->capacite;
    }

    /**
     * @param int $capacite
     */
    public function setCapacite($capacite)
    {
        $this->capacite = $capacite;
    }

    /**
     * @return mixed
     */
    public function getEtudiants()
    {
        return $this->etudiants;
    }

    /**
     * @param mixed $etudiants
     */
    public function setEtudiants($etudiants)
    {
        $this->etudiants = $etudiants;
    }

    /**
     * @return mixed
     */
    public function getEnseignants()
    {
        return $this->enseignants;
    }

    /**
     * @param mixed $enseignants
     */
    public function setEnseignants($enseignants)
    {
        $this->enseignants = $enseignants;
    }

    /**
     * @return mixed
     */
    public function getTypeClasse()
    {
        return $this->type_classe;
    }

    /**
     * @param mixed $type_classe
     */
    public function setTypeClasse($type_classe)
    {
        $this->type_classe = $type_classe;
    }

    public function __toString()
    {
        return "" . $this->niveau . "  " . $this->type_classe . " " . $this->num_classe;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
            "id" => $this->id,
            "classe" => $this->num_classe,
            "type" => $this->type_classe."",
            "niveau" => $this->niveau,
            "capacite" => $this->capacite,
            "num" => $this->num_classe
        ];

    }
}

