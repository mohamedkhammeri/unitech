<?php

namespace ClasseBundle\Entity;

use AppBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Mgilet\NotificationBundle\Annotation\Notifiable;
use Mgilet\NotificationBundle\NotifiableInterface;

/**
 * Enseignant
 *
 * @ORM\Table(name="enseignant")
 * @ORM\Entity(repositoryClass="ClasseBundle\Repository\EnseignantRepository")
 * @Notifiable(name="teacher_notify")
 */
class Enseignant extends User implements NotifiableInterface, \JsonSerializable
{
    public function __construct()
    {
        parent::__construct();
        $this->addRole(User::ROLE_ENSEIGNANT);
    }

    /**
     * @ORM\ManyToMany(targetEntity="NoteBundle\Entity\Matiere", mappedBy="enseignants")
     */
    protected $matieres;

    /**
     * @ORM\ManyToMany(targetEntity="ClasseBundle\Entity\Classe", inversedBy="enseignants")
     * @ORM\JoinTable(
     *      joinColumns={@ORM\JoinColumn(referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(referencedColumnName="id_classe")}
     *      )
     */
    protected $classes;

    /**
     * @ORM\OneToMany(targetEntity="ReclamationBundle\Entity\ReclamationProf", mappedBy="prof")
     */
    protected $reclamations;

    /**
     * @return mixed
     */
    public function getMatieres()
    {
        return $this->matieres;
    }

    /**
     * @param mixed $matieres
     */
    public function setMatieres($matieres)
    {
        $this->matieres = $matieres;
    }

    /**
     * @return mixed
     */
    public function getClasses()
    {
        return $this->classes;
    }

    /**
     * @param mixed $classes
     */
    public function setClasses($classes)
    {
        $this->classes = $classes;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
            "id" => $this->id,
            "nom" => $this->getFirstName(),
            "CIN" => $this->getCIN(),
            "prenom" => $this->getLastName(),
        ];
    }
}

