<?php


namespace ClasseBundle\Fixtures;


use ClasseBundle\Entity\TypeClasse;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class TypeClasseFixtures implements FixtureInterface
{

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        for ($l = 'A'; $l < 'C'; $l++) {
            $typeClasse = new TypeClasse();
            $typeClasse->setType($l);
            $manager->persist($typeClasse);
        }

        $manager->flush();
    }
}