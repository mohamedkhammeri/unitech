<?php


namespace ClasseBundle\Controller;


use ClasseBundle\Entity\Etudiant;
use ClasseBundle\Entity\Permutation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PermutationFrontController
 * @package ClasseBundle\Controller
 * @Route("front/permutation")
 */
class PermutationFrontController extends Controller
{

    /**
     * Creates a new permutation entity.
     *
     * @Route("/new", name="permutation_front_new")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        //connecté ?
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED', null, 'Unable to access this page!');

        //etudiant ?
        if (!($this->getUser() instanceof Etudiant))
            throw $this->createNotFoundException($this->getUser() . " n'est pas un etudiant");

        $students = [];

        $em = $this->getDoctrine()->getManager();
        $studentsRepo = $em->getRepository("ClasseBundle:Etudiant");
        foreach ($studentsRepo->findAll() as $st) {
            if (($st->getClasse()->getNiveau() == $this->getUser()->getClasse()->getNiveau()) and
                ($st->getClasse()->getNumClasse() != $this->getUser()->getClasse()->getNumClasse())
            ) {
                array_push($students, $st);
            }
        }

        $permutation = new Permutation();
        $form = $this->createForm('ClasseBundle\Form\PermutationFrontType', $permutation);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $permutation->setStudent1($this->getUser());
            $student2 = $studentsRepo->find($request->get('your-student'));
            $permutation->setStudent2($student2);
            $em->persist($permutation);
            $em->flush();

            return $this->redirectToRoute('permutation_front_show', array('id' => $permutation->getId()));
        }

        return $this->render('@Classe/front/permutation/new.html.twig', array(
            'permutation' => $permutation,
            'students' => $students,
            'form' => $form->createView(),
        ));
    }

    /**
     * Success a new permutation entity.
     *
     * @Route("/show/{id}", name="permutation_front_show")
     * @Method({"GET"})
     */
    public function showAction(Request $request, Permutation $id)
    {
        //connecté ?
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED', null, 'Unable to access this page!');

        //etudiant ?
        if (!($this->getUser() instanceof Etudiant))
            throw $this->createNotFoundException($this->getUser() . " n'est pas un etudiant");

        return $this->render('@Classe/front/permutation/show.html.twig', [
            'permutation' => $id
        ]);

    }

}