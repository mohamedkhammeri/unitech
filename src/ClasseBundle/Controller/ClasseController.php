<?php

namespace ClasseBundle\Controller;

use ClasseBundle\Entity\Classe;
use Mgilet\NotificationBundle\Manager\NotificationManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Classe controller.
 *
 * @Route("crud")
 */
class ClasseController extends Controller
{

    /**
     * New class entity.
     *
     * @Route("/new/admin", name="classe_new_admin")
     * @Method("GET")
     */
    public function newAdminAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $students = $em->getRepository('ClasseBundle:Etudiant')->findBy(['classe' => null]);

        $classe = new Classe();
        $form = $this->createForm('ClasseBundle\Form\ClasseType', $classe);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $studentsClasse = [];

            if ($request->get('students'))
                foreach ($request->get('students') as $studentClasse) {
                    array_push($studentsClasse, $em->getRepository('ClasseBundle:Etudiant')->find($studentClasse));
                }

            $classe->setEtudiants($studentsClasse);
            $em->persist($classe);
            $em->flush();

            foreach ($studentsClasse as $studentClasse) {
                $studentClasse->setClasse($classe);
                $em->persist($studentClasse);
            }
            $em->flush();

            return $this->redirectToRoute('classe_show_id', array('id' => $classe->getId()));
        }

        return $this->render('@Classe/back/classe/new.html.twig', [
            'students' => $students,
            'classe' => $classe,
            'form' => $form->createView(),
        ]);
    }

    /**
     * edit classe entity.
     *
     * @Route("/edit/{classe}/admin", name="classe_edit_admin")
     * @Method("GET")
     */
    public function editAdminAction(Request $request, Classe $classe)
    {

        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm('ClasseBundle\Form\ClasseType', $classe);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($classe);
            $em->flush();

            return $this->redirectToRoute('classe_show_id', array('id' => $classe->getId()));
        }

        return $this->render('@Classe/back/classe/edit.html.twig', [
            'classe' => $classe,
            'form' => $form->createView(),
        ]);
    }

    /**
     * New classe entity.
     *
     * @Route("/add_student/{classe}/admin", name="classe_add_student_admin")
     * @Method("GET")
     */
    public function addStudentAction(Request $request, Classe $classe)
    {
        $em = $this->getDoctrine()->getManager();
        $students = $em->getRepository('ClasseBundle:Etudiant')->findBy(['classe' => null]);

        return $this->render('@Classe/back/classe/add_student.html.twig', [
            'students' => $students,
            'classe' => $classe
        ]);
    }

    /**
     * @var NotificationManager
     */
    private $manager;

    /**
     * New student/classe entity.
     *
     * @Route("/add_student/{classe}/admin/new", name="classe_add_new_student_admin")
     * @Method("POST")
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function submitNewStudentAction(Request $request, Classe $classe)
    {
        $em = $this->getDoctrine()->getManager();
        $this->manager = $this->get('mgilet.notification');

        $studentsClasse = [];

        if ($request->get('students'))
            foreach ($request->get('students') as $studentClasse) {
                array_push($studentsClasse, $em->getRepository('ClasseBundle:Etudiant')->find($studentClasse));
            }

        $classe->setEtudiants($studentsClasse);
        $em->persist($classe);
        $em->flush();

        //notification-bundle
        $notif = $this->manager->createNotification('Welcome to ' . $classe . ' class !', 'Your class has ' . $classe->getCapacite() . ' students', null);

        foreach ($studentsClasse as $studentClasse) {
            $studentClasse->setClasse($classe);
            $em->persist($studentClasse);
        }
        $this->manager->addNotification($studentsClasse, $notif, true);
        $em->flush();

        return $this->redirectToRoute('classe_show_id', array('id' => $classe->getId()));
    }

    /**
     * Deletes a classe entity.
     *
     * @Route("/admin/delete/{id}/classe", name="classe_delete_admin")
     */
    public function deleteClasseAction(Request $request, Classe $classe)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($classe);
        $em->flush();

        return $this->redirectToRoute('classe_all');
    }

    /**
     * Lists all classes entities.
     *
     * @Route("/all", name="classe_all")
     * @Method("GET")
     */
    public function allAction()
    {
        $em = $this->getDoctrine()->getManager();

        $classes = $em->getRepository('ClasseBundle:Classe')->findAll();

        return $this->render('@Classe/back/classe/all.html.twig', array(
            'classes' => $classes,
        ));
    }

    /**
     * Show reclamation.
     *
     * @Route("/classe_details/{id}", name="classe_show_id")
     * @Method("GET")
     */
    public function showRecAction(Request $request, Classe $classe)
    {
        return $this->render('@Classe/back/classe/show.html.twig', array(
            'classe' => $classe,
        ));
    }

    /**
     * Lists all classe entities.
     *
     * @Route("/", name="classe_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $classes = $em->getRepository('ClasseBundle:Classe')->findAll();

        return $this->render('classe/index.html.twig', array(
            'classes' => $classes,
        ));
    }

    /**
     * Creates a new classe entity.
     *
     * @Route("/new", name="classe_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $classe = new Classe();
        $form = $this->createForm('ClasseBundle\Form\ClasseType', $classe);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($classe);
            $em->flush();

            return $this->redirectToRoute('classe_show', array('id' => $classe->getId()));
        }

        return $this->render('classe/new.html.twig', array(
            'classe' => $classe,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a classe entity.
     *
     * @Route("/{id}", name="classe_show")
     * @Method("GET")
     */
    public function showAction(Classe $classe)
    {
        $deleteForm = $this->createDeleteForm($classe);

        return $this->render('classe/show.html.twig', array(
            'classe' => $classe,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing classe entity.
     *
     * @Route("/{id}/edit", name="classe_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Classe $classe)
    {
        $deleteForm = $this->createDeleteForm($classe);
        $editForm = $this->createForm('ClasseBundle\Form\ClasseType', $classe);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('classe_edit', array('id' => $classe->getId()));
        }

        return $this->render('classe/edit.html.twig', array(
            'classe' => $classe,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a classe entity.
     *
     * @Route("/{id}", name="classe_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Classe $classe)
    {
        $form = $this->createDeleteForm($classe);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($classe);
            $em->flush();
        }

        return $this->redirectToRoute('classe_index');
    }

    /**
     * Creates a form to delete a classe entity.
     *
     * @param Classe $classe The classe entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Classe $classe)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('classe_delete', array('id' => $classe->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
