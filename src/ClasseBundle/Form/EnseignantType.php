<?php

namespace ClasseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EnseignantType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('first_Name', TextType::class, [
            'required' => true,
            'label' => 'First name'
        ])->add('last_Name', TextType::class, [
            'required' => true,
            'label' => 'Last name'
        ])->add('phone_number', NumberType::class, [
            'required' => true,
            'label' => 'Phone number'
        ])->add('CIN', NumberType::class, [
            'required' => true,
            'label' => 'CIN'
        ])->add('classes');
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ClasseBundle\Entity\Enseignant'
        ));
    }

    public function getParent()
    {
        return 'AppBundle\Form\RegistrationType';
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'classebundle_enseignant';
    }


}
