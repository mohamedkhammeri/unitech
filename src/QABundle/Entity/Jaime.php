<?php

namespace QABundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Jaime
 *
 * @ORM\Table(name="jaime")
 * @ORM\Entity
 */
class Jaime
{
    /**
     * @var integer
     *
     * @ORM\Column(name="Id_User", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idUser;

    /**
     * @var integer
     *
     * @ORM\Column(name="Id_Sujet", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idSujet;

    /**
     * @var integer
     *
     * @ORM\Column(name="valeur_jaime", type="integer", nullable=false)
     */
    private $valeurJaime;



    /**
     * Set idUser
     *
     * @param integer $idUser
     *
     * @return Jaime
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return integer
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set idSujet
     *
     * @param integer $idSujet
     *
     * @return Jaime
     */
    public function setIdSujet($idSujet)
    {
        $this->idSujet = $idSujet;

        return $this;
    }

    /**
     * Get idSujet
     *
     * @return integer
     */
    public function getIdSujet()
    {
        return $this->idSujet;
    }

    /**
     * Set valeurJaime
     *
     * @param integer $valeurJaime
     *
     * @return Jaime
     */
    public function setValeurJaime($valeurJaime)
    {
        $this->valeurJaime = $valeurJaime;

        return $this;
    }

    /**
     * Get valeurJaime
     *
     * @return integer
     */
    public function getValeurJaime()
    {
        return $this->valeurJaime;
    }
}
