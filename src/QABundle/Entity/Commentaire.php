<?php

namespace QABundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Commentaire
 *
 * @ORM\Table(name="commentaire")
 * @ORM\Entity
 */
class Commentaire
{
    /**
     * @var integer
     *
     * @ORM\Column(name="Id_Commentaire", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idCom;

    /**
     * @var integer
     *
     * @ORM\Column(name="Id_Sujet", type="integer", nullable=false)
     */
    private $idF;

    /**
     * @var integer
     *
     * @ORM\Column(name="Id_User ", type="integer", nullable=false)
     */
    private $idUser;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Temps_Commentaire", type="date", nullable=false)
     */
    private $dateCom;

    /**
     * @var string
     *
     * @ORM\Column(name="Contenu_Commentaire", type="string", length=255, nullable=false)
     */
    private $descriptionCom;


    /**
     * Get idCom
     *
     * @return integer
     */
    public function getIdCom()
    {
        return $this->idCom;
    }

    /**
     * Set idF
     *
     * @param integer $idF
     *
     * @return Commentaire
     */
    public function setIdF($idF)
    {
        $this->idF = $idF;

        return $this;
    }

    /**
     * Get idF
     *
     * @return integer
     */
    public function getIdF()
    {
        return $this->idF;
    }

    /**
     * Set idUser
     *
     * @param integer $idUser
     *
     * @return Commentaire
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return integer
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set dateCom
     *
     * @param \DateTime $dateCom
     *
     * @return Commentaire
     */
    public function setDateCom($dateCom)
    {
        $this->dateCom = $dateCom;

        return $this;
    }

    /**
     * Get dateCom
     *
     * @return \DateTime
     */
    public function getDateCom()
    {
        return $this->dateCom;
    }

    /**
     * Set descriptionCom
     *
     * @param string $descriptionCom
     *
     * @return Commentaire
     */
    public function setDescriptionCom($descriptionCom)
    {
        $this->descriptionCom = $descriptionCom;

        return $this;
    }

    /**
     * Get descriptionCom
     *
     * @return string
     */
    public function getDescriptionCom()
    {
        return $this->descriptionCom;
    }
}
