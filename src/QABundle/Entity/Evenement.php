<?php

namespace QABundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Evenement
 *
 * @ORM\Table(name="evenement", indexes={@ORM\Index(name="Id_User", columns={"Id_User"})})
 * @ORM\Entity
 */
class Evenement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="Id_Event", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idEvent;

    /**
     * @var string
     *
     * @ORM\Column(name="Descr_Event", type="string", length=255, nullable=false)
     */
    private $descrEvent;

    /**
     * @var string
     *
     * @ORM\Column(name="Image_Event", type="string", length=255, nullable=false)
     */
    private $imageEvent;

    /**
     * @var string
     *
     * @ORM\Column(name="Titre_Event", type="string", length=255, nullable=false)
     */
    private $titreEvent;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DATED_EVENT", type="date", nullable=false)
     */
    private $datedEvent;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DATEF_EVENT", type="date", nullable=false)
     */
    private $datefEvent;

    /**
     * @var string
     *
     * @ORM\Column(name="EMPLACEMENT", type="string", length=255, nullable=false)
     */
    private $emplacement;

    /**
     * @var integer
     *
     * @ORM\Column(name="Id_User", type="integer", nullable=false)
     */
    private $idUser;

    /**
     * @var string
     *
     * @ORM\Column(name="categorie_Event", type="string", length=255, nullable=false)
     */
    private $categorieEvent;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbr_place_E", type="integer", nullable=false)
     */
    private $nbrPlaceE;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbr_r", type="integer", nullable=true)
     */
    private $nbrR;



    /**
     * Get idEvent
     *
     * @return integer
     */
    public function getIdEvent()
    {
        return $this->idEvent;
    }

    /**
     * Set descrEvent
     *
     * @param string $descrEvent
     *
     * @return Evenement
     */
    public function setDescrEvent($descrEvent)
    {
        $this->descrEvent = $descrEvent;

        return $this;
    }

    /**
     * Get descrEvent
     *
     * @return string
     */
    public function getDescrEvent()
    {
        return $this->descrEvent;
    }

    /**
     * Set imageEvent
     *
     * @param string $imageEvent
     *
     * @return Evenement
     */
    public function setImageEvent($imageEvent)
    {
        $this->imageEvent = $imageEvent;

        return $this;
    }

    /**
     * Get imageEvent
     *
     * @return string
     */
    public function getImageEvent()
    {
        return $this->imageEvent;
    }

    /**
     * Set titreEvent
     *
     * @param string $titreEvent
     *
     * @return Evenement
     */
    public function setTitreEvent($titreEvent)
    {
        $this->titreEvent = $titreEvent;

        return $this;
    }

    /**
     * Get titreEvent
     *
     * @return string
     */
    public function getTitreEvent()
    {
        return $this->titreEvent;
    }

    /**
     * Set datedEvent
     *
     * @param \DateTime $datedEvent
     *
     * @return Evenement
     */
    public function setDatedEvent($datedEvent)
    {
        $this->datedEvent = $datedEvent;

        return $this;
    }

    /**
     * Get datedEvent
     *
     * @return \DateTime
     */
    public function getDatedEvent()
    {
        return $this->datedEvent;
    }

    /**
     * Set datefEvent
     *
     * @param \DateTime $datefEvent
     *
     * @return Evenement
     */
    public function setDatefEvent($datefEvent)
    {
        $this->datefEvent = $datefEvent;

        return $this;
    }

    /**
     * Get datefEvent
     *
     * @return \DateTime
     */
    public function getDatefEvent()
    {
        return $this->datefEvent;
    }

    /**
     * Set emplacement
     *
     * @param string $emplacement
     *
     * @return Evenement
     */
    public function setEmplacement($emplacement)
    {
        $this->emplacement = $emplacement;

        return $this;
    }

    /**
     * Get emplacement
     *
     * @return string
     */
    public function getEmplacement()
    {
        return $this->emplacement;
    }

    /**
     * Set idUser
     *
     * @param integer $idUser
     *
     * @return Evenement
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return integer
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set categorieEvent
     *
     * @param string $categorieEvent
     *
     * @return Evenement
     */
    public function setCategorieEvent($categorieEvent)
    {
        $this->categorieEvent = $categorieEvent;

        return $this;
    }

    /**
     * Get categorieEvent
     *
     * @return string
     */
    public function getCategorieEvent()
    {
        return $this->categorieEvent;
    }

    /**
     * Set nbrPlaceE
     *
     * @param integer $nbrPlaceE
     *
     * @return Evenement
     */
    public function setNbrPlaceE($nbrPlaceE)
    {
        $this->nbrPlaceE = $nbrPlaceE;

        return $this;
    }

    /**
     * Get nbrPlaceE
     *
     * @return integer
     */
    public function getNbrPlaceE()
    {
        return $this->nbrPlaceE;
    }

    /**
     * Set nbrR
     *
     * @param integer $nbrR
     *
     * @return Evenement
     */
    public function setNbrR($nbrR)
    {
        $this->nbrR = $nbrR;

        return $this;
    }

    /**
     * Get nbrR
     *
     * @return integer
     */
    public function getNbrR()
    {
        return $this->nbrR;
    }
}
