<?php

namespace QABundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert ;
/**
 * Sujet
 *
 * @ORM\Table(name="sujet", indexes={@ORM\Index(name="Id_User", columns={"id_user"})})
 * @ORM\Entity
 */
class Sujet
{
    /**
     * @var integer
     *
     * @ORM\Column(name="Id_Sujet", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idF;

    /**
     * @var integer
     *
     * @ORM\Column(name="Id_User", type="integer", nullable=false)
     */
    private $idUser;

    /**
     * @var integer
     *
     * @ORM\Column(name="Etat", type="integer", nullable=false)
     */
    private $etat;

    /**
     * @var string
     * @Assert\NotBlank(message="description ne peut pas etre vide")
     * @ORM\Column(name="Contenu_Sujet", type="string", length=255, nullable=false)
     */
    private $descriptionF;
    /**
     * @var string
     * @Assert\NotBlank(message="Titre ne peut pas etre vide")
     * @ORM\Column(name="Titre_Sujet", type="string", length=255, nullable=false)
     */
    private $titreF;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    private $date;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbre_jaime", type="integer", nullable=true)
     */
    private $nbreJaime;



    /**
     * Get idF
     *
     * @return integer
     */
    public function getIdF()
    {
        return $this->idF;
    }

    /**
     * Set idUser
     *
     * @param integer $idUser
     *
     * @return Sujet
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return integer
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set etat
     *
     * @param integer $etat
     *
     * @return Sujet
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return integer
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set descriptionF
     *
     * @param string $descriptionF
     *
     * @return Sujet
     */
    public function setDescriptionF($descriptionF)
    {
        $this->descriptionF = $descriptionF;

        return $this;
    }

    /**
     * Get descriptionF
     *
     * @return string
     */
    public function getDescriptionF()
    {
        return $this->descriptionF;
    }
    /**
     * Set titreF
     *
     * @param string $titreF
     *
     * @return Sujet
     */
    public function setTitreF($titreF)
    {
        $this->titreF = $titreF;

        return $this;
    }

    /**
     * Get titreF
     *
     * @return string
     */
    public function getTitreF()
    {
        return $this->titreF;
    }
    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Sujet
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set nbreJaime
     *
     * @param integer $nbreJaime
     *
     * @return Sujet
     */
    public function setNbreJaime($nbreJaime)
    {
        $this->nbreJaime = $nbreJaime;

        return $this;
    }

    /**
     * Get nbreJaime
     *
     * @return integer
     */
    public function getNbreJaime()
    {
        return $this->nbreJaime;
    }
}
