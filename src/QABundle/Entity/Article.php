<?php

namespace QABundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Article
 *
 * @ORM\Table(name="article", indexes={@ORM\Index(name="Id_User", columns={"Id_User"}), @ORM\Index(name="Titre_Event", columns={"Titre_Event"})})
 * @ORM\Entity
 */
class Article
{
    /**
     * @var integer
     *
     * @ORM\Column(name="Id_Article", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idArticle;

    /**
     * @var integer
     *
     * @ORM\Column(name="Id_User", type="integer", nullable=false)
     */
    private $idUser;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom_Article", type="string", length=255, nullable=false)
     */
    private $nomArticle;

    /**
     * @var string
     *
     * @ORM\Column(name="Contenu_Article", type="string", length=255, nullable=false)
     */
    private $contenuArticle;

    /**
     * @var string
     *
     * @ORM\Column(name="Image_Article", type="string", length=255, nullable=false)
     */
    private $imageArticle;

    /**
     * @var string
     *
     * @ORM\Column(name="Titre_Event", type="string", length=255, nullable=false)
     */
    private $titreEvent;

    /**
     * @var integer
     *
     * @ORM\Column(name="Edition", type="integer", nullable=false)
     */
    private $edition;

    /**
     * @var string
     *
     * @ORM\Column(name="Date_Article", type="string", length=255, nullable=false)
     */
    private $dateArticle;



    /**
     * Get idArticle
     *
     * @return integer
     */
    public function getIdArticle()
    {
        return $this->idArticle;
    }

    /**
     * Set idUser
     *
     * @param integer $idUser
     *
     * @return Article
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return integer
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set nomArticle
     *
     * @param string $nomArticle
     *
     * @return Article
     */
    public function setNomArticle($nomArticle)
    {
        $this->nomArticle = $nomArticle;

        return $this;
    }

    /**
     * Get nomArticle
     *
     * @return string
     */
    public function getNomArticle()
    {
        return $this->nomArticle;
    }

    /**
     * Set contenuArticle
     *
     * @param string $contenuArticle
     *
     * @return Article
     */
    public function setContenuArticle($contenuArticle)
    {
        $this->contenuArticle = $contenuArticle;

        return $this;
    }

    /**
     * Get contenuArticle
     *
     * @return string
     */
    public function getContenuArticle()
    {
        return $this->contenuArticle;
    }

    /**
     * Set imageArticle
     *
     * @param string $imageArticle
     *
     * @return Article
     */
    public function setImageArticle($imageArticle)
    {
        $this->imageArticle = $imageArticle;

        return $this;
    }

    /**
     * Get imageArticle
     *
     * @return string
     */
    public function getImageArticle()
    {
        return $this->imageArticle;
    }

    /**
     * Set titreEvent
     *
     * @param string $titreEvent
     *
     * @return Article
     */
    public function setTitreEvent($titreEvent)
    {
        $this->titreEvent = $titreEvent;

        return $this;
    }

    /**
     * Get titreEvent
     *
     * @return string
     */
    public function getTitreEvent()
    {
        return $this->titreEvent;
    }

    /**
     * Set edition
     *
     * @param integer $edition
     *
     * @return Article
     */
    public function setEdition($edition)
    {
        $this->edition = $edition;

        return $this;
    }

    /**
     * Get edition
     *
     * @return integer
     */
    public function getEdition()
    {
        return $this->edition;
    }

    /**
     * Set dateArticle
     *
     * @param string $dateArticle
     *
     * @return Article
     */
    public function setDateArticle($dateArticle)
    {
        $this->dateArticle = $dateArticle;

        return $this;
    }

    /**
     * Get dateArticle
     *
     * @return string
     */
    public function getDateArticle()
    {
        return $this->dateArticle;
    }
}
