<?php

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$collection = new RouteCollection();

$collection->add('evenement_index', new Route(
    '/',
    array('_controller' => 'QABundle:Evenement:index'),
    array(),
    array(),
    '',
    array(),
    array('GET')
));

$collection->add('evenement_show', new Route(
    '/{idEvent}/show',
    array('_controller' => 'QABundle:Evenement:show'),
    array(),
    array(),
    '',
    array(),
    array('GET')
));

$collection->add('evenement_new', new Route(
    '/new',
    array('_controller' => 'QABundle:Evenement:new'),
    array(),
    array(),
    '',
    array(),
    array('GET', 'POST')
));

$collection->add('evenement_edit', new Route(
    '/{idEvent}/edit',
    array('_controller' => 'QABundle:Evenement:edit'),
    array(),
    array(),
    '',
    array(),
    array('GET', 'POST')
));

$collection->add('evenement_delete', new Route(
    '/{idEvent}/delete',
    array('_controller' => 'QABundle:Evenement:delete'),
    array(),
    array(),
    '',
    array(),
    array('DELETE')
));

return $collection;
