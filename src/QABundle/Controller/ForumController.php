<?php

namespace QABundle\Controller;

use ClasseBundle\Entity\Etudiant;
use Snipe\BanBuilder\CensorWords;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use QABundle\Entity\Commentaire;
use QABundle\Entity\Rating;
use QABundle\Entity\Sujet;
use AppBundle\Entity\User;


class ForumController extends Controller
{
    public function indexAction(Request $request)
    {


        $user=$this->etudiant = $this->get('security.token_storage')->getToken()->getUser();




        $em = $this->getDoctrine()->getManager();

        $RAW_QUERY = "SELECT * FROM sujet WHERE id_user !='$user' ORDER BY date DESC" ;

        $statement = $em->getConnection()->prepare($RAW_QUERY);
        $statement->execute();

        $forums = $statement->fetchAll();


        $forums  = $this->get('knp_paginator')->paginate(
            $forums,
            $request->query->get('page', 1)/*le numéro de la page à afficher*/,
            5/*nbre d'éléments par page*/
        );

        $censor = new CensorWords;
        return $this->render('forum/index.html.twig',array(
            'forums' => $forums,
            'user' => $user,
            'censorF' => $censor
        ));
    }

    public function addAction(Request $request)
    {


        //$em = $this->getDoctrine()->getManager();

        $user=$this->etudiant = $this->get('security.token_storage')->getToken()->getUser();


        // var_dump($user);
        $forum = new Sujet();
        $form = $this->createFormBuilder($forum)
           // ->add('descriptionCom',TextType::class,array('attr'=>array('class'=>'form-control') ))

          //  ->add('save',SubmitType::class,array('label'=>'Ajouter Comment','attr'=>array('class'=>'btn btn-primary') ))
          //  ->getForm();
          //  ->add('descriptionF',CKEditorType::class,array('attr'=>array('class'=>'form-control') ))
           ->add('titreF',TextType::class,array('attr'=>array('class'=>'form-control') ))

            ->add('descriptionF',TextType::class,array('attr'=>array('class'=>'form-control') ))

            ->add('save',SubmitType::class,array('label'=>'Ajouter Sujet','attr'=>array('class'=>'btn btn-primary') ))
            ->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted()&&$form->isValid())
        {
            $desc=$form['descriptionF']->getData();

            $forum->setDescriptionF($desc);
            $forum->setDate(new \DateTime("now"));
            $forum->setNbreJaime(0);
            $user = $this->getUser();
            $forum->setIdUser($user->getId());
            $forum->setEtat(0);

            $em = $this->getDoctrine()->getManager();
            //   var_dump($time);
            $em->persist($forum);
            $em->flush();

            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Le forum a été ajouté avec succées ...!');
            $this->addFlash(
                'success',
                "Votre <strong> Question </strong> a bien eté posée !"
            );
            return $this->redirectToRoute('mesForums');
        }

        return $this->render('forum/ajout.html.twig',array( 'user' => $user,'form'=>$form->createView()));

    }


    public function detailsAction(Request $request,$id)
    {


        //$em = $this->getDoctrine()->getManager();

        $user=$this->etudiant = $this->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();



        $forum = $this->getDoctrine()->getRepository('QABundle:Sujet')->find($id);

        $comments = $em->getRepository('QABundle:Commentaire')->findBy([
            "idF"=>$id
        ]);


        $comment = new Commentaire();
        $form = $this->createFormBuilder($comment)

            ->add('descriptionCom',TextType::class,array('attr'=>array('class'=>'form-control') ))

            ->add('save',SubmitType::class,array('label'=>'Ajouter Comment','attr'=>array('class'=>'btn btn-primary') ))
            ->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted()&&$form->isValid()) {
            $desc=$form['descriptionCom']->getData();

            $comment->setDescriptionCom($desc);
            $comment->setDateCom(new \DateTime("now"));
            $comment->setIdUser($user->getId());
            $comment->setIdF($forum->getIdF());
            // var_dump($comment);
            $em = $this->getDoctrine()->getManager();

            $em->persist($comment);
            $em->flush();

            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Commentaire a été ajouté avec succées ...!');

            return $this->redirectToRoute('detailsF',array('id' => $id));
        }


        return $this->render('forum/details.html.twig',array(
            'forum' => $forum,
            'comments'=>$comments,
            'user' => $user,
            'form'=>$form->createView()
        ));
    }

    public function modifierAction(Request $request , $id)
    {

        //$em = $this->getDoctrine()->getManager();

        $user=$this->etudiant = $this->get('security.token_storage')->getToken()->getUser();
        // $em = $this->getDoctrine()->getManager();
        $forum = $this->getDoctrine()->getRepository('QABundle:Sujet')->find($id);

// ->add('descriptionF',CKEditorType::class,array('attr'=>array('class'=>'form-control') ))
        $form = $this->createFormBuilder($forum)

            ->add('descriptionF',TextType::class,array('attr'=>array('class'=>'form-control') ))

            ->add('save',SubmitType::class,array('label'=>'save' ))
            ->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted()&&$form->isValid())
        {
            $desc=$form['descriptionF']->getData();
            $forum->setDescriptionF($desc);
            $forum->setDate(new \DateTime("now"));
            $forum->setIdUser($user->getId());

            $em = $this->getDoctrine()->getManager();
            $em->persist($forum);
            $em->flush();
            //return $this->redirectToRoute('index_back');

            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Lévénement a été modifié avec succées ...!');


            $url = $this->generateUrl('mesForums');

            return $this->redirect($url);
        }
        $em = $this->getDoctrine()->getManager();
        return $this->render('forum/modifier.html.twig',array(
            'forum' => $forum,
            'user' => $user,
            'form'=>$form->createView()
        ));
    }

    public function supprimerAction($id,Request $request)
    {



        $forum = $this->getDoctrine()->getRepository('QABundle:Sujet')->find($id);

        $em =$this->getDoctrine()->getManager();
        $em->remove($forum);
        $em->flush();
        $request->getSession()
            ->getFlashBag()
            ->add('success', 'Le forum a été supprimer avec succées ...!');

        return $this->redirectToRoute('mesForums');
    }




    public function mesForumsAction()
    {


        //$em = $this->getDoctrine()->getManager();

        $user=$this->etudiant = $this->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $forums = $em->getRepository('QABundle:Sujet')->findBy([
            "idUser"=>$user
        ]);

        foreach ($forums as $forum) {

            $comments = $em->getRepository('QABundle:Commentaire')->findBy([
                "idF"=>$forum->getIdF()
            ]);


        }



        $em = $this->getDoctrine()->getManager();
        return $this->render('forum/mesforums.html.twig',array(
            'forums' => $forums,
            'user' => $user,
        ));
    }



    public function chartAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $forums = $em->getRepository('QABundle:Sujet')->findAll();

        foreach ($forums as $forum) {

            $comments = $em->getRepository('QABundle:Commentaire')->findBy([
                "idF"=>$forum->getIdF()
            ]);

          //  $forum->setCountComments(count($comments));

        }
        $em = $this->getDoctrine()->getManager();
        return $this->render('forum/chart.html.twig', array(
            'forums' => $forums,
            'user' => $user,
        ));
    }


    public function backAction()
    {
        $em = $this->getDoctrine()->getManager();

        $publicites = $em->getRepository('QABundle:Sujet')->findBy(array('etat' =>0));

        return $this->render('forum/back.html.twig', array(
            'publicites' => $publicites,

        ));
    }

    public function validerAction(Request $request, Sujet $forum)
    {
        $em = $this->getDoctrine()->getManager();
        $forum->setEtat(1);
        $this->getDoctrine()->getManager()->flush();
        $publicites = $em->getRepository('QABundle:Sujet')->findBy(array('etat' =>0));
        return $this->redirectToRoute('back_forum', array(
            'publicites' => $publicites,

        ));
    }

    public function refuserAction(Request $request, Sujet $forum)
    {
        $em = $this->getDoctrine()->getManager();
        $forum->setEtat(-1);
        $this->getDoctrine()->getManager()->flush();
        $publicites = $em->getRepository('QABundle:Sujet')->findBy(array('etat' =>0));
        return $this->redirectToRoute('back_forum', array(
            'publicites' => $publicites,

        ));
    }

    public function likeAction(Request $request)
    {
        $id=$request->get('id');




        //$em = $this->getDoctrine()->getManager();

        $user=$this->etudiant = $this->get('security.token_storage')->getToken()->getUser();
        $forum = $this->getDoctrine()->getRepository('QABundle:Sujet')->find($id);


        $em = $this->getDoctrine()->getManager();

        $comments = $em->getRepository('QABundle:Commentaire')->findBy([
            "idF"=>$id
        ]);
        $comment = new Commentaire();
        $form = $this->createFormBuilder($comment)

            ->add('descriptionCom',TextType::class,array('attr'=>array('class'=>'form-control') ))

            ->add('save',SubmitType::class,array('label'=>'Ajouter Comment','attr'=>array('class'=>'btn btn-primary') ))
            ->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted()&&$form->isValid()) {
            $desc=$form['descriptionCom']->getData();

            $comment->setDescriptionCom($desc);
            $comment->setDateCom(new \DateTime("now"));
            $comment->setIdUser($user);
            $comment->setIdF($forum->getIdF());
            // var_dump($comment);
            $em = $this->getDoctrine()->getManager();

            $em->persist($comment);
            $em->flush();

            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Commentaire a été ajouté avec succées ...!');
          //  return $this->render('forum/details.html.twig', ['id' => $id, 'user'=>$user]);

            return $this->redirectToRoute('detailsF',array('id' => $id,'user'=>$user));
        }





        $ancien=$forum->getNbreJaime();

        $forum->setNbreJaime($ancien + 1 );


        $em->persist($forum);
        $em->flush();

        return $this->render('forum/details.html.twig', ['forum' => $forum, 'comments'=>$comments,'form'=>$form->createView(),'user'=>$user]);


    }



    public function RatAction(Request $request)
    {

        $id = $request->get('id');
        $note=$request->get('note');
        $user=$this->etudiant = $this->get('security.token_storage')->getToken()->getUser();
        $ok=$this->etudiant = $this->get('security.token_storage')->getToken()->getUser()->getId();
        $forum = $this->getDoctrine()->getRepository('QABundle:Sujet')->find($id);


        $em = $this->getDoctrine()->getManager();

        $comments = $em->getRepository('QABundle:Commentaire')->findBy([
            "idF"=>$id
        ]);
        $comment = new Commentaire();
        $form = $this->createFormBuilder($comment)

            ->add('descriptionCom',TextType::class,array('attr'=>array('class'=>'form-control') ))

            ->add('save',SubmitType::class,array('label'=>'Ajouter Comment','attr'=>array('class'=>'btn btn-primary') ))
            ->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted()&&$form->isValid()) {
            $desc=$form['descriptionCom']->getData();

            $comment->setDescriptionCom($desc);
            $comment->setDateCom(new \DateTime("now"));
            $comment->setIdUser($user);
            $comment->setIdF($forum->getIdF());
            // var_dump($comment);
            $em = $this->getDoctrine()->getManager();

            $em->persist($comment);
            $em->flush();

            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Commentaire a été ajouté avec succées ...!');

            return $this->redirectToRoute('detailsF',array('id' => $id));
        }




        if($request->isXmlHttpRequest()) {

            $star = new Rating();

            $star->setNote($note);


            $em5 = $this->getDoctrine()->getManager();
            $idp=$em5->find('QABundle:Sujet', $id);
            $star->setIdPub($idp);
            $em = $this->getDoctrine()->getManager();
            $star->setIdUser($ok);

            $em->persist($star);
            $em->flush();
            return $this->render('forum/details.html.twig', ['forum' => $forum, 'comments'=>$comments,'form'=>$form->createView(),'user'=>$user]);

        }

    }



    public function supprimerCAction($id,Request $request)
    {

        $user = $this->container->get('security.token_storage')->getToken()->getUser();


        $com = $this->getDoctrine()->getRepository('QABundle:Commentaire')->find($id);

        $em =$this->getDoctrine()->getManager();
        $em->remove($com);
        $em->flush();

        return $this->redirectToRoute('detailsF',array('id' => $com->getIdF()));
    }

    public function modifierCAction(Request $request,$id)
    {
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();

        $com = $this->getDoctrine()->getRepository('QABundle:Commentaire')->find($id);
        $com->setDescriptionCom($com->getDescriptionCom());
        $forum = $this->getDoctrine()->getRepository('QABundle:Sujet')->find($com->getIdF());

        $form = $this->createFormBuilder($com)

            ->add('descriptionCom',TextType::class,array('attr'=>array('class'=>'form-control') ))

            ->add('save',SubmitType::class,array('label'=>'modifier Comment','attr'=>array('class'=>'btn btn-primary') ))
            ->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted()&&$form->isValid()) {
            $desc=$form['descriptionCom']->getData();

            $com->setDescriptionCom($desc);
            $com->setDateCom($com->getDateCom());
            $com->setIdUser($user->getId());
            $com->setIdF($com->getIdF());
            // var_dump($comment);
            $em = $this->getDoctrine()->getManager();
            $em->persist($com);
            $em->flush();

            return $this->redirectToRoute('detailsF',array('id' => $com->getIdF()));
        }


        return $this->render('forum/modifierC.html.twig',array(
            'form'=>$form->createView(),
            'forum' => $forum,
            'user' => $user,

        ));
    }


}
