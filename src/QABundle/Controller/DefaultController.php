<?php

namespace QABundle\Controller;

use ClasseBundle\Entity\Etudiant;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use QABundle\Entity\Evenement;
use QABundle\Entity\User;
use QABundle\Form\EvenementType;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('@QA/Default/index.html.twig');
    }
    public function READ2Action()
    {
        $clubs=$this->getDoctrine()->getRepository(Evenement::class)->findAll();

        return $this->render ("@QA/Default/index.html.twig",array('clubs'=>$clubs));
    }
    public function homepageAction()
    {$a=1;
        $membre=$this->container->get('security.token_storage')->getToken()->getUser();
       if( $this->container->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))

        return $this->render('@QA/Default/admin.html.twig');
else
    return $this->render('@QA/Default/home.html.twig',array('member'=>$a));

    }

    public function crudeAction()
    {


        return $this->render ("@QA/Default/crud_e.html.twig");
    }

    public function showeAction()
    {
        //$membre=$this->container->get('security.token_storage')->getToken()->getUser()->getId();
        $em = $this->getDoctrine()->getManager();

        $evenements = $em->getRepository('QABundle:Evenement')->findAll();

        return $this->render('@QA/Default/crud_e.html.twig', array(
            'evenements' => $evenements
        ));


    }

    public function supprimerevAction($id) {

        $em = $this->getDoctrine()->getManager();
        $club = $em->getRepository(Evenement::class)->find($id);
        $em->remove($club);
        $em->flush();
        return $this->redirectToRoute("showeeeee");
    }
    public function modifierevAction($id , Request $request) {
        $club = new Evenement();
        $em = $this->getDoctrine()->getManager();
        $club = $em->getRepository(Evenement::class)->find($id);
        $form = $this->createForm(EvenementType::class,$club);
        $form = $form->handleRequest($request);
        if ($form->isSubmitted())
        {
            $em->persist($club);
            $em->flush();
            return $this->redirectToRoute("showeeeee");
        }
        return $this->render('@QA/Default/updateev.html.twig',array('form' => $form->createView()));


    }

    public function addeAction(Request $request){


        //$em = $this->getDoctrine()->getManager();
         $membre=$this->etudiant = $this->get('security.token_storage')->getToken()->getUser()->getId();
        $club = new Evenement();
        $form = $this->createForm(EvenementType::class,$club);
        $form = $form->handleRequest($request);
        if ($form->isSubmitted() ) {

            $file= $request->files->get('qabundle_evenement')['imageEvent'];
            $uploads_directory=$this->getParameter('uploads_directory');


           //var_dump($file);
            $imageEvent=$file->getClientoriginalName();

            $em = $this->getDoctrine()->getManager();

            $file->move($uploads_directory,$imageEvent);
            $club->setImageEvent($imageEvent);
            $club->setIdUser($membre );
            $em->persist($club);
            $em->flush();
            return $this->redirectToRoute("showeeeee");
        }
        return $this->render('@QA/Default/adde.html.twig',array('form' => $form->createView()));
    }


    public function mapAction($id , Request $request){
//$a="9.968851, -10.151367";
        $club = new Evenement();
        $em = $this->getDoctrine()->getManager();
        $club = $em->getRepository(Evenement::class)->find($id);
        $form = $this->createForm(EvenementType::class,$club);
        $form = $form->handleRequest($request);
        if ($form->isSubmitted())
        {
            $em->persist($club);
            $em->flush();
            return $this->redirectToRoute("showeeeee");
        }

        return $this->render('@QA/Default/map.html.twig',array('id'=>$club));
    }
}
