<?php

namespace AdminBundle\Controller;

use fados\ChartjsBundle\Model\ChartBuiderData;
use fados\ChartjsBundle\Utils\TypeCharjs;
use fados\ChartjsBundle\Utils\TypeColors;
use ReclamationBundle\Entity\ReclamationNote;
use ReclamationBundle\Entity\ReclamationProf;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="dashboard")
     */
    public function dashAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $nbRA = [0, 0, 0, 0];

        $recRepo = $em->getRepository("ReclamationBundle:Reclamation");
        $recProfRepo = $em->getRepository("ReclamationBundle:ReclamationProf");
        $recNoteRepo = $em->getRepository("ReclamationBundle:ReclamationNote");
        //rec autre
        foreach ($recRepo->findAll() as $rec) {
            if (!($rec instanceof ReclamationNote || $rec instanceof ReclamationProf)) {
                $nbRA[$rec->getEtat()]++;
            }
        }

        //bundle stat
        $grafica = new ChartBuiderData();
        $grafica->setBackgroundOpacity(1);
        $grafica->setType(TypeCharjs::CHARJS_BAR);
        $grafica->setLabels(['Crée', 'En cours', 'Résolu', 'Non Résolu']);
        $grafica->setData(
            array(
                'Reclamation Autre' => $nbRA,
                'Reclamation Prof' => [
                    count($recProfRepo->findBy(['etat' => 0])),
                    count($recProfRepo->findBy(['etat' => 1])),
                    count($recProfRepo->findBy(['etat' => 2])),
                    count($recProfRepo->findBy(['etat' => 3])),
                ],
                'Reclamation Note' => [
                    count($recNoteRepo->findBy(['etat' => 0])),
                    count($recNoteRepo->findBy(['etat' => 1])),
                    count($recNoteRepo->findBy(['etat' => 2])),
                    count($recNoteRepo->findBy(['etat' => 3])),
                ],
            ));
        $grafica->setBackgroundcolor(
            [
                TypeColors::violet,
                TypeColors::tomato,
                TypeColors::orange
            ]
        );
        $grafica->setBordercolor(
            [
                TypeColors::violet,
                TypeColors::tomato,
                TypeColors::orange
            ]
        );

        return $this->render('back/home.html.twig', ['graph' => $grafica]);
    }

}
