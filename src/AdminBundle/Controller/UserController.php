<?php


namespace AdminBundle\Controller;


use AppBundle\Entity\User;
use ClasseBundle\Entity\Enseignant;
use ClasseBundle\Entity\Etudiant;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 * @package AdminBundle\Controller
 * @Route("/users")
 */
class UserController extends Controller
{
    /**
     * @Route("/", name="dashboard_users")
     */
    public function usersAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        return $this->render('@Admin/users/index.html.twig', [
            'users' => $em->getRepository('AppBundle:User')->findAll()
        ]);
    }

    /**
     * @Route("/{user}", name="dashboard_users_show")
     */
    public function usersShowAction(Request $request, User $user)
    {
        return $this->render('back/user/show.html.twig', [
            'user' => $user
        ]);
    }

    /**
     * @Route("/new/user", name="dashboard_user_new")
     */
    public function userNewAction(Request $request)
    {
        $user = new User();

        if ($request->get("typeUser") == "student") {
            $user = new Etudiant();
            $user->addRole(User::ROLE_ETUDIANT);
        } else if ($request->get("typeUser") == "ens") {
            $user = new Enseignant();
            $user->addRole(User::ROLE_ENSEIGNANT);
        } else if ($request->get("typeUser") == "admin") {
            $user->addRole(User::ROLE_SUPER_ADMIN);
        }

        $form = $this->createForm('AdminBundle\Form\UserAdminType', $user);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute('dashboard_users');
        }

        return $this->render('@Admin/users/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }
}