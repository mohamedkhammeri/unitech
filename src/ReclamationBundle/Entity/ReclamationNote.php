<?php

namespace ReclamationBundle\Entity;

use Doctrine\Common\Annotations\Annotation\Enum;
use Doctrine\ORM\Mapping as ORM;
use NoteBundle\Entity\Note;

/**
 * ReclamationNote
 *
 * @ORM\Table(name="reclamation_note", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="RNoteType", columns={"type_note", "note_id"})}
 *     )
 * @ORM\Entity(repositoryClass="ReclamationBundle\Repository\ReclamationNoteRepository")
 */
class ReclamationNote extends Reclamation
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Note
     * @ORM\ManyToOne(targetEntity="NoteBundle\Entity\Note")
     * @ORM\JoinColumn(name="note_id", referencedColumnName="id_note")
     */
    private $note;

    /**
     * @var string
     * @ORM\Column(type="string",columnDefinition="ENUM('CC', 'DS', 'EXAM', 'MOY')")
     * @Enum(value={})
     */
    private $typeNote = '';

    /**
     * @return mixed
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param mixed $note
     */
    public function setNote($note)
    {
        $this->note = $note;
    }

    /**
     * @return string
     */
    public function getTypeNote()
    {
        return $this->typeNote;
    }

    /**
     * @param string $typeNote
     */
    public function setTypeNote(string $typeNote)
    {
        $this->typeNote = $typeNote;
    }

    /**
     * @return float
     */
    public function getNoteNumber()
    {
        switch ($this->typeNote) {
            case 'MOY' :
                return $this->note->getMoyenne();
            case 'EXAM' :
                return $this->note->getNoteExam();
            case 'DS' :
                return $this->note->getNoteDs();
            case 'CC' :
                return $this->note->getNoteCc();
        }
    }
}

