<?php

namespace ReclamationBundle\Entity;

use ClasseBundle\Entity\Etudiant;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Normalizer\NormalizableInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Reclamation
 *
 * @ORM\Table(name="reclamation")
 * @ORM\Entity(repositoryClass="ReclamationBundle\Repository\ReclamationRepository")
 * @ORM\InheritanceType(value="JOINED")
 */
class Reclamation implements \JsonSerializable
{

    private static $reclamationEtat = [
        "0" =>
            ["type" => "crée", "color" => "bg-warning"],
        "1" =>
            ["type" => "en cours de traitement", "color" => "bg-info"],
        "2" =>
            ["type" => "résolu", "color" => "bg-success"],
        "3" =>
            ["type" => "non résolu", "color" => "bg-danger"]
    ];

    /**
     * @return array
     */
    public static function getReclamationEtat()
    {
        return self::$reclamationEtat;
    }

    /**
     * @var Etudiant
     * reclamation belongs to one user
     * @ORM\ManyToOne(targetEntity="ClasseBundle\Entity\Etudiant", inversedBy="reclamations")
     */
    private $etudiant;

    public function __construct()
    {
        $this->etat = 0;
        $this->dateCreation = new \DateTime("now");
        //if($this->container->get('security.token_storage')->getToken()->getUser())
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $sujet;

    /**
     * @ORM\Column(type="text", nullable=false)
     */
    public $description;

    /**
     * 0 = crée
     * 1 = en cours
     * 2 = résolu
     * 3 = non résolu
     * @ORM\Column(type="smallint", options={0,1,2,3}, nullable=false)
     */
    private $etat;

    /**
     * @ORM\Column(type="date")
     */
    private $dateCreation;

    /**
     * @ORM\Column(type="string", unique=true, nullable=true)
     */
    private $codeSuivie;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getSujet()
    {
        return $this->sujet;
    }

    /**
     * @param mixed $sujet
     */
    public function setSujet($sujet)
    {
        $this->sujet = $sujet;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * @param mixed $etat
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;
    }

    /**
     * @return mixed
     */
    public function getEtudiant()
    {
        return $this->etudiant;
    }

    /**
     * @param mixed $etudiant
     */
    public function setEtudiant($etudiant)
    {
        $this->etudiant = $etudiant;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * @param \DateTime $dateCreation
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;
    }

    /**
     * @return mixed
     */
    public function getCodeSuivie()
    {
        return $this->codeSuivie;
    }

    /**
     * @param mixed $codeSuivie
     */
    public function setCodeSuivie($codeSuivie)
    {
        $this->codeSuivie = $codeSuivie;
    }

    /**
     * @return string
     */
    public function getType()
    {
        if ($this instanceof ReclamationProf)
            return "Reclamation Prof";
        else
            if ($this instanceof ReclamationNote)
                return "Reclamation Note";
            else
                return "Reclamation Autre";
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
            "id" => $this->id,
            "desc" => $this->description,
            "subject" => $this->sujet,
            "codeSuivie" => $this->codeSuivie,
            "status" => $this->etat,
            "type" => $this->getType(),
            "date" => $this->dateCreation->format("d-M-yy"),
            "etudiant" => $this->getEtudiant()
        ];
    }
}

