<?php

namespace ReclamationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ReclamationProf
 *
 * @ORM\Table(name="reclamation_prof")
 * @ORM\Entity(repositoryClass="ReclamationBundle\Repository\ReclamationProfRepository")
 */
class ReclamationProf extends Reclamation
{

    /**
     * @ORM\ManyToOne(targetEntity="ClasseBundle\Entity\Enseignant", inversedBy="reclamations")
     * @ORM\JoinColumn(name="prof_id", referencedColumnName="id")
     */
    private $prof;

    /**
     * @return mixed
     */
    public function getProf()
    {
        return $this->prof;
    }

    /**
     * @param mixed $prof
     */
    public function setProf($prof)
    {
        $this->prof = $prof;
    }


}

