<?php

namespace ReclamationBundle\Controller;

use AppBundle\Entity\User;
use ClasseBundle\Entity\Etudiant;
use Faker\Factory;
use ReclamationBundle\Entity\Reclamation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Reclamation controller.
 *
 * @Route("crud")
 */
class ReclamationController extends Controller
{

    /**
     * Lists all reclamation entities.
     *
     * @Route("/all", name="reclamation_all", methods={"GET"})
     */
    public function allAction()
    {
        $reclamations = [];
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        //check role
        if ($user instanceof Etudiant)
            $reclamations = $user->getReclamations();
        else if ($user and $user->hasRole(User::ROLE_SUPER_ADMIN))
            $reclamations = $em->getRepository('ReclamationBundle:Reclamation')->findAll();
        else
            throw $this->createAccessDeniedException($user . ' has no access to this page');

        return $this->render('@Reclamation/back/reclamation/all.html.twig', array(
            'reclamations' => $reclamations,
        ));
    }

    /**
     * Show reclamation.
     *
     * @Route("/rec/{id}", name="reclamation_show_id")
     * @Method("GET")
     */
    public function showRecAction(Request $request, Reclamation $reclamation)
    {
        return $this->render('@Reclamation/back/reclamation/show.html.twig', array(
            'reclamation' => $reclamation,
        ));
    }

    /**
     * Delete reclamation.
     *
     * @Route("/remove/{id}", name="reclamation_delete_id")
     * @Method("POST")
     */
    public function deleteRecAction(Request $request, Reclamation $reclamation)
    {

        $em = $this->getDoctrine()->getManager();
        $em->remove($reclamation);
        $em->flush();

        return $this->redirectToRoute('reclamation_all');
    }

    /**
     * Edit reclamation.
     *
     * @Route("/rec/edit/{id}", name="reclamation_edit_back_id", methods={"GET"})
     * @Method("GET")
     */
    public function editRecAction(Request $request, Reclamation $reclamation)
    {

        if ($reclamation->getEtat() == 2 or $reclamation->getEtat() == 3)
            throw $this->createAccessDeniedException('this reclamation cannot be modified');

        return $this->render('@Reclamation/back/reclamation/edit.html.twig', array(
            'reclamation' => $reclamation,
        ));
    }

    /**
     * Submit Edit reclamation.
     *
     * @Route("/rec/edit/{id}", methods={"POST"})
     */
    public function editSubmitRecAction(Request $request, Reclamation $reclamation)
    {
        if ($reclamation->getEtat() == 2 or $reclamation->getEtat() == 3)
            throw $this->createAccessDeniedException('this reclamation cannot be modified');

        //verif role
        if ($this->getUser()->hasRole(User::ROLE_SUPER_ADMIN))
            $reclamation->setEtat($request->get('your-status'));
        else
            if ($this->getUser()->hasRole(User::ROLE_ETUDIANT))
                $reclamation->setDescription($request->get('your-desc'));
            else
                throw $this->createAccessDeniedException($this->getUser()->getUsername() . ' has no rights');

        $em = $this->getDoctrine()->getManager();
        $em->persist($reclamation);
        $em->flush();

        return $this->redirectToRoute('reclamation_show_id', ['id' => $reclamation->getId()]);
    }


    /**
     * Lists all reclamation entities.
     *
     * @Route("/", name="reclamation_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $reclamations = $em->getRepository('ReclamationBundle:Reclamation')->findAll();

        return $this->render('reclamation/index.html.twig', array(
            'reclamations' => $reclamations,
        ));
    }

    /**
     * Creates a new reclamation entity.
     *
     * @Route("/new", name="reclamation_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {

        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED', null, 'Unable to access this page!');

        $reclamation = new Reclamation();
        $form = $this->createForm('ReclamationBundle\Form\ReclamationType', $reclamation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $reclamation->setEtudiant($this->container->get('security.token_storage')->getToken()->getUser());

            $em = $this->getDoctrine()->getManager();
            $em->persist($reclamation);
            $em->flush();
            $reclamation->setCodeSuivie(Factory::create()
                ->unique(true)
                ->numerify($reclamation->getId() . '###')
            );
            $em->persist($reclamation);
            $em->flush();

            return $this->redirectToRoute('reclamation_show', array('id' => $reclamation->getId()));
        }

        return $this->render('reclamation/new.html.twig', array(
            'reclamation' => $reclamation,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a reclamation entity.
     *
     * @Route("/{id}", name="reclamation_show")
     * @Method("GET")
     */
    public function showAction(Reclamation $reclamation)
    {
        $deleteForm = $this->createDeleteForm($reclamation);

        return $this->render('reclamation/show.html.twig', array(
            'reclamation' => $reclamation,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing reclamation entity.
     *
     * @Route("/{id}/edit", name="reclamation_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Reclamation $reclamation)
    {
        $deleteForm = $this->createDeleteForm($reclamation);
        $editForm = $this->createForm('ReclamationBundle\Form\ReclamationType', $reclamation);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('reclamation_edit', array('id' => $reclamation->getId()));
        }

        return $this->render('reclamation/edit.html.twig', array(
            'reclamation' => $reclamation,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a reclamation entity.
     *
     * @Route("/{id}", name="reclamation_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Reclamation $reclamation)
    {
        $form = $this->createDeleteForm($reclamation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($reclamation);
            $em->flush();
        }

        return $this->redirectToRoute('reclamation_index');
    }

    /**
     * Creates a form to delete a reclamation entity.
     *
     * @param Reclamation $reclamation The reclamation entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Reclamation $reclamation)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('reclamation_delete', array('id' => $reclamation->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
