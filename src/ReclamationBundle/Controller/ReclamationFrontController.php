<?php

namespace ReclamationBundle\Controller;

use ClasseBundle\Entity\Etudiant;
use Faker\Factory;
use ReclamationBundle\Entity\Reclamation;
use ReclamationBundle\Entity\ReclamationNote;
use ReclamationBundle\Entity\ReclamationProf;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/front")
 */
class ReclamationFrontController extends Controller
{

    /**
     * @var Etudiant
     */
    private $etudiant;

    /**
     * @Route("/",name="reclamation_front_index", methods={"GET"})
     */
    public function indexAction()
    {

        if ($this->getUser() == null or !($this->getUser() instanceof Etudiant))
            return $this->render('@Reclamation/front/reclamation/search_by_code.html.twig');

        //connecté ?
        //$this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED', null, 'Unable to access this page!');

        //etudiant ?
        //assert($this->get('security.token_storage')->getToken()->getUser() instanceof Etudiant);

        //$em = $this->getDoctrine()->getManager();

        $this->etudiant = $this->get('security.token_storage')->getToken()->getUser();
        $nbReclamationAutres = $nbReclamationProf = $nbReclamationNotes = 0;
        $nbRecARes = $nbRecPRes = $nbRecNRes = 0;
        foreach ($this->etudiant->getReclamations() as $rec) {
            if ($rec instanceof ReclamationNote) {
                $nbReclamationNotes++;
                if ($rec->getEtat() == 2) {
                    $nbRecNRes++;
                }
            } else
                if ($rec instanceof ReclamationProf) {
                    $nbReclamationProf++;
                    if ($rec->getEtat() == 2) {
                        $nbRecPRes++;
                    }
                } else {
                    $nbReclamationAutres++;
                    if ($rec->getEtat() == 2) {
                        $nbRecARes++;
                    }
                }
        };

        return $this->render('@Reclamation/front/reclamation/all.html.twig', [
            'reclamationAutres' => $nbReclamationAutres,
            'reclamationProf' => $nbReclamationProf,
            'reclamationNote' => $nbReclamationNotes,
            'recAutresResolu' => $nbRecARes,
            'recProfResolu' => $nbRecPRes,
            'recNoteResolu' => $nbRecNRes,
        ]);
    }

    /**
     * Lists all reclamation entities.
     *
     * @Route("/", name="reclamation_front_new_autre", methods={"POST"})
     */
    public function newAutreRecAction(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED', null, 'Unable to access this page!');

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        //check role
        if (!($user instanceof Etudiant)) {
            throw $this->createAccessDeniedException($user . ' has no access to this page');
        }

        $rec = new Reclamation();
        $rec->setEtudiant($user);
        $rec->setSujet(trim($request->get("your-subject")));
        $rec->setDescription(trim($request->get("your-message")));

        $em->persist($rec);
        $em->flush();

        $rec->setCodeSuivie(Factory::create()
            ->unique(true)
            ->numerify($rec->getId() . '###')
        );
        $em->persist($rec);
        $em->flush();

        return $this->redirectToRoute('reclamation_front_show', ["reclamation" => $rec->getId()]);

    }

    /**
     * @Route("/show/codesuivi",name="reclamation_front_codesuivi")
     */
    public function codeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $recCode = $em->getRepository('ReclamationBundle:Reclamation')->findBy(['codeSuivie' => $request->get('code_search')]);

        assert(sizeof($recCode) > 0);

        return $this->render('@Reclamation/front/reclamation/show_code.html.twig', [
            'rec' => $recCode[0]
        ]);
    }


    /**
     * @Route("/show/type/{reclamationType}",name="reclamation_front_show_type")
     */
    public function showByTypeAction(Request $request, $reclamationType)
    {
        $reclamations = [];

        $this->etudiant = $this->getUser();

        foreach ($this->etudiant->getReclamations() as $rec) {
            if ($reclamationType == 'note' && $rec instanceof ReclamationNote) {
                array_push($reclamations, $rec);
            } else
                if ($reclamationType == 'prof' && $rec instanceof ReclamationProf) {
                    array_push($reclamations, $rec);
                } else if ($reclamationType == 'autre' && !($rec instanceof ReclamationProf) && !($rec instanceof ReclamationNote)) {
                    array_push($reclamations, $rec);
                }
        };

        return $this->render('@Reclamation/front/reclamation/show.html.twig', [
            'reclamations' => $reclamations
        ]);
    }

    /**
     * @Route("/show/{reclamation}",name="reclamation_front_show")
     */
    public function showAction(Request $request, Reclamation $reclamation)
    {
        return $this->render('@Reclamation/front/reclamation/show_code.html.twig', [
            'rec' => $reclamation
        ]);
    }
}
