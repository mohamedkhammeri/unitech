<?php

namespace ReclamationBundle\Controller;

use ClasseBundle\Entity\Etudiant;
use Faker\Factory;
use ReclamationBundle\Entity\ReclamationProf;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/frontEns")
 */
class EnseignantsFrontController extends Controller
{

    private $etudiant;

    /**
     * @Route("/",name="enseignants_front_index")
     */
    public function indexAction()
    {
        //connecté ?
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED', null, 'Unable to access this page!');

        //etudiant ?
        assert($this->get('security.token_storage')->getToken()->getUser() instanceof Etudiant);

        $em = $this->getDoctrine()->getManager();
        $teachers = $em->getRepository('ClasseBundle:Enseignant')->findAll();


        return $this->render('@Reclamation/front/enseignants/all.html.twig', [
            'teachers' => $teachers
        ]);
    }

    /**
     * @Route("/new",name="enseignants_new_rec")
     */
    public function newAction(Request $request)
    {
        //connecté ?
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED', null, 'Unable to access this page!');

        //etudiant ?
        assert($this->get('security.token_storage')->getToken()->getUser() instanceof Etudiant);

        $em = $this->getDoctrine()->getManager();

        $reclamationProf = new ReclamationProf();

        $reclamationProf->setEtudiant($this->get('security.token_storage')->getToken()->getUser());
        $reclamationProf->setDescription($request->get('your-message'));
        $reclamationProf->setProf($em->getRepository('ClasseBundle:Enseignant')->find($request->get('your-teacher')));

        $em->persist($reclamationProf);
        $em->flush();
        $reclamationProf->setCodeSuivie(Factory::create()
            ->unique(true)
            ->numerify($reclamationProf->getId() . '###')
        );
        $em->persist($reclamationProf);
        $em->flush();


        return $this->render('@Reclamation/front/enseignants/new.html.twig', [
            'recProf' => $reclamationProf
        ]);
    }
}
