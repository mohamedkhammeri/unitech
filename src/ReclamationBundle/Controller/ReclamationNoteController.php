<?php

namespace ReclamationBundle\Controller;

use ReclamationBundle\Entity\ReclamationNote;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Reclamationnote controller.
 *
 * @Route("crudnote")
 */
class ReclamationNoteController extends Controller
{
    /**
     * Lists all reclamationNote entities.
     *
     * @Route("/", name="reclamationnote_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $reclamationNotes = $em->getRepository('ReclamationBundle:ReclamationNote')->findAll();

        return $this->render('reclamationnote/index.html.twig', array(
            'reclamationNotes' => $reclamationNotes,
        ));
    }

    /**
     * Creates a new reclamationNote entity.
     *
     * @Route("/new", name="reclamationnote_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {

        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED', null, 'Unable to access this page!');

        $reclamationNote = new Reclamationnote();
        $form = $this->createForm('ReclamationBundle\Form\ReclamationNoteType', $reclamationNote);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $reclamationNote->setEtudiant($this->container->get('security.token_storage')->getToken()->getUser());

            $em = $this->getDoctrine()->getManager();
            $em->persist($reclamationNote);
            $em->flush();

            return $this->redirectToRoute('reclamationnote_show', array('id' => $reclamationNote->getId()));
        }

        return $this->render('reclamationnote/new.html.twig', array(
            'reclamationNote' => $reclamationNote,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a reclamationNote entity.
     *
     * @Route("/{id}", name="reclamationnote_show")
     * @Method("GET")
     */
    public function showAction(ReclamationNote $reclamationNote)
    {
        $deleteForm = $this->createDeleteForm($reclamationNote);

        return $this->render('reclamationnote/show.html.twig', array(
            'reclamationNote' => $reclamationNote,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing reclamationNote entity.
     *
     * @Route("/{id}/edit", name="reclamationnote_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, ReclamationNote $reclamationNote)
    {
        $deleteForm = $this->createDeleteForm($reclamationNote);
        $editForm = $this->createForm('ReclamationBundle\Form\ReclamationNoteType', $reclamationNote);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('reclamationnote_edit', array('id' => $reclamationNote->getId()));
        }

        return $this->render('reclamationnote/edit.html.twig', array(
            'reclamationNote' => $reclamationNote,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a reclamationNote entity.
     *
     * @Route("/{id}", name="reclamationnote_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, ReclamationNote $reclamationNote)
    {
        $form = $this->createDeleteForm($reclamationNote);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($reclamationNote);
            $em->flush();
        }

        return $this->redirectToRoute('reclamationnote_index');
    }

    /**
     * Creates a form to delete a reclamationNote entity.
     *
     * @param ReclamationNote $reclamationNote The reclamationNote entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ReclamationNote $reclamationNote)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('reclamationnote_delete', array('id' => $reclamationNote->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
