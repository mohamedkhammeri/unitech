<?php

namespace ReclamationBundle\Controller;

use ClasseBundle\Entity\Etudiant;
use Faker\Factory;
use NoteBundle\Entity\Note;
use ReclamationBundle\Entity\ReclamationNote;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/frontNotes")
 */
class NotesFrontController extends Controller
{

    private $etudiant;

    /**
     * @Route("/",name="notes_front_index")
     */
    public function indexAction()
    {
        //connecté ?
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED', null, 'Unable to access this page!');

        //etudiant ?
        //assert($this->get('security.token_storage')->getToken()->getUser() instanceof Etudiant);
        if (!($this->getUser() instanceof Etudiant)) {
            throw $this->createNotFoundException($this->getUser()->getUsername() . " doesn't have notes");
        }

        //$em = $this->getDoctrine()->getManager();

        $this->etudiant = $this->get('security.token_storage')->getToken()->getUser();

        return $this->render('@Reclamation/front/notes/all.html.twig', [
            'notes' => $this->etudiant->getNotes()
        ]);
    }

    /**
     * @Route("/reclamerNote/{note}",name="notes_reclamer")
     */
    public function reclamerAction(Request $request, Note $note)
    {
        //connecté ?
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED', null, 'Unable to access this page!');

        //etudiant ?
        assert($this->get('security.token_storage')->getToken()->getUser() instanceof Etudiant);

        return $this->render('@Reclamation/front/notes/new.html.twig', [
            'note' => $note
        ]);
    }

    /**
     * @Route("/reclamerNote/{note}/show",name="notes_reclamer_new")
     */
    public function newAction(Request $request, Note $note)
    {
        //connecté ?
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED', null, 'Unable to access this page!');

        //etudiant ?
        assert($this->get('security.token_storage')->getToken()->getUser() instanceof Etudiant);

        $em = $this->getDoctrine()->getManager();

        $this->etudiant = $this->get('security.token_storage')->getToken()->getUser();

        $reclamationNote = new ReclamationNote();
        $reclamationNote->setNote($note);
        $reclamationNote->setEtudiant($this->etudiant);
        $reclamationNote->setDescription($request->get('your-message'));
        $reclamationNote->setTypeNote($request->get('your-note'));

        $em->persist($reclamationNote);
        $em->flush();
        $reclamationNote->setCodeSuivie(Factory::create()
            ->unique(true)
            ->numerify($reclamationNote->getId() . '###')
        );
        $em->persist($reclamationNote);
        $em->flush();


        return $this->render('@Reclamation/front/notes/show.html.twig', [
            'rec' => $reclamationNote
        ]);
    }
}
