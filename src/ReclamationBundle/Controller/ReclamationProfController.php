<?php

namespace ReclamationBundle\Controller;

use ReclamationBundle\Entity\ReclamationProf;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Reclamationprof controller.
 *
 * @Route("crudprof")
 */
class ReclamationProfController extends Controller
{
    /**
     * Lists all reclamationProf entities.
     *
     * @Route("/", name="reclamationprof_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $reclamationProfs = $em->getRepository('ReclamationBundle:ReclamationProf')->findAll();

        return $this->render('reclamationprof/index.html.twig', array(
            'reclamationProfs' => $reclamationProfs,
        ));
    }

    /**
     * Creates a new reclamationProf entity.
     *
     * @Route("/new", name="reclamationprof_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {

        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED', null, 'Unable to access this page!');

        $reclamationProf = new Reclamationprof();
        $form = $this->createForm('ReclamationBundle\Form\ReclamationProfType', $reclamationProf);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $reclamationProf->setEtudiant($this->container->get('security.token_storage')->getToken()->getUser());

            $em = $this->getDoctrine()->getManager();
            $em->persist($reclamationProf);
            $em->flush();

            return $this->redirectToRoute('reclamationprof_show', array('id' => $reclamationProf->getId()));
        }

        return $this->render('reclamationprof/new.html.twig', array(
            'reclamationProf' => $reclamationProf,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a reclamationProf entity.
     *
     * @Route("/{id}", name="reclamationprof_show")
     * @Method("GET")
     */
    public function showAction(ReclamationProf $reclamationProf)
    {
        $deleteForm = $this->createDeleteForm($reclamationProf);

        return $this->render('reclamationprof/show.html.twig', array(
            'reclamationProf' => $reclamationProf,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing reclamationProf entity.
     *
     * @Route("/{id}/edit", name="reclamationprof_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, ReclamationProf $reclamationProf)
    {
        $deleteForm = $this->createDeleteForm($reclamationProf);
        $editForm = $this->createForm('ReclamationBundle\Form\ReclamationProfType', $reclamationProf);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('reclamationprof_edit', array('id' => $reclamationProf->getId()));
        }

        return $this->render('reclamationprof/edit.html.twig', array(
            'reclamationProf' => $reclamationProf,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a reclamationProf entity.
     *
     * @Route("/{id}", name="reclamationprof_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, ReclamationProf $reclamationProf)
    {
        $form = $this->createDeleteForm($reclamationProf);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($reclamationProf);
            $em->flush();
        }

        return $this->redirectToRoute('reclamationprof_index');
    }

    /**
     * Creates a form to delete a reclamationProf entity.
     *
     * @param ReclamationProf $reclamationProf The reclamationProf entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ReclamationProf $reclamationProf)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('reclamationprof_delete', array('id' => $reclamationProf->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
