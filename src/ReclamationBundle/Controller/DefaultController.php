<?php

namespace ReclamationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        //$this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED', null, 'Unable to access this page!');

        return $this->render('ReclamationBundle:Default:index.html.twig');
    }
}
