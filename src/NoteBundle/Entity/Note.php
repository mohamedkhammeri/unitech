<?php

namespace NoteBundle\Entity;
use Doctrine\ORM\Mapping as ORM;


/**
 * Note
 *
 * @ORM\Table(name="note", uniqueConstraints= { @ORM\UniqueConstraint(name="NoteMatConst", columns={"id_user","id_matiere"})})
 * @ORM\Entity(repositoryClass="NoteBundle\Repository\NoteRepository")
 */
class Note implements \JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_note", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="note_cc", type="float",nullable=true)
     */
    private $note_cc;

    /**
     * @var float
     *
     * @ORM\Column(name="note_ds", type="float",nullable=true)
     */
    private $note_ds;

    /**
     * @var float
     *
     * @ORM\Column(name="note_exam", type="float",nullable=true)
     */
    private $note_exam;

    /**
     * @var float
     *
     * @ORM\Column(name="moyenne", type="float")
     */
    private $moyenne =0 ;

    /**
     * @var float
     *
     * @ORM\Column(name="net", type="float")
     */
    private $net =0 ;

    /**
     * @ORM\ManyToOne(targetEntity="ClasseBundle\Entity\Etudiant" , inversedBy="notes")
     * @ORM\JoinColumn(name="id_user",referencedColumnName="id")
     */
    private $user;

    /**
     * @var Matiere
     * @ORM\ManyToOne(targetEntity="NoteBundle\Entity\Matiere")
     * @ORM\JoinColumn(name="id_matiere",referencedColumnName="id_matiere")
     */
    private $matiere ;

    /**
     * @ORM\ManyToOne(targetEntity="ClasseBundle\Entity\Classe")
     * @ORM\JoinColumn(name="id_classe", referencedColumnName="id_classe")
     */
    private $classe;

    /**
     * Get id
     *
     * @return int
     */

    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return float
     */
    public function getNoteCc()
    {
        return $this->note_cc;
    }

    /**
     * @param float $note_cc
     */
    public function setNoteCc($note_cc)
    {
        $this->note_cc = $note_cc;
    }


    /**
     * @return int
     */
    public function getNoteDs()
    {
        return $this->note_ds;
    }

    /**
     * @param  $note_ds
     */
    public function setNoteDs($note_ds)
    {
        $this->note_ds = $note_ds;
    }

    /**
     * @return int
     */
    public function getNoteExam()
    {
        return $this->note_exam;
    }

    /**
     * @param int $note_exam
     */
    public function setNoteExam($note_exam)
    {
        $this->note_exam = $note_exam;
    }

    /**
     * @return int
     */
    public function getMoyenne()
    {
        return $this->note_cc*0.2+$this->note_ds*0.2+$this->note_exam*0.6;
    }

    /**
     * @param int $moyenne
     */
    public function setMoyenne($moyenne)
    {
        $this->moyenne = $moyenne;
    }

    /**
     * @return int
     */
    public function getNet()
    {
        return $this->net;
    }

    /**
     * @param int $net
     */
    public function setNet($net)
    {
        $this->net = $net;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return Matiere
     */
    public function getMatiere()
    {
        return $this->matiere;
    }

    /**
     * @param mixed $matiere
     */
    public function setMatiere($matiere)
    {
        $this->matiere = $matiere;
    }

    /**
     * @return mixed
     */
    public function getClasse()
    {
        return $this->classe;
    }

    /**
     * @param mixed $classe
     */
    public function setClasse($classe)
    {
        $this->classe = $classe;
    }

    /**
     * @return int
     */
    public function getNbNote()
    {
        $nb = 0;

        if ($this->note_cc != null)
            $nb++;
        if ($this->note_exam != null)
            $nb++;
        if ($this->note_ds != null)
            $nb++;

        return $nb;
    }


    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
            "id"=> $this->id,
            "matiere"=> $this->matiere->getNomMat(),
            "exam"=>$this->note_exam,
            "ds"=>$this->note_ds,
            "cc"=>$this->note_cc,
            "note_cc" => $this->note_cc,
            "note_ds" => $this->note_ds,
            "note_exam" => $this->note_exam,
            "moyenne" => $this->moyenne,
            "net" => $this->net,
            "User" => $this->getUser(),
            "Matiere" => $this->getMatiere(),
            "Classe" => $this->getClasse()
        ];
    }
}

