<?php

namespace NoteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Matiere
 * @ORM\Table(name="matiere")
 * @ORM\Entity(repositoryClass="NoteBundle\Repository\MatiereRepository")
 */
class Matiere implements \JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_matiere", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="Veuillez remplir ce champ")
     * @ORM\Column(name="nom_mat",type="string", length=255,unique=true)
     */
    private $nom_mat;

    /**
     * @var string
     * @Assert\NotBlank(message="Veuillez remplir ce champ")
     * @ORM\Column(name="coef", type="float")
     */
    private $coef;

    /**
     * @ORM\ManyToMany(targetEntity="ClasseBundle\Entity\Etudiant" , inversedBy="matieres")
     * @ORM\JoinTable(
     *      joinColumns={@ORM\JoinColumn(name="matiere_id", referencedColumnName="id_matiere")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="etudiant_id", referencedColumnName="id")}
     *      )
     */
    private $etudiants;

    /**
     * @ORM\ManyToMany(targetEntity="ClasseBundle\Entity\Enseignant" , inversedBy="matieres")
     * @ORM\JoinTable(
     *      joinColumns={@ORM\JoinColumn(name="matiere_id", referencedColumnName="id_matiere")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="enseignant_id", referencedColumnName="id")}
     *      )
     */
    private $enseignants;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNomMat()
    {
        return $this->nom_mat;
    }

    /**
     * @param string $nom_mat
     */
    public function setNomMat($nom_mat)
    {
        $this->nom_mat = $nom_mat;
    }

    /**
     * @return string
     */
    public function getCoef()
    {
        return $this->coef;
    }

    /**
     * @param string $coef
     */
    public function setCoef($coef)
    {
        $this->coef = $coef;
    }

    public function __toString()
    {
        $format = "matiere (id: %s, nom_mat: %s, coef: %s)";
        return sprintf($format, $this->id, $this->nom_mat, $this->coef);
    }

    /**
     * @return mixed
     */
    public function getEtudiants()
    {
        return $this->etudiants;
    }

    /**
     * @param mixed $etudiants
     */
    public function setEtudiants($etudiants)
    {
        $this->etudiants = $etudiants;
    }

    /**
     * @return mixed
     */
    public function getEnseignants()
    {
        return $this->enseignants;
    }

    /**
     * @param mixed $enseignants
     */
    public function setEnseignants($enseignants)
    {
        $this->enseignants = $enseignants;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
            "id" => $this->id,
            "matiere" => $this->nom_mat,
            "coef" => $this->coef,
        ];
    }
}