<?php

namespace NoteBundle\Controller;

use Exception;
use NoteBundle\Entity\Matiere;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class MatiereController extends Controller
{
    public function indexAction()
    {
        return $this->render('NoteBundle:Default:index.html.twig');
    }

    /**
     *
     * @Route("/show",name="Note_show")
     */
    public function showAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $matiere = $em->getRepository(Matiere::class)->findAll();
        if ($request->isMethod("POST")) {
            $nom = $request->get('nom_mat');
            $matiere = $this->getDoctrine()->getRepository(Matiere::class)
                ->FindBy(array('nom_mat' => $nom));
        }
        return $this->render('@Note/Matiere/show.html.twig', array('matiere' => $matiere));
    }


    /**
     *
     * @Route("/show",name="Note_show_ajout")
     */
    public function showajoutAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $matiere = $em->getRepository(Matiere::class)->findAll();
        if ($request->isMethod("POST")) {
            $nom = $request->get('nom_mat');
            $matiere = $this->getDoctrine()->getRepository(Matiere::class)
                ->FindBy(array('nom_mat' => $nom));
        }
        return $this->render('@Note/Matiere/show.html.twig', array('matiere' => $matiere));
    }


    /**
     *
     * @Route("/create",name="Note_create")
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $mati = new Matiere();
        if ($request->isMethod("POST")) {
            $mati->setNomMat($request->get('nom_mat'));
            $mati->setCoef($request->get('coef'));
            $em->persist($mati);

            try {
                $em->flush();
                $this->addFlash('success', 'Matiére ajouté avec succés');
            } catch (Exception $exception) {
                $this->addFlash('dangern', 'Matiére déja existe');
            }

            return $this->redirectToRoute("Note_show_ajout");
        }

        return $this->render('@Note/Matiere/ajout.html.twig');

    }

    /**
     *
     * @Route("/delete/{id}",name="Note_delete")
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $matiere = $em->getRepository(Matiere::class)->find($id);
        $em->remove($matiere);
        $em->flush();
        $this->addFlash('successdelete', 'Matiére supprimé avec succés');
        return $this->redirectToRoute("Note_show");

    }

    /**
     *
     * @Route("/edit/{id}",name="Note_edit")
     */
    public function modifiermatAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $matiere = $em->getRepository(Matiere::class)->find($id);
        if ($request->isMethod('POST')) {

            $matiere->setNomMat($request->get('nom_mat'));
            $matiere->setCoef($request->get('coef'));
            try {
                $em->flush();
                $this->addFlash('successmodif', 'Note modifié avec succés');
            } catch (Exception $exception) {
                $this->addFlash('danger', 'Erreur lors de mise à jour du base de données');
            }
            return $this->redirectToRoute('Note_show');
        }
        return $this->render('@Note/Matiere/modifier.html.twig', array('matiere' => $matiere));
    }

}
