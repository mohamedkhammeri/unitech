<?php

namespace NoteBundle\Controller;

use ClasseBundle\Entity\Enseignant;
use ClasseBundle\Entity\Etudiant;
use Exception;
use NoteBundle\Entity\Note;
use NoteBundle\Form\NoteType;
use NoteBundle\Form\RechercheclasseensType;
use NoteBundle\Form\RechercheclasseType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class NoteController extends Controller
{
    /**
     *
     * @Route("/index")
     */
    public function indexAction()
    {

        return $this->render('NoteBundle:Default:index.html.twig');
    }

    /**
     *
     * @Route("/consulterens",name="Note_consulterens")
     */

    public function consulterensAction(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED', null, 'Unable to access this page!');

        //etudiant ?
        assert($this->get('security.token_storage')->getToken()->getUser() instanceof Enseignant);

        //$em = $this->getDoctrine()->getManager();

        $user = $this->etudiant = $this->get('security.token_storage')->getToken()->getUser();
        $note = new Note();
        $form = $this->createForm(RechercheclasseensType::class, $note);
        $form = $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {
            $note = $this->getDoctrine()->getRepository(Note::class)
                ->FindBy(array('user' => $note->getUser(), 'classe' => $note->getClasse(), 'matiere' => $note->getMatiere()));
        } else {
            $note = $this->getDoctrine()->getRepository(Note::class)->findAll();
        }
        return $this->render('@Note/Note/consulterens.html.twig', array('f' => $form->createView(), 'note' => $note));
    }

    /**
     *
     * @Route("/ajouternote",name="Note_ajouternote")
     */
    public function ajouternoteAction(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED', null, 'Unable to access this page!');

        //etudiant ?
        assert($this->get('security.token_storage')->getToken()->getUser() instanceof Enseignant);

        //$em = $this->getDoctrine()->getManager();

        $user = $this->etudiant = $this->get('security.token_storage')->getToken()->getUser();
        $note = new Note();
        $form = $this->createForm(NoteType::class, $note);
        $form = $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $note->setMoyenne($note->getMoyenne());
            $em->persist($note);
            try {
                $em->flush();
                $this->addFlash('successnote', 'note ajouté avec succés');
            } catch (Exception $exception) {
                $this->addFlash('dangernote11', 'Note déja existe');
            }

            return $this->redirectToRoute('Note_consulterens');
        }
        return $this->render('@Note/Note/ajouternote.html.twig', array('f' => $form->createView()));
    }


    /**
     *
     * @Route("/consulteradmin",name="Note_consulteradmin")
     */
    public function consulteradminAction(Request $request)
    {
        $note = new Note();
        $form = $this->createForm(RechercheclasseensType::class, $note);
        $form = $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {
            $note = $this->getDoctrine()->getRepository(Note::class)
                ->FindBy(array('user' => $note->getUser(), 'classe' => $note->getClasse(), 'matiere' => $note->getMatiere()));
        } else {
            $note = $this->getDoctrine()->getRepository(Note::class)->findAll();
        }

        return $this->render('@Note/Note/consulteradmin.html.twig', array('f' => $form->createView(), 'note' => $note));
    }

    /**
     *
     * @Route("/modifiernote/{id}", name="Note_modifiernote")
     */
    public function modifiernoteAction(Request $request, $id)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED', null, 'Unable to access this page!');

        //etudiant ?
        assert($this->get('security.token_storage')->getToken()->getUser() instanceof Enseignant);

        //$em = $this->getDoctrine()->getManager();

        $user = $this->etudiant = $this->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $note = $em->getRepository(Note::class)->find($id);
        if ($request->isMethod('POST')) {

            $note->setNoteCc($request->get('note_cc'));
            $note->setNoteDs($request->get('note_ds'));
            $note->setNoteExam($request->get('note_exam'));
            try {
                $em->flush();
                $this->addFlash('successnotemodif', 'Note modifié avec succés');
            } catch (Exception $exception) {
                $this->addFlash('dangernote', 'Erreur lors de mise à jour du base de données');
            }

            return $this->redirectToRoute('Note_consulterens');
        }
        return $this->render('@Note/Note/modifiernote.html.twig', array('note' => $note));
    }

    /**
     *
     * @Route("/supprimernote/{id}",name="Note_supprimernote")
     */
    public function supprimernoteAction($id)
    {

        $em = $this->getDoctrine()->getManager();
        $note = $em->getRepository(Note::class)->find($id);
        $em->remove($note);
        $em->flush();
        return $this->redirectToRoute("Note_consulteradmin");

    }

    /**
     *
     * @Route("/consulterrelev",name="Note_consulterrelev")
     */


    public function consulterrelevAction()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED', null, 'Unable to access this page!');

        //etudiant ?
        assert($this->get('security.token_storage')->getToken()->getUser() instanceof Etudiant);

        //$em = $this->getDoctrine()->getManager();

        $user = $this->etudiant = $this->get('security.token_storage')->getToken()->getUser();
        $note = $this->getDoctrine()->getRepository(Note::class)->findBy([
            "user" => $user
        ]);
        return $this->render('@Note/Note/consulterrelev.html.twig', array('note' => $note));
    }

    /**
     *
     * @Route("/rechercheclasseadmin",name="Note_rechercheclasseadmin")
     */

    public function rechercheclasseadminAction(Request $request)
    {
        $note = new note();
        $form = $this->createForm(RechercheclasseType::class, $note);
        $form = $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {
            $note = $this->getDoctrine()->getRepository(Note::class)
                ->FindBy(array('classe' => $note->getClasse(), 'matiere' => $note->getMatiere()));
        } else {
            $note = $this->getDoctrine()->getRepository(Note::class)->findAll();
        }
        return $this->render('@Note/Matiere/show.html.twig', array('f' => $form->createView(), 'note' => $note));
    }

    /**
     *
     * @Route("/rechercheclasse",name="Note_rechercheclasse")
     */
    public function rechercheclasseAction(Request $request)
    {

        $note = new note();
        $form = $this->createForm(RechercheclasseType::class, $note);
        $form = $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {
            $note = $this->getDoctrine()->getRepository(Note::class)
                ->FindBy(array('classe' => $note->getClasse(), 'matiere' => $note->getMatiere()));
        } else {
            $note = $this->getDoctrine()->getRepository(Note::class)->findAll();

        }
        return $this->render('@Note/Note/Rechercheclasse.html.twig', array('f' => $form->createView(), 'note' => $note));
    }


    /**
     *
     * @Route("/pdf",name="Note_pdf")
     */
    public function pdfAction()

    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED', null, 'Unable to access this page!');

        //etudiant ?
        assert($this->get('security.token_storage')->getToken()->getUser() instanceof Etudiant);

        //$em = $this->getDoctrine()->getManager();

        $user = $this->etudiant = $this->get('security.token_storage')->getToken()->getUser();
        $note = $this->getDoctrine()->getRepository(Note::class)->findBy([
            "user" => $user]);
        $snappy = $this->get('knp_snappy.pdf');
        $html = $this->render('@Note/Note/pdf.html.twig', array('note' => $note)
        );
        $filename = 'SnappyPDF';
        return new Response(
            $snappy->getOutputFromHtml($html),
            300,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline; filename="' . $filename . '.pdf"'
            )
        );
    }
}






