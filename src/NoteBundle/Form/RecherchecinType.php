<?php

namespace NoteBundle\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use NoteBundle\Entity\matiere;
use AppBundle\Entity\User;


class RecherchecinType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder  ->add('user',EntityType::class, array(
            'class'=>'ClasseBundle:Etudiant',
            'choice_label'=> function($entity) {
                return $entity->getCIN();
            },
            'placeholder'   => 'Choisir Cin etudiant',
            'label'     =>  'Cin',
            'attr'      =>  array(
                'class' =>  'form-control',

            )))

            ->add('Chercher',SubmitType::class);

    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'NoteBundle\Entity\Note'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'notebundle_note';
    }


}
