<?php

namespace NoteBundle\Form;
use Doctrine\DBAL\Types\StringType;
use NoteBundle\NoteBundle;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use ClasseBundle\Entity\Classe;
use NoteBundle\Entity\Matiere;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class RechercheclasseType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('classe',EntityType::class, array(
            'class'         => 'ClasseBundle:Classe',
            'placeholder'   => 'Choisir classe',
            'choice_label'  => 'NumClasse',
            'attr'      =>  array(
                'class' =>  'form-control',
            )))
            ->add('matiere',EntityType::class, array(
                'class'         => 'NoteBundle:Matiere',
                'placeholder'   => 'Choisir matiere',
                'choice_label'  => 'NomMat',
                'attr'      =>  array(
                    'class' =>  'form-control',
                )))
            ->add('Chercher',SubmitType::class);

    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'NoteBundle\Entity\Note'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'notebundle_note';
    }


}
