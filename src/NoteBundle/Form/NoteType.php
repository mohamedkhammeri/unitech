<?php

namespace NoteBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NoteType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('user',EntityType::class, array(
                'class'=>'ClasseBundle:Etudiant',
                'choice_label'=> function($entity) {
                    return $entity->getCIN();
                },
                'placeholder'   => 'Choisir Cin étudiant',
                'label'     =>  'Cin',
                'attr'      =>  array(
                    'class' =>  'form-control',

                )))

            ->add('classe',EntityType::class, array(
                'class'         => 'ClasseBundle:Classe',
                'placeholder'   => 'Choisir classe',
                'choice_label'  => 'NumClasse',
                'attr'      =>  array(
                    'class' =>  'form-control',
                )))
            ->add('matiere',EntityType::class, array(
                'class'=>'NoteBundle:Matiere',
                'choice_label'=> function($entity) {
                    return $entity->getNomMat();
                },
                'placeholder'   => 'Choisir Matiére',
                'label'     =>  'Nom matiére',
                'attr'      =>  array(
                    'class' =>  'form-control',

                )))
              ->add('note_cc',IntegerType::class, array(
                'label'    =>  'Note cc',
                'attr'     =>  array(
                'class' =>  'form-control'
                    )))
            ->add('note_ds',IntegerType::class, array(
                'label'    =>  'Note Ds',
                'attr'     =>  array(
                    'class' =>  'form-control'
                )))
            ->add('note_exam',IntegerType::class, array(
                'label'    =>  'Note Examen',
                'attr'     =>  array(
                    'class' =>  'form-control'
                )))
            ->add('submit',SubmitType::class, array(
            'label' => 'Ajouter',
            'attr'  =>  array(
                'class'  => 'btn btn-primary',
            'formnovalidate'=>'formnovalidate')

            )
            );


    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'NoteBundle\Entity\Note'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'notebundle_note';
    }


}
