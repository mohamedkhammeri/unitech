<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Mgilet\NotificationBundle\Annotation\Notifiable;
use Mgilet\NotificationBundle\NotifiableInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\InheritanceType(value="JOINED")
 * @Notifiable(name="user_notify")
 */
class User extends BaseUser implements NotifiableInterface, \JsonSerializable
{
    const ROLE_ETUDIANT = 'ROLE_STUDENT';

    const ROLE_ENSEIGNANT = 'ROLE_ENS';

    public function __construct()
    {
        parent::__construct();
        $this->enabled = true;
        $this->addRole(User::ROLE_DEFAULT);

        //$this->reclamations = new ArrayCollection();
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\NotBlank(message="first_name est obligatoire")
     * @ORM\Column(name="first_Name",type="string",length=255)
     */
    private $first_Name;
    /**
     * @Assert\NotBlank(message="last_name est obligatoire")
     * @ORM\Column(name="last_Name",type="string",length=255)
     */
    private $last_Name;


    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $phone_number;

    /**
     * @var int
     * @Assert\NotBlank(message="Veuillez remplir CIN")
     * @ORM\Column(name="CIN", type="integer",unique=true)
     */
    private $CIN;


    /**
     * @return mixed
     */
    public function getCIN()
    {
        return $this->CIN;
    }

    /**
     * @param mixed $CIN
     */
    public function setCIN($CIN)
    {
        $this->CIN = $CIN;
    }

    /**
     * @return int
     */
    public function getPhoneNumber()
    {
        return $this->phone_number;
    }

    /**
     * @param int $phone_number
     */
    public function setPhoneNumber($phone_number)
    {
        $this->phone_number = $phone_number;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->first_Name;
    }

    /**
     * @param mixed $first_Name
     */
    public function setFirstName($first_Name)
    {
        $this->first_Name = $first_Name;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->last_Name;
    }

    /**
     * @param mixed $last_Name
     */
    public function setLastName($last_Name)
    {
        $this->last_Name = $last_Name;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
            "id" => $this->id,
            "CIN" => $this->getCIN(),
            "nom" => $this->getFirstName(),
            "prenom" => $this->getLastName(),
        ];
    }
}

