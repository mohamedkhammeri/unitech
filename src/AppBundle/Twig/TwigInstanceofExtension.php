<?php


namespace AppBundle\Twig;


use ReclamationBundle\Entity\Reclamation;
use ReclamationBundle\Entity\ReclamationNote;
use ReclamationBundle\Entity\ReclamationProf;
use Twig\Extension\AbstractExtension;

class TwigInstanceofExtension extends AbstractExtension
{
    /**
     * {@inheritdoc}
     */
    public function getTests()
    {
        return [
            new \Twig_SimpleTest('instanceof', array($this, 'isInstanceOf')),
            new \Twig_SimpleTest('instanceOfRecNote', function (Reclamation $reclamation) {
                return $reclamation instanceof ReclamationNote;
            }),
            new \Twig_SimpleTest('instanceOfRecProf', function (Reclamation $reclamation) {
                return $reclamation instanceof ReclamationProf;
            })
        ];
    }

    public function isInstanceOf($object, $class)
    {
        try {
            $reflectionClass = new \ReflectionClass($class);
        } catch (\ReflectionException $e) {
        }

        return $reflectionClass->isInstance($object);
    }
}