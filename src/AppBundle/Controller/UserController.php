<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * User controller.
 *
 * @Route("users")
 */
class UserController extends Controller
{
    /**
     * @Route("/{id}", name="user_show")
     */
    public function userShowAction(Request $request, User $user)
    {
        return $this->render('back/user/show.html.twig', [
            'user' => $user
        ]);
    }
}
