<?php


namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('first_Name', TextType::class, [
            'required' => true,
            'label' => 'First name'
        ])->add('last_Name', TextType::class, [
            'required' => true,
            'label' => 'Last name'
        ])->add('phone_number', NumberType::class, [
            'required' => true,
            'label' => 'Phone number'
        ])->add('CIN', NumberType::class, [
            'required' => true,
            'label' => 'CIN'
        ]);
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';

        // Or for Symfony < 2.8
        // return 'fos_user_registration';
    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }
}