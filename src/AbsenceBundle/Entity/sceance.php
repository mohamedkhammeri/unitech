<?php

namespace AbsenceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * seance
 *
 * @ORM\Table(name="seance")
 * @ORM\Entity(repositoryClass="AbsenceBundle\Repository\sceanceRepository")
 */
class sceance implements \JsonSerializable
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     *
     * @ORM\GeneratedValue(strategy="AUTO")
     */

    private $id_seance;

    /**
     * @Assert\NotBlank(message="ce champ est obligatoire")
     * @ORM\Column(type="time")
     */
    private $heure;
    /**
     * @Assert\NotBlank(message="ce champ est obligatoire")
     * @ORM\Column(type="string",length=255)
     */
    private $jour;

    /**
     * @return int
     */
    public function getNum()
    {
        return $this->num;
    }

    /**
     * @param int $num
     */
    public function setNum($num)
    {
        $this->num = $num;
    }

    /**
     * @var int
     *
     * @ORM\Column(name="num", type="integer")
     */
    private $num;


    /**
     * @return int
     */
    public function getIdSeance()
    {
        return $this->id_seance;
    }

    /**
     * @param int $id_seance
     */
    public function setIdSeance($id_seance)
    {
        $this->id_seance = $id_seance;
    }


    /**
     * @return mixed
     */
    public function getHeure()
    {
        return $this->heure;
    }

    /**
     * @param mixed $heure
     */
    public function setHeure($heure)
    {
        $this->heure = $heure;
    }

    /**
     * @return mixed
     */
    public function getJour()
    {
        return $this->jour;
    }

    /**
     * @param mixed $jour
     */
    public function setJour($jour)
    {
        $this->jour = $jour;
    }

    public function __toString()
    {
        return 'S' . $this->num . ' ' . $this->jour . ' à ' . $this->heure->format('H:i:s');
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
            "id" => $this->id_seance,
            "num" => $this->num
        ];
    }
}

