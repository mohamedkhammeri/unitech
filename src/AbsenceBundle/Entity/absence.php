<?php

namespace AbsenceBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * absence
 *
 * @ORM\Table(name="absence")
 * @ORM\Entity(repositoryClass="AbsenceBundle\Repository\absenceRepository")
 */
class absence implements \JsonSerializable
{
    /**
     * @var int
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idabsence;
    /**
     * @ORM\Column(type="integer")
     */
    private $id_user;

    /**
     * @return int
     */
    public function getIdUser()
    {
        return $this->id_user;
    }

    /**
     * @param int $id_user
     */
    public function setIdUser($id_user)
    {
        $this->id_user = $id_user;
    }

    /**
     *
     * @ORM\Column(type="integer")
     */
    private $absence;

    /**
     * @var string
     *
     * @ORM\Column(name="Image_Event", type="string", length=255, nullable=true)
     */
    private $imageEvent;
    /**
     * @ORM\Column(type="integer")
     */
    private $id_classe;
    /**
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="id_user",referencedColumnName="id")
     */
    private $user;
    /**
     *
     * @ORM\ManyToOne(targetEntity="sceance")
     * @ORM\JoinColumn(name="id_seance",referencedColumnName="id_seance")
     */
    private $sceance;
    /**
     * @ORM\ManyToOne(targetEntity="ClasseBundle\Entity\Classe")
     * @ORM\JoinColumn(name="id_classe",referencedColumnName="id_classe")
     */
    private $classe;
    /**
     * @ORM\ManyToOne(targetEntity="NoteBundle\Entity\Matiere")
     * @ORM\JoinColumn(name="id_matiere",referencedColumnName="id_matiere")
     */
    private $matiere;

    /**
     * @return mixed
     */
    public function getClasse()
    {
        return $this->classe;
    }

    /**
     * @param mixed $classe
     */
    public function setClasse($classe)
    {
        $this->classe = $classe;
    }

    /**
     * @return mixed
     */
    public function getMatiere()
    {
        return $this->matiere;
    }

    /**
     * @param mixed $matiere
     */
    public function setMatiere($matiere)
    {
        $this->matiere = $matiere;
    }


    /**
     * @return mixed
     */
    public function getSceance()
    {
        return $this->sceance;
    }

    /**
     * @param mixed $sceance
     */
    public function setSceance($sceance)
    {
        $this->sceance = $sceance;
    }


    /**
     * @return int
     */
    public function getIdabsence()
    {
        return $this->idabsence;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @param int $idabsence
     */
    public function setIdabsence($idabsence)
    {
        $this->idabsence = $idabsence;
    }


    /**
     * @return int
     */
    public function getIdSeance()
    {
        return $this->id_seance;
    }

    /**
     * @param int $id_seance
     */
    public function setIdSeance($id_seance)
    {
        $this->id_seance = $id_seance;
    }

    /**
     * @return int
     */
    public function getAbsence()
    {
        return $this->absence;
    }

    /**
     * @param int $absence
     */
    public function setAbsence($absence)
    {
        $this->absence = $absence;
    }


    /**
     * @return int
     */
    public function getIdClasse()
    {
        return $this->id_classe;
    }

    /**
     * @param int $id_classe
     */
    public function setIdClasse($id_classe)
    {
        $this->id_classe = $id_classe;
    }

    /**
     * @return string
     */
    public function getImageEvent()
    {
        return $this->imageEvent;
    }

    /**
     * @param string $imageEvent
     */
    public function setImageEvent($imageEvent)
    {
        $this->imageEvent = $imageEvent;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
            "id" => $this->idabsence,
            "abs" => $this->absence,
            "user" => $this->getUser(),
            "classe" => $this->getClasse(),
            "sceance" => $this->getSceance(),
            "matiere" => $this->getMatiere()
        ];
    }

}

