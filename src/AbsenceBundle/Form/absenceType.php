<?php

namespace AbsenceBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class absenceType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', EntityType::class,
                array(
                    'class' => 'ClasseBundle:Etudiant',
                    'choice_label' => 'CIN',
                    'multiple' => false

                )
            )
            ->add('classe', EntityType::class, array(
                'class' => 'ClasseBundle:Classe',
                'placeholder' => 'Choisir classe',
                'attr' => array(
                    'class' => 'form-control',
                )))
            ->add('matiere', EntityType::class, array(
                'class' => 'NoteBundle:Matiere',
                'choice_label' => function ($entity) {
                    return $entity->getNomMat();
                },
                'placeholder' => 'Choisir Matiére',
                'label' => 'Nom matiére',
                'attr' => array(
                    'class' => 'form-control',

                )))
            ->add('sceance', EntityType::class,
                array(
                    'class' => 'AbsenceBundle:sceance',
                    'multiple' => false

                )
            )
            ->add('absence')
            ->add('imageEvent', FileType::class, ['mapped' => false, 'label' => 'Image: *'])
            // ->add('id_seance')
            //->add('dateabs')
            //->add('id_mat')
            // ->add('id_classe')

            ->add('ajouter', SubmitType::class);;;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AbsenceBundle\Entity\absence'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'absencebundle_absence';
    }


}
