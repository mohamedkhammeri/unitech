<?php

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$collection = new RouteCollection();

$collection->add('absence_index', new Route(
    '/',
    array('_controller' => 'AbsenceBundle:Absence:index'),
    array(),
    array(),
    '',
    array(),
    array('GET')
));

$collection->add('absence_show', new Route(
    '/{id}/read1',
    array('_controller' => 'AbsenceBundle:Absence:read1'),
    array(),
    array(),
    '',
    array(),
    array('GET')
));

$collection->add('absence_new', new Route(
    '/addabsence',
    array('_controller' => 'AbsenceBundle:Absence:addabsence'),
    array(),
    array(),
    '',
    array(),
    array('GET', 'POST')
));

$collection->add('absence_edit', new Route(
    '/{id}/updateabs',
    array('_controller' => 'AbsenceBundle:Absence:updateabs'),
    array(),
    array(),
    '',
    array(),
    array('GET', 'POST')
));

$collection->add('absence_delete', new Route(
    '/{id}/deleteabs',
    array('_controller' => 'AbsenceBundle:Absence:deleteabs'),
    array(),
    array(),
    '',
    array(),
    array('DELETE')
));

return $collection;
