<?php

namespace AbsenceBundle\Controller;

use AbsenceBundle\Entity\absence;
use AbsenceBundle\Form\absenceType;
use AbsenceBundle\Form\RechercheclasseType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class AbsenceController extends Controller
{
    public function read1Action()

    {
        $absences = $this->getDoctrine()->getRepository(absence::class)->findAll();
        return $this->render('@Absence/Default/read1.html.twig', array('absences' => $absences));
    }

    public function readetudiantAction()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED', null, 'Unable to access this page!');

        //etudiant ?
        assert($this->get('security.token_storage')->getToken()->getUser());

        //$em = $this->getDoctrine()->getManager();

        $user = $this->etudiant = $this->get('security.token_storage')->getToken()->getUser()->getId();
        $test = $this->getDoctrine()->getRepository(absence::class)->findAbs($user);
        $absences = $this->getDoctrine()->getRepository(absence::class)->findBy([
            "id_user" => $user
        ]);
        return $this->render('@Absence/Default/etudiant.html.twig', array('absences' => $absences, 'test' => $test));
    }

    public function deleteabsAction($id)
    {

        $em = $this->getDoctrine()->getManager();
        $absence = $em->getRepository(absence::class)->find($id);
        $em->remove($absence);
        try {
            $em->flush();
            $this->addFlash('successmodif', 'Absence supprimé avec succés');
        } catch (Exception $exception) {
            $this->addFlash('danger', 'Erreur lors de mise à jour du base de données');
        }
        $em->flush();
        return $this->redirectToRoute("absence_read1");


    }

    public function updateabsAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $absence = $em->getRepository(absence::class)->find($id);
        if ($request->isMethod('POST')) {
            $absence->setAbsence($request->get('absence'));
            try {
                $em->flush();
                $this->addFlash('successmodif', 'Absence modifé avec succés');
            } catch (Exception $exception) {
                $this->addFlash('danger', 'Erreur lors de mise à jour du base de données');
            }


            $em->flush();
            return $this->redirectToRoute('absence_rechercheabs');
        }
        return $this->render('@Absence/Default/updateabs.html.twig', array('absence' => $absence));
    }

    public function addabsenceAction(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED', null, 'Unable to access this page!');

        //etudiant ?

        //$em = $this->getDoctrine()->getManager();
        $membre = $this->etudiant = $this->get('security.token_storage')->getToken()->getUser()->getId();
        $absence = new absence();
        $form = $this->createForm(absenceType::class, $absence);
        $form = $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {
            $file = $request->files->get('absencebundle_absence')['imageEvent'];
            $uploads_directory = $this->getParameter('uploads_directory');
            $imageEvent = $file->getClientoriginalName();
            $em = $this->getDoctrine()->getManager();
            $file->move($uploads_directory, $imageEvent);
            $absence->setImageEvent($imageEvent);
            $absence->setIdUser($membre);

            $em->persist($absence);
            try {
                $em->flush();
                $this->addFlash('successabsence', 'absence ajouté avec succés');
            } catch (Exception $exception) {
                $this->addFlash('dangerabsence', 'absence déja existe');
            }
            // $em->flush();
            return $this->redirectToRoute('absence_rechercheabs');
        }
        return $this->render('@Absence/Default/addabsence.html.twig', array('f' => $form->createView()));


    }

    public function rechercheAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $absence = $em->getRepository(absence::class)->findAll();
        if ($request->isMethod("POST")) {
            $absence = $request->get('absence');
            $absence = $em->getRepository("AbsenceBundle:absence")->findBy(array('absence' => $absence));
        }

        return $this->render('@Absence/Default/rechercheCin.html.twig', array('absence' => $absence));
    }

    public function rechercheclasseAction(Request $request)
    {
        $absence = new absence();
        $form = $this->createForm(RechercheclasseType::class, $absence);
        $form = $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {
            $absence = $this->getDoctrine()->getRepository(absence::class)
                ->FindBy(array('user' => $absence->getUser(), 'classe' => $absence->getClasse(), 'matiere' => $absence->getMatiere()));
            //->FindBy(array('classe' => $absence->getClasse(),'matiere' => $absence->getMatiere()));
        } else {
            $absence = $this->getDoctrine()->getRepository(absence::class)->findAll();
        }
        return $this->render('@Absence/Default/Rechercheclasse.html.twig', array('f' => $form->createView(), 'absence' => $absence));
    }


}
