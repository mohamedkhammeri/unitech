<?php

namespace AbsenceBundle\Controller;

use AbsenceBundle\AbsenceBundle;
use AbsenceBundle\Entity\absence;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AbsenceBundle\Entity\sceance;
use Symfony\Component\HttpFoundation\Request;
use AbsenceBundle\Form\sceanceType;
use AbsenceBundle\Form\RechercheclasseType;


class SeanceController extends Controller
{
    public function test2Action()
    {
        return $this->render('AbsenceBundle:Default:index.html.twig');
    }

    public function readAction()
    {
        $sceances= $this->getDoctrine() ->getRepository(sceance::class)->findAll();
      return $this->render('@Absence/Default/read.html.twig',array('sceances'=>$sceances));


    }


    public function ajoutsceanceAction(Request $request)
    {
        $sceance = new sceance();
        $form=$this->createForm(sceanceType::class, $sceance);
        $form=$form->handleRequest($request);
        if($form->isSubmitted()){
            $em=$this->getDoctrine()->getManager();
            $em->persist($sceance);
            $em->flush();
            return $this->redirectToRoute('absence_recherche');}
        return $this->render('@Absence/Default/ajout.html.twig',array('f'=>$form->createView()));



    }
    public function deleteAction($id)
    {

        $em=$this->getDoctrine()->getManager();
        $sceance =$em ->getRepository(sceance::class) ->find($id);
        $em->remove($sceance);
        try{
            $em->flush();
            $this->addFlash('successmodif', 'Absence supprimé avec succés');
        }
        catch(Exception $exception)
        {
            $this->addFlash('danger', 'Erreur lors de mise à jour du base de données');
        }
        $em->flush();
        return $this->redirectToRoute("absence_recherche" );


    }
    public function updateAction( Request $request,$id)
    {
        $em = $this->getDoctrine()->getManager();
        $sceance = $em->getRepository(sceance::class)->find($id);
        if ($request->isMethod('POST')) {
            $sceance->setHeure($request->get('heure'));
            $sceance->setJour($request->get('jour'));
            try{
                $em->flush();
                $this->addFlash('successmodif', 'Absence modifié avec succés');
            }
            catch(Exception $exception)
            {
                $this->addFlash('danger', 'Erreur lors de mise à jour du base de données');
            }

            $em->flush();
            return $this->redirectToRoute('absence_recherche');
        }
        return $this->render('@Absence/Default/update.html.twig', array('sceance' => $sceance));
    }

   public function recherchebyjourAction(Request $request){
        $em=$this->getDoctrine()->getManager();
        $sceance=$em->getRepository(sceance::class)->findAll();
        if($request->isMethod("POST"))
        {
            $jour=$request->get('jour');
            $sceance=$em->getRepository("AbsenceBundle:sceance")->findBy(array('jour'=>$jour));


        }
       return $this->render('@Absence/Default/recherche.html.twig', array('sceance' => $sceance));
   }

    public function chartAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        //$user = $this->container->get('security.token_storage')->getToken()->getUser();
        $seances = $em->getRepository('AbsenceBundle:sceance')->findAll();

        foreach ($seances as $seance) {

            $comments = $em->getRepository('AbsenceBundle:sceance')->findBy([
                "id_seance"=>$seance->getIdSeance()
            ]);



        }
        $em = $this->getDoctrine()->getManager();
        return $this->render('@Absence/Default/stat.html.twig',array('seances'=>$seances));
    }
    public function rechercheclasseadminAction(Request $request)
    {
        $absence = new absence();
        $form = $this->createForm(RechercheclasseType::class, $absence);
        $form = $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()){
            $absence = $this->getDoctrine()->getRepository(absence::class)
                ->FindBy(array('user' => $absence->getUser(),'classe' => $absence->getClasse(),'matiere' => $absence->getMatiere()));
            //->FindBy(array('classe' => $absence->getClasse(),'matiere' => $absence->getMatiere()));
        } else {
            $absence = $this->getDoctrine()->getRepository(absence::class)->findAll();
        }
        return $this->render('@Absence/Default/Rechercheclasseadmin.html.twig', array('f' => $form->createView(), 'absence' => $absence));
    }




}
