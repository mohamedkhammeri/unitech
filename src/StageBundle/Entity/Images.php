<?php

namespace StageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Images
 *
 * @ORM\Table(name="images")
 * @ORM\Entity(repositoryClass="StageBundle\Repository\ImagesRepository")
 */
class Images
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Listestage" ,cascade={"persist"},inversedBy="images")
     * @ORM\JoinColumn(name="listestage_id",referencedColumnName="id_listestage")
     */
    private $Listestage;
    /**
     * @ORM\Column(name="image", type="string", nullable=true)
     */
    private $image;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set listestage.
     *
     * @param \StageBundle\Entity\Listestage|null $listestage
     *
     * @return Images
     */
    public function setListestage(\StageBundle\Entity\Listestage $listestage)
    {
        $this->Listestage = $listestage;

        return $this;
    }

    /**
     * Get listestage.
     *
     * @return \StageBundle\Entity\Listestage|null
     */
    public function getListestage()
    {
        return $this->Listestage;
    }


    /**
     * Set image.
     *
     * @param string|null $image
     *
     * @return Images
     */
    public function setImage($image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image.
     *
     * @return string|null
     */
    public function getImage()
    {
        return $this->image;
    }
}
