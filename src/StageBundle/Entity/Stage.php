<?php

namespace StageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="stage")
 * @ORM\Entity(repositoryClass="StageBundle\Repository\StageeRepository")
 */
class Stage
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id_stage;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="id_user")
     */
    private $id_user;


    /**
     * @ORM\Column(type="string",length=255)
     * @Assert\NotBlank(message="merci d'ajouter un sujet")
     */
    private $sujet;
    /**
     * @ORM\Column(type="string",length=255)
     * @Assert\NotBlank(message="merci d'ajouter une  description")
     */
    private $description;
    /**
     * @ORM\Column(name="branche",type="string",length=255)
     * @Assert\Choice({"Data_science","Informatique","Business","Genie_logiciel"})
     */
    private $branche;
    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date_creation;

    /**
     * @return mixed
     */
    public function getIdStage()
    {
        return $this->id_stage;
    }

    /**
     * @param mixed $id_stage
     */
    public function setIdStage($id_stage)
    {
        $this->id_stage = $id_stage;
    }

    /**
     * @return mixed
     */
    public function getIdUser()
    {
        return $this->id_user;
    }

    /**
     * @param mixed $id_user
     */
    public function setIdUser($id_user)
    {
        $this->id_user = $id_user;
    }

    /**
     * @return mixed
     */
    public function getSujet()
    {
        return $this->sujet;
    }

    /**
     * @param mixed $sujet
     */
    public function setSujet($sujet)
    {
        $this->sujet = $sujet;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getBranche()
    {
        return $this->branche;
    }

    /**
     * @param mixed $branche
     */
    public function setBranche($branche)
    {
        $this->branche = $branche;
    }

    /**
     * @return mixed
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }

    /**
     * @param mixed $date_creation
     */
    public function setDateCreation($date_creation)
    {
        $this->date_creation = $date_creation;
    }

}