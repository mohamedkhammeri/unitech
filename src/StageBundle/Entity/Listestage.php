<?php

namespace StageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Listestage
 *
 * @ORM\Table(name="listestage")
 * @ORM\Entity(repositoryClass="StageBundle\Repository\ListestageRepository")
 */
class Listestage
{

    /**
     * @var int
     *
     * @ORM\Column(name="id_listestage", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id_listestage;

    /**
     * @var string
     *
     * @ORM\Column(name="sujet", type="string")
     *
     */
    private $sujet;

    /**
     * @ORM\Column(name="description", type="string")
     */
    private $description;
    /**
     * @ORM\Column(name="branche", type="string")
     */
    private $branche;

    /**
     * @ORM\Column(name="LettreMotivation", type="string")
     * @Assert\NotBlank(message="il faut ecrire une lettre de motivation")
     */
    private $LettreMotivation;

    /**
     * @ORM\Column(name="CV", type="string")
     */
    private $CV;

    /**
     * @ORM\Column(name="adresse", type="string")
     * @Assert\Email(message="verifier qu'il s'agit d'un mail ")
     * @Assert\NotBlank(message="merci de saisir votre adresse")
     */
    private $adresse;

    /**
     * @ORM\OneToMany(targetEntity="Images", mappedBy="Listestage")
     * @Assert\NotBlank(message="merci d ajouter votre CV")
     */
    private $images;

    /**
     * Get id_listestage.
     *
     * @return int
     */
    public function getIdListestage()
    {
        return $this->id_listestage;
    }


    /**
     * Set sujet.
     *
     * @param string $sujet
     *
     * @return Listestage
     */
    public function setSujet($sujet)
    {
        $this->sujet = $sujet;

        return $this;
    }

    /**
     * Get sujet.
     *
     * @return string
     */
    public function getSujet()
    {
        return $this->sujet;
    }

    /**
     * Set CV.
     *
     * @param string|null $CV
     *
     * @return Listestage
     */
    public function setCV($CV = null)
    {
        $this->CV = $CV;

        return $this;
    }

    /**
     * Get CV.
     *
     * @return string|null
     */
    public function getCV()
    {
        return $this->CV;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add image.
     *
     * @param \StageBundle\Entity\Images $image
     *
     * @return Listestage
     */
    public function addImage(\StageBundle\Entity\Images $image)
    {
        $this->images[] = $image;

        return $this;
    }

    /**
     * Remove image.
     *
     * @param \StageBundle\Entity\Images $image
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeImage(\StageBundle\Entity\Images $image)
    {
        return $this->images->removeElement($image);
    }

    /**
     * Get images.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return Listestage
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set branche.
     *
     * @param string|null $branche
     *
     * @return Listestage
     */
    public function setBranche($branche = null)
    {
        $this->branche = $branche;

        return $this;
    }

    /**
     * Get branche.
     *
     * @return string|null
     */
    public function getBranche()
    {
        return $this->branche;
    }

    /**
     * Set LettreMotivation.
     *
     * @param string|null $LettreMotivation
     *
     * @return Listestage
     */
    public function setLettreMotivation($LettreMotivation = null)
    {
        $this->LettreMotivation = $LettreMotivation;

        return $this;
    }

    /**
     * Get LettreMotivation.
     *
     * @return string|null
     */
    public function getLettreMotivation()
    {
        return $this->LettreMotivation;
    }

    /**
     * Set adresse.
     *
     * @param string|null adresse
     *
     * @return Listestage
     */
    public function setAdresse($adresse = null)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse.
     *
     * @return string|null
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

}
