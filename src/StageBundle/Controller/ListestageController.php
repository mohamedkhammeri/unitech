<?php

namespace StageBundle\Controller;

use FOS\UserBundle\Model\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use StageBundle\Entity\Listestage;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ListestageController extends Controller
{
    /**
     * @Route("/ajouter/demande"   ,   name="adddemande")
     */
    public function AddDemandeAction(Request $request)
    {
        //$em1 = $this->getDoctrine()->getManager();
        // $stage = $em1->getRepository(Stage::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $listestage = new Listestage();
        $listestage->setSujet($request->get("sujet"));
        $listestage->setDescription($request->get("description"));
        $listestage->setBranche($request->get("branche"));
        $listestage->setLettreMotivation($request->get("LettreMotivation"));
        $listestage->setAdresse($request->get("adresse"));

        $file = $request->files->get('fileToUpload');
        $uploads_directory = $this->getParameter('uploads_directory');
        $filename = $file->getClientoriginalName();
        $file->move($uploads_directory, $filename);
        $listestage->setCV($filename);
        $validator = $this->get('validator');
        $listErrors = $validator->validate($listestage);

        if (count($listErrors) > 0) {
            return new Response((string)$listErrors);
        } else {
            //var_dump($files);exit;
            $em->persist($listestage);
            $em->flush();
            return $this->redirectToRoute("consulterliste");
        }

        return $this->render('StageBundle:Front:postuler.html.twig', array('stage' => $stage));
    }


    /**
     * @Route("/liste/demande", name="listedemandestage")
     */
    public function listeDemandeAction(Request $request)
    {
        $user = $this->getUser();

        if ($user and $user->hasRole(User::ROLE_SUPER_ADMIN)) {
            $listestages = $this->getDoctrine()->getRepository(Listestage::class)->findAll();

            return $this->render('StageBundle:Back:listeDemande.html.twig', array('listestages' => $listestages));
        } else
            throw $this->createAccessDeniedException($user . 'has no access to this page');
    }

    /**
     * @Route("/traiter/demande/{id}" , name="traiterdemande")
     */
    public function TraiterAction($id, Request $request)
    {
        $user = $this->getUser();

        if ($user and $user->hasRole(User::ROLE_SUPER_ADMIN)) {
            $em = $this->getDoctrine()->getManager();
            $demande = $em->getRepository(Listestage::class)->find($id);
            // return $this->redirectToRoute("consulterliste");
            return $this->render('StageBundle:Back:traiter.html.twig', array('demande' => $demande));

        } else
            throw $this->createAccessDeniedException($user . 'has no access to this page');
    }

    /**
     * @Route("/delete/listestage/{id}", name="deletelistestage")
     */

    public function deleteAction($id)
    {
        $user = $this->getUser();

        if ($user and $user->hasRole(User::ROLE_SUPER_ADMIN)) {
            $em = $this->getDoctrine()->getManager();
            $listestage = $em->getRepository(Listestage::class)->find($id);
            $em->remove($listestage);
            $em->flush();
            return $this->redirectToRoute("listedemandestage");
        } else
            throw $this->createAccessDeniedException($user . 'has no access to this page');
    }
}
