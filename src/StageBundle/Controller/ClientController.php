<?php

namespace StageBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use StageBundle\Entity\Stage;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ClientController extends Controller
{
    /**
     * @Route("/consulter/liste" , name="consulterliste")
     */
    public function ConsulterAction(Request $request)
    {
        $stages = $this->getDoctrine()->getRepository(Stage::class)->findAll();
        return $this->render('StageBundle:Front:listeClient.html.twig', ['stages' => $stages]);
    }

    /**
     * @Route("/postuler/stage/{id}" , name="postulerstage")
     */
    public function PostulerAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $stage = $em->getRepository(Stage::class)->find($id);
        //return $this->redirectToRoute("consulterliste");
        return $this->render('StageBundle:Front:postuler.html.twig', array('stage' => $stage));


    }
}
