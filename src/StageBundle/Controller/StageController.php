<?php

namespace StageBundle\Controller;


use FOS\UserBundle\Model\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use StageBundle\Entity\Listestage;
use StageBundle\Entity\Stage;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class StageController extends Controller
{
    /**
     * @Route("/add/stage", name="createnewstage")
     */
    public function AddStageAction(Request $request)
    {
        $user = $this->getUser();

        if ($user and $user->hasRole(User::ROLE_SUPER_ADMIN)) {
            $em = $this->getDoctrine()->getManager();
            $stage = new Stage();


            if ($request->isMethod("POST")) {
                $stage->setSujet($request->get('sujet'));
                $stage->setDescription($request->get('description'));
                $stage->setBranche($request->get('branche'));
                $validator = $this->get('validator');
                $listErrors = $validator->validate($stage);


                if (count($listErrors) > 0) {

                    return new Response((string)$listErrors);
                } else {
                    $em->persist($stage);
                    $em->flush();
                    return $this->redirectToRoute("listestage");

                }
            }
            return $this->render('StageBundle:Back:ajout.html.twig');
        } else
            throw $this->createAccessDeniedException($user . 'has no access to this page');

    }

    /**
     * @Route("/liste", name="listestage")
     */
    public function listeAction(Request $request)
    {
        $user = $this->getUser();

        if ($user and $user->hasRole(User::ROLE_SUPER_ADMIN)) {
            $stages = $this->getDoctrine()->getRepository(Stage::class)->findAll();
            $listestages = $this->getDoctrine()->getRepository(Listestage::class)->findAll();
            $stagess = $this->get('knp_paginator')->paginate($stages, $request->query->get('page', 1), 3);
            return $this->render('StageBundle:Back:liste.html.twig', array('stages' => $stages, "stages" => $stagess, 'listestage' => $listestages));
        } else
            throw $this->createAccessDeniedException($user . 'has no access to this page');
    }

    /**
     * @Route("/delete/stage/{id}", name="deletestage")
     */

    public function deleteAction($id)
    {
        $user = $this->getUser();

        if ($user and $user->hasRole(User::ROLE_SUPER_ADMIN)) {

            $em = $this->getDoctrine()->getManager();
            $stage = $em->getRepository(Stage::class)->find($id);
            $em->remove($stage);
            $em->flush();
            return $this->redirectToRoute("listestage");
        } else
            throw $this->createAccessDeniedException($user . 'has no access to this page');

    }

    /**
     * @Route("/update/stage/{id}", name="updatestage")
     */
    public function updateAction(Request $request, $id)
    {
        $user = $this->getUser();

        if ($user and $user->hasRole(User::ROLE_SUPER_ADMIN)) {
            $em = $this->getDoctrine()->getManager();
            $stage = $em->getRepository(Stage::class)->find($id);
            if ($request->isMethod('POST')) {
                $stage->setSujet($request->get('sujet'));
                $stage->setDescription($request->get('description'));
                $stage->setBranche($request->get('branche'));

                $em->flush();
                return $this->redirectToRoute('listestage');
            }
            return $this->render('StageBundle:Back:update.html.twig', array('stage' => $stage));
        } else
            throw $this->createAccessDeniedException($user . 'has no access to this page');
    }

    /**
     * @Route("/stage/business", name="businessbranche")
     */
    public Function businessbrancheAction()
    {
        $user = $this->getUser();

        if ($user and $user->hasRole(User::ROLE_SUPER_ADMIN)) {
            $listestages = $this->getDoctrine()->getRepository(Listestage::class)->findAll();
            $repository = $this->getDoctrine()->getManager()->getRepository(Stage::class);
            $business = $repository->BusinessBranche();
            return ($this->render('@Stage/Recherche/business.html.twig', array('business' => $business, 'listestage' => $listestages)));
        } else
            throw $this->createAccessDeniedException($user . 'has no access to this page');

    }

    /**
     * @Route("/stage/GL", name="genielogicielbranche")
     */
    public Function genielogicielbrancheAction()

    {
        $user = $this->getUser();

        if ($user and $user->hasRole(User::ROLE_SUPER_ADMIN)) {
            $listestages = $this->getDoctrine()->getRepository(Listestage::class)->findAll();
            $repository = $this->getDoctrine()->getManager()->getRepository(Stage::class);
            $branche = $repository->GenielogicielBranche();
            return ($this->render('@Stage/Recherche/gl.html.twig', array('branche' => $branche, 'listestage' => $listestages)));
        } else
            throw $this->createAccessDeniedException($user . 'has no access to this page');
    }

    /**
     * @Route("/stage/Info", name="Infobranche")
     */
    public Function infobrancheAction()

    {
        $user = $this->getUser();

        if ($user and $user->hasRole(User::ROLE_SUPER_ADMIN)) {
            $listestages = $this->getDoctrine()->getRepository(Listestage::class)->findAll();
            $repository = $this->getDoctrine()->getManager()->getRepository(Stage::class);
            $branche = $repository->InfoBranche();
            return ($this->render('@Stage/Recherche/info.html.twig', array('branche' => $branche, 'listestage' => $listestages)));
        } else
            throw $this->createAccessDeniedException($user . 'has no access to this page');
    }

    /**
     * @Route("/stage/DataScience", name="Datasciencebranche")
     */
    public Function DataSAction()
    {
        $user = $this->getUser();

        if ($user and $user->hasRole(User::ROLE_SUPER_ADMIN)) {
            $listestages = $this->getDoctrine()->getRepository(Listestage::class)->findAll();
            $repository = $this->getDoctrine()->getManager()->getRepository(Stage::class);
            $branche = $repository->DataScienceBranche();
            return ($this->render('@Stage/Recherche/datas.html.twig', array('branche' => $branche, 'listestage' => $listestages)));
        } else
            throw $this->createAccessDeniedException($user . 'has no access to this page');
    }

    /**
     * @Route("/stage/recherchsujet", name="recherchsujet" , options={"expose"=true})
     */
    public function recherchbrancheAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $liststage = $em->getRepository(Stage::class)->findbysujet($request->get('value'));

        return $this->render("StageBundle:Recherche:recherchbranche.html.twig", ['liststage' => $liststage, "fstages" => $request->get('stages'), "stages" => $request->get('stagess')]);

    }

    /**
     * @Route("/stage/stageswithfiltre", name="stageswithfiltre")
     */
    public function stagefiltreAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $stage = $em->getRepository(Stage::class)->find($request->get('ids'));
        $stages = $this->getDoctrine()->getRepository(Stage::class)->findbysujet($request->get('stages'));
        return $this->render('StageBundle:Front:stageswithfiltre.html.twig', array('stages' => $stages, 'stage' => $stage));
    }


}
