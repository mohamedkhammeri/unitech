<?php


namespace ApiBundle\Controller;


use Faker\Factory;
use ReclamationBundle\Entity\Reclamation;
use ReclamationBundle\Entity\ReclamationNote;
use ReclamationBundle\Entity\ReclamationProf;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\JsonSerializableNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ReclamationApiController extends Controller
{

    /**
     * @Route("/reclamation/{id}", methods={"GET"})
     */
    public function indexAction(Request $request, Reclamation $id)
    {
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(2);
        $normalizer->setIgnoredAttributes(['dateCreation', 'etudiant']);
        $ser = new Serializer([$normalizer]);
        return new JsonResponse($ser->normalize($id));
    }

    /**
     * @Route("/reclamations", methods={"GET"})
     */
    public function allAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $recRepo = $em->getRepository("ReclamationBundle:Reclamation");
        $etudRepo = $em->getRepository("ClasseBundle:Etudiant");
        $recs = [];
        if (trim($request->get("userCnt")) != "") {
            $etud = $etudRepo->find(trim($request->get("userCnt")));
            if ($etud)
                $recs = $recRepo->findBy(["etudiant" => $etud]);
        }

        $ser = new Serializer([new JsonSerializableNormalizer()]);
        return new JsonResponse($ser->normalize($recs));
    }

    /**
     * @Route("/reclamations/countResolu", methods={"GET"})
     */
    public function countReclamationsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $recRepo = $em->getRepository("ReclamationBundle:Reclamation");
        $etudRepo = $em->getRepository("ClasseBundle:Etudiant");
        $recs = [];

        if (trim($request->get("userCnt")) != "") {
            $etud = $etudRepo->find(trim($request->get("userCnt")));
            if ($etud)
                $recs = $recRepo->findBy([
                    "etat" => 2, "etudiant" => $etud
                ]);
        }

        return new JsonResponse(count($recs));
    }

    /**
     * @Route("/reclamation", methods={"DELETE"})
     */
    public function deleteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $reclamationRepo = $em->getRepository("ReclamationBundle:Reclamation");
        $reclamation = $reclamationRepo->find($request->get("id"));
        if ($reclamation) {
            $em->remove($reclamation);
            $em->flush();
            return new JsonResponse("{status: 'deleted'}", Response::HTTP_OK);
        } else
            return new JsonResponse(null, 404);
    }

    /**
     * @Route("/reclamation", methods={"PUT"})
     */
    public function updateAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $reclamationRepo = $em->getRepository("ReclamationBundle:Reclamation");
        $reclamation = $reclamationRepo->find($request->get("id"));
        if ($reclamation) {
            $reclamation->setSujet($request->get("sujet"));
            $reclamation->setDescription($request->get("desc"));
            $em->flush();
            return new JsonResponse("{status: 'updated'}", Response::HTTP_OK);
        } else
            return new JsonResponse(null, 404);
    }

    /**
     * @Route("/reclamation", methods={"POST"})
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $etudiantRepo = $em->getRepository("ClasseBundle:Etudiant");
        $profRepo = $em->getRepository("ClasseBundle:Enseignant");
        $noteRepo = $em->getRepository("NoteBundle:Note");
        $etudiant = $etudiantRepo->find($request->get("user"));

        if ($etudiant == null)
            return new JsonResponse(null, 404);

        $reclamation = null;
        if ($request->get("type") == "enseignant") {
            $reclamation = new ReclamationProf();
            $reclamation->setProf($profRepo->find($request->get("prof")));
        } else if ($request->get("type") == "note") {
            $reclamation = new ReclamationNote();
            $reclamation->setNote($noteRepo->find($request->get("note")));
            $reclamation->setTypeNote($request->get("type_note"));
        } else
            $reclamation = new Reclamation();

        $reclamation->setSujet($request->get("sujet"));
        $reclamation->setDescription($request->get("desc"));
        //$reclamation->setEtudiant($this->getUser());
        $reclamation->setEtudiant($etudiant);

        $em->persist($reclamation);
        $em->flush();
        $reclamation->setCodeSuivie(Factory::create()
            ->unique(true)
            ->numerify($reclamation->getId() . '###')
        );
        $em->persist($reclamation);
        $em->flush();

        $ser = new Serializer([new JsonSerializableNormalizer()]);
        return new JsonResponse($ser->normalize($reclamation));
    }

    /**
     * @Route("/reclamation/search/{codeSuivie}", methods={"GET"})
     */
    public function searchCodeAction(Request $request, $codeSuivie)
    {
        $em = $this->getDoctrine()->getManager();
        $reclamation = $em->getRepository("ReclamationBundle:Reclamation")->findOneBy([
            "codeSuivie" => $codeSuivie
        ]);
        $ser = new Serializer([new JsonSerializableNormalizer()]);
        return new JsonResponse($ser->normalize($reclamation));
    }
}