<?php

namespace ApiBundle\Controller;

use ClasseBundle\Entity\Enseignant;
use ClasseBundle\Entity\Etudiant;
use Mgilet\NotificationBundle\Manager\NotificationManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Entity\User;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\JsonSerializableNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;


class UsermobileController extends Controller
{
    /**
     * @Route("/find/user", name="finduser")
     */
    public function userAction(Request $request)
    {
        //$users=$this->getDoctrine()->getManager()->getRepository('AppBundle:User')->findByRoles('ROLE_CLIENT');
        //$user = new User();
        $user = $this->getUser();
        if ($user and $user->hasRole(\FOS\UserBundle\Model\User::ROLE_SUPER_ADMIN)) {
            $users = $this->getDoctrine()->getManager()->getRepository('ClasseBundle:Classe')->findAll();
            //  $liststage = $this->getDoctrine()->getManager()->getRepository('ReclamationBundle:ReclamationProf')->findAll();
            //$liststage = $this->getDoctrine()->getRepository('StageBundle:Listestage')->findAll();
        } else if ($user and $user->hasRole(User::ROLE_ENSEIGNANT)) {
            $users = $this->getDoctrine()->getManager()->getRepository('ClasseBundle:Enseignant')->findAll();
        } elseif ($user and $user->hasRole(User::ROLE_ETUDIANT)) {
            $users = $this->getDoctrine()->getManager()->getRepository("ClasseBundle:Etudiant")->findAll();
        }
        $ser = new Serializer([new JsonSerializableNormalizer()]);
        return new JsonResponse($ser->normalize($users));
    }

    /**
     * @Route("/userlogin/{email}/{password}",name="userLog")
     */
    public function userloginAction($email, $password)
    {
        $em = $this->getDoctrine()->getManager();
        $userRepo = $em->getRepository("AppBundle:User");
        $user = $userRepo->findOneBy([
            "email" => $email
        ]);

        if ($user && password_verify($password, $user->getPassword())) {
            $normalizer = new ObjectNormalizer();
            $normalizer->setCircularReferenceLimit(1);
            $normalizer->setIgnoredAttributes(["dateCreation", "notes", "reclamations", "lastLogin", "classe", "matieres", "password", "salt",
                "emailCanonical", "plainPassword", "confirmationToken", "roles", "accountNonLocked", "accountNonExpired",
                "credentialsNonExpired", "usernameCanonical", "enabled", "groupNames", "groups", "passwordRequestedAt"]);
            $normalizer->setCircularReferenceHandler(function ($object) {
                if (method_exists($object, 'getId')) {
                    return $object->getId();
                }
            });
            $serializer = new Serializer([$normalizer]);
            $formatted = $serializer->normalize($user);
            return new JsonResponse($formatted);
        }
        return new JsonResponse(null);
    }

    /**
     * @Route("/add/user", name="add_user")
     */
    public function AdduserAction(Request $request)
    {
        $user = null;
        $role_selected = ($request->get('roles') == "client" ? "ROLE_ENSEIGNANT" : "ROLE_STUDENT");
        if ($role_selected == "ROLE_STUDENT")
            $user = new Etudiant();
        else
            $user = new Enseignant();

        $user->setUsername($request->get('username'));
        $user->setUsernameCanonical($request->get('username_cononcial'));
        $user->setEmail($request->get('email'));
        $user->setEmailCanonical($request->get('email_cononcial'));
        $user->setEnabled(true);
        $user->setPlainPassword($request->get('password'));
        $user->setLastLogin($request->get('last_login'));
        $user->setRoles([$role_selected]);
        //$user->setSuperAdmin();
        //$user->setType($role_selected);
        $user->setFirstName($request->get('first_name'));
        $user->setLastName($request->get('last_name'));
        $user->setPhoneNumber($request->get('phone_number'));
        $user->setCIN($request->get('CIN'));
        // $user->setType($request->get('dtype'));
        $this->getDoctrine()->getManager()->persist($user);
        $this->getDoctrine()->getManager()->flush();
        $ser = new Serializer([new JsonSerializableNormalizer()]);
        return new JsonResponse($ser->normalize($user));
    }
}
