<?php

namespace ApiBundle\Controller;

use QABundle\Entity\Sujet;
use QABundle\Entity\Commentaire;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;

class RPMobileController extends Controller
{
    /**
     * @Route("/affichereponseMobile/{id}/", name="affichereponseMobile")
     */
    public function affichereponseMobileAction($id)
    {

        $comments = $this->getDoctrine()->getRepository(Commentaire::class)->findBy(
            array('idF' => $id));
        $forum = $this->getDoctrine()->getRepository(Sujet::class)->findBy(
            array('idF' => $id));

        $serializer = new Serializer([new DateTimeNormalizer(), new ObjectNormalizer()]);
        $formatted = $serializer->normalize(
            array('question' => $forum, 'comments' => $comments, 'username' => get_current_user())
        );
        return new JsonResponse($formatted);

    }

    /**
     * @Route("/deletereponseMobile/{id}/", name="deletereponseMobile")
     */

    public function deletereponseAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $comments = $em->getRepository(Commentaire::class)->find($id);
        $em->remove($comments);
        $em->flush();
        $serializer = new Serializer([new DateTimeNormalizer(), new ObjectNormalizer()]);
        $formatted = $serializer->normalize($comments);
        return new JsonResponse($formatted);
    }

    /**
     * @Route("/ajouteReponse", name="ajouteReponse")
     */

    public function ajouteReponseAction(Request $request)
    {


        $em = $this->getDoctrine()->getManager();
        $Commentaire = new Commentaire();

        $Commentaire->setDescriptionCom($request->get('descriptionCom'));
        $Commentaire->setDateCom(new \DateTime());
        //  $reponse->setIdQuestion($request->get('idQuestion'));
        $Commentaire->setIdUser($request->get('id'));

        $question = $em->getRepository("QABundle:Sujet")->find($request->query->get("idF"));
        //->getIdF());
        $Commentaire->setIdF($request->get('idF'));

        $em->persist($Commentaire);
        $em->flush();
        $serializer = new Serializer([new DateTimeNormalizer(), new ObjectNormalizer()]);
        $formatted = $serializer->normalize([$Commentaire]);
        return new JsonResponse($formatted);

    }


    /**
     * @Route("/modifierreponseMobile/{idrep}/", name="modifierreponseMobile")
     */

    public function modifierreponseAction(Request $request, $idrep)
    {
        $reponse = $this->getDoctrine()->getRepository(
            Commentaire::class)->find($idrep);

        $em = $this->getDoctrine()->getManager();

        $reponse->setDescriptionCom($request->get('descriptionCom'));
        $reponse->setDateCom(new \DateTime());

        $reponse->setIdF($request->get('idF'));


        $em->flush();
        $serializer = new Serializer([new DateTimeNormalizer(), new ObjectNormalizer()]);
        $formatted = $serializer->normalize([$reponse]);
        return new JsonResponse($formatted);
    }


}