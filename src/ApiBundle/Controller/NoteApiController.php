<?php

namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\JsonSerializableNormalizer;
use Symfony\Component\Serializer\Serializer;

class NoteApiController extends Controller
{

    /**
     * @Route("/notes", methods={"GET"})
     */
    public function allAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $etudiantRepo = $em->getRepository("ClasseBundle:Etudiant");

        $etudiant = $etudiantRepo->find($request->get("etudiant"));
        //check
        if ($etudiant == null)
            return new JsonResponse(null, 404);

        $ens = $em->getRepository("NoteBundle:Note")->findBy([
            'user' => $etudiant
        ]);

        $ser = new Serializer([new JsonSerializableNormalizer()]);
        return new JsonResponse($ser->normalize($ens));
    }


    /**
     * @Route("/affiche", methods={"GET"})
     */
    public function afficheAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $recs = $em->getRepository("NoteBundle:Note")->findAll();
        $ser = new Serializer([new JsonSerializableNormalizer()]);
        return new JsonResponse($ser->normalize($recs));
    }

    /**
     * @Route("/affichecin", methods={"GET"})
     */
    public function affichecinAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $recs = $em->getRepository("ClasseBundle:Etudiant")->findAll();
        $ser = new Serializer([new JsonSerializableNormalizer()]);
        return new JsonResponse($ser->normalize($recs));
    }

    /**
     * @Route("/affichematiere", methods={"GET"})
     */
    public function affichematiereAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $recs = $em->getRepository("NoteBundle:Matiere")->findAll();
        $ser = new Serializer([new JsonSerializableNormalizer()]);
        return new JsonResponse($ser->normalize($recs));
    }

    /**
     *
     * @Route("/ajoutermobile",name="Note_ajoutermobile")
     */

    public function ajouternotemobileAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $etudiantRepo = $em->getRepository("ClasseBundle:Etudiant");
        $etudiantclasse = $em->getRepository("ClasseBundle:Classe");
        $matiereRepo = $em->getRepository("NoteBundle:Matiere");
        $note = new Note();
        $note->setNoteCc($request->get('note_cc'));
        $note->setNoteDs($request->get('note_ds'));
        $note->setNoteExam($request->get('note_exam'));
        $note->setUser($etudiantRepo->find($request->get('user')));
        $note->setClasse($etudiantclasse->find($request->get('classe')));
        $note->setMatiere($matiereRepo->find($request->get('mat')));
        //$note->setMatiere($matiereRepo->find($request->get('matiere')));
        $note->setMoyenne($note->getMoyenne());
        $em->persist($note);
        $em->flush();
        $ser = new Serializer([new JsonSerializableNormalizer()]);
        return new JsonResponse($ser->normalize($note));

    }


    /**
     *
     * @Route("/updatemobile/{id}",name="Note_updatermobile")
     */
    public function updatemobileAction(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();
        $note = $em->getRepository(Note::class)->find($id);
        $note->setNoteCc($request->get('note_cc'));
        $note->setNoteDs($request->get('note_ds'));
        $note->setNoteExam($request->get('note_exam'));
        $note->setMoyenne($note->getMoyenne());
        //$em->persist($note);
        $em->flush();
        $ser = new Serializer([new JsonSerializableNormalizer()]);
        return new JsonResponse($ser->normalize($note));

    }

    /**
     *
     * @Route("/recherche",name="Note_recherche")
     */
    public function rechercheAction()
    {
        $note = new note();
        $note = $this->getDoctrine()->getManager()
            ->getRepository('NoteBundle:Note')
            ->FindBy(array('user' => $note->getUser()));
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($note);
        return new JsonResponse($formatted);
    }
}
