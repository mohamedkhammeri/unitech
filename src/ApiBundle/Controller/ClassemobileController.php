<?php

namespace ApiBundle\Controller;

use ClasseBundle\Entity\Classe;
use Mgilet\NotificationBundle\Manager\NotificationManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Entity\User;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\JsonSerializableNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;


class ClassemobileController extends Controller
{

    /**
     * @Route("/ttclasses", methods={"GET"})
     */
    public function allClassesAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $recs = $em->getRepository("ClasseBundle:Classe")->findAll();

        $ser = new Serializer([new JsonSerializableNormalizer()]);
        return new JsonResponse($ser->normalize($recs));
    }


    /**
     * @Route("/ttens", methods={"GET"})
     */
    public function allEnssAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $recs = $em->getRepository("ClasseBundle:Enseignant")->findAll();

        $ser = new Serializer([new JsonSerializableNormalizer()]);
        return new JsonResponse($ser->normalize($recs));
    }

    /**
     * @Route("/ttet/{id}", methods={"GET"})
     */
    public function AllEtAction(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();
        $classe = $em->getRepository("ClasseBundle:Classe")->find($id);
        $etudiants = $em->getRepository("ClasseBundle:Etudiant")->findByClasse(['classe' => $classe]);
        $ser = new Serializer([new JsonSerializableNormalizer()]);
        return new JsonResponse($ser->normalize($etudiants));
    }

    /**
     * @Route("/all/etud",name="etudall")
     */
    public function readAction()
    {
        $em = $this->getDoctrine()->getManager();
        $recs = $em->getRepository("ClasseBundle:Etudiant")->findAll();

        $ser = new Serializer([new JsonSerializableNormalizer()]);
        return new JsonResponse($ser->normalize($recs));
    }


}
