<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\User;
use Doctrine\Common\Annotations\AnnotationReader;
use StageBundle\Entity\Listestage;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\JsonSerializableNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class StageApiController extends Controller
{
    /**
     * @Route("/consulter/stage",name="consultermobile")
     */
    public function readAction()
    {
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(1);
        $normalizer->setCircularReferenceHandler(function ($object) {
            if (method_exists($object, 'getId')) {
                return $object->getId();
            }
        });
        $normalizer->setIgnoredAttributes(["idUser"]);
        $liststage = $this->getDoctrine()->getManager()->getRepository('StageBundle:Stage')->findAll();
        $serializer = new Serializer([$normalizer], [new JsonEncoder()]);
        $formatted = $serializer->normalize($liststage);
        return new JsonResponse($formatted);
    }

    /**
     * @Route("/postuler/stagee/{id}",name="postulermobile")
     */
    public function postulerAction($id)
    {
        $liststage = $this->getDoctrine()->getManager()->getRepository('StageBundle:Stage')->find($id);
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($liststage);
        return new JsonResponse($formatted);
    }

    /**
     * @Route("/envoyer/stage",name="envoyermobile")
     */
    public function demandeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $demande = new Listestage();
        $demande->setSujet($request->get("sujet"));
        $demande->setDescription($request->get("description"));
        $demande->setBranche($request->get("branche"));

        $demande->setLettreMotivation($request->get("LettreMotivation"));
        $demande->setCV($request->get("CV"));

        $demande->setAdresse($request->get("adresse"));
        $em->persist($demande);
        $em->flush();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($demande);
        return new JsonResponse($formatted);
    }


    /**
     * @Route("/find/user", name="finduser")
     */
    public function userAction(Request $request)
    {
        //$users=$this->getDoctrine()->getManager()->getRepository('AppBundle:User')->findByRoles('ROLE_CLIENT');
        //$user = new User();
        $user = $this->getUser();
        if ($user and $user->hasRole(\FOS\UserBundle\Model\User::ROLE_SUPER_ADMIN)) {
            $liststage = $this->getDoctrine()->getManager()->getRepository('StageBundle:Stage')->findAll();
            //  $liststage = $this->getDoctrine()->getManager()->getRepository('ReclamationBundle:ReclamationProf')->findAll();
            // $liststage = $this->getDoctrine()->getRepository('StageBundle:Listestage')->findByBranche('Business');

        } else if ($user and $user->hasRole(\FOS\UserBundle\Model\User::ROLE_ENSEIGNANT)) {
            $liststage = $this->getDoctrine()->getManager()->getRepository('StageBundle:Listestage')->findAll();
        } elseif ($user and $user->hasRole(\FOS\UserBundle\Model\User::ROLE_ETUDIANT)) {
            $liststage = $this->getDoctrine()->getManager()->getRepository('StageBundle:Listestage')->findAll();
        }

        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($liststage);
        return new JsonResponse($formatted);
    }

    /**
     * @Route("/add/user", name="add_user")
     */
    public function AdduserAction(Request $request)
    {

        $user = new User();
        $user->setUsername($request->get('username'));
        $user->setUsernameCanonical($request->get('username_cononcial'));
        $user->setEmail($request->get('email'));
        $user->setEmailCanonical($request->get('email_cononcial'));
        $user->setEnabled(true);
        $user->setSalt($request->get('salt'));
        $user->setPassword($request->get('password'));
        //$user->setPlainPassword($request->get('pwd'));
        $user->setLastLogin($request->get('last_login'));
        $user->setConfirmationToken($request->get('confirmation_token'));
        $user->setPasswordRequestedAt();

        $role_selected = ($request->get('roles') == "client" ? 'ROLE_ETUDIANT' : "ROLE_ENSEIGNANT");
        $user->setRoles([$role_selected]);
        //$user->setSuperAdmin();
        //$user->setType($role_selected);
        $user->setFirstName($request->get('first_name'));
        $user->setLastName($request->get('last_name'));
        $user->setPhoneNumber($request->get('phone_number'));
        $user->setCIN($request->get('CIN'));
        //$user->setType($request->get('dtype'));
        $this->getDoctrine()->getManager()->persist($user);
        $this->getDoctrine()->getManager()->flush();


        $ser = new Serializer([new JsonSerializableNormalizer()]);
        return new JsonResponse($ser->normalize($user));
    }

}
