<?php

namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use QABundle\Entity\Commentaire;
use QABundle\Entity\Rating;
use QABundle\Entity\Sujet;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;


class FMobileController extends Controller
{

    /**
     * @Route("/listeQuestionMobile")
     * @param Request $request
     * @return JsonResponse
     */
    public function indexmAction(Request $request)
    {
        $user = $this->etudiant = $this->get('security.token_storage')->getToken()->getUser();

        $em = $this->getDoctrine()->getManager();

        $RAW_QUERY = "SELECT * FROM sujet WHERE id_user !='$user' ORDER BY date DESC";

        $statement = $em->getConnection()->prepare($RAW_QUERY);
        $statement->execute();

        $forums = $statement->fetchAll();

        $serializer = new Serializer([new DateTimeNormalizer(), new ObjectNormalizer()]);
        $formatted = $serializer->normalize(['forums' => $forums, 'username' => get_current_user()]);
        return new JsonResponse($formatted);
    }

    /**
     * @Route("/ajoutQuestionMobile", name="ajoutQuestionMobile")
     */
    public function addmAction(Request $request)
    {
        //$em = $this->getDoctrine()->getManager();
        $user = $this->etudiant = $this->get('security.token_storage')->getToken()->getUser();

        // var_dump($user);
        $sujet = new Sujet();
        $em = $this->gaffichereponseMobileetDoctrine()->getManager();
        $sujet->setTitreF($request->get('titreF'));
        $sujet->setDescriptionF($request->get('descriptionF'));
        $sujet->setDate(new \DateTime("now"));
        $sujet->setEtat(0);
        $sujet->setNbreJaime(0);
        $sujet->setIdUser($request->get('id'));
        //   var_dump($time);
        $em->persist($sujet);
        $em->flush();

        $serializer = new Serializer([new DateTimeNormalizer(), new ObjectNormalizer()]);
        $formatted = $serializer->normalize([$sujet]);
        return new JsonResponse($formatted);
    }


    /**
     * @Route("/updatequestion/{id}/", name="updatequestion")
     */
    public function modifierquestionAction(Request $request, $id)
    {
        $user = $this->etudiant = $this->get('security.token_storage')->getToken()->getUser();

        $forum = $this->getDoctrine()->getRepository('QABundle:Sujet')->find($id);
        //prepare the form with the function: createForm()
        $form = $this->createFormBuilder($forum)
            ->add('descriptionF', TextType::class, array('attr' => array('class' => 'form-control')))
            ->add('save', SubmitType::class, array('label' => 'save'))
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            //create an entity manager object
            $em = $this->getDoctrine()->getManager();
            //update the data base with flush
            $em->flush();
            $url = $this->generateUrl('mesForums');

            return $this->redirect($url);
        }
        return $this->render('forum/modifier.html.twig', array(
            'forum' => $forum,
            'user' => $user,

            'form' => $form->createView()
        ));
    }


    /**
     * @Route("/deletequestionMobile/{id}/", name="deletequestionMobile")
     */

    public function deletequestionMobileAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $forum = $em->getRepository('QABundle:Sujet')->find($id);
        $em->remove($forum);
        $em->flush();

        $serializer = new Serializer([new DateTimeNormalizer(), new ObjectNormalizer()]);
        $formatted = $serializer->normalize($forum);
        return new JsonResponse($formatted);
    }

}
