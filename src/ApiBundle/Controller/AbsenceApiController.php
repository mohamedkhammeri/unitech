<?php

namespace ApiBundle\Controller;

use AbsenceBundle\Entity\absence;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\JsonSerializableNormalizer;
use Symfony\Component\Serializer\Serializer;

class AbsenceApiController extends Controller
{
    /**
     * @Route("/all")
     * @param Request $request
     * @return JsonResponse
     */
    public function allAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $recs = $em->getRepository("AbsenceBundle:absence")->findAll();
        $ser = new Serializer([new JsonSerializableNormalizer()]);
        return new JsonResponse($ser->normalize($recs));
    }
    /* public function alluserAction()
     {

         $posts = $this->getDoctrine()->getManager()
             ->getRepository('AppBundle:User')
             ->findAll();
         $serializer = new Serializer([new ObjectNormalizer()]);
         $formatted = $serializer->normalize($posts);
         return new JsonResponse($formatted);
     }*/
//nzid use zaam as2il omik

    /**
     * @Route("/affichecin")
     * @return JsonResponse
     */
    public function affichecinAction()
    {

        $em = $this->getDoctrine()->getManager();
        $recs = $em->getRepository("AppBundle:User")->findAll();
        $ser = new Serializer([new JsonSerializableNormalizer()]);
        return new JsonResponse($ser->normalize($recs));
    }

    /**
     * @Route("/afficheMat")
     * @return JsonResponse
     */
    public function afficheMatAction()
    {

        $em = $this->getDoctrine()->getManager();
        $recs = $em->getRepository("NoteBundle:Matiere")->findAll();
        $ser = new Serializer([new JsonSerializableNormalizer()]);
        return new JsonResponse($ser->normalize($recs));
    }

    /**
     * @Route("/afficheSc")
     * @return JsonResponse
     */
    public function afficheScAction()
    {

        $em = $this->getDoctrine()->getManager();
        $recs = $em->getRepository("AbsenceBundle:sceance")->findAll();
        $ser = new Serializer([new JsonSerializableNormalizer()]);
        return new JsonResponse($ser->normalize($recs));
    }

    /**
     * @Route("/afficheClasse")
     * @return JsonResponse
     */
    public function afficheClasseAction()
    {

        $em = $this->getDoctrine()->getManager();
        $recs = $em->getRepository("ClasseBundle:Classe")->findAll();
        $ser = new Serializer([new JsonSerializableNormalizer()]);
        return new JsonResponse($ser->normalize($recs));
    }

    /**
     * @Route("/ajouterabsencemobile")
     * @param Request $request
     * @return JsonResponse
     */
    public function ajouterabsencemobileAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $etudiantRepo = $em->getRepository("AppBundle:User");
        $etudiantclasse = $em->getRepository("ClasseBundle:Classe");
        $etudiantseance = $em->getRepository("AbsenceBundle:sceance");
        $matiereRepo = $em->getRepository("NoteBundle:Matiere");
        $absence = new absence();
        $absence->setAbsence($request->get('abs'));
        $absence->setUser($etudiantRepo->find($request->get('user')));
        $absence->setClasse($etudiantclasse->find($request->get('classe')));
        $absence->setSceance($etudiantseance->find($request->get('sceance')));
        $absence->setMatiere($matiereRepo->find($request->get('mat')));
        //$absence->setIdClasse($request->get('classe'));
        // $absence->setIdUser($request->get('user'));

        $em->persist($absence);
        $em->flush();
        $ser = new Serializer([new JsonSerializableNormalizer()]);
        return new JsonResponse($ser->normalize($absence));

    }

    /**
     * @Route("/Takematiere/{nom_mat}/")
     * @param $nom_mat
     * @return JsonResponse
     */
    public function TakematiereAction($nom_mat)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery("
            SELECT c.id FROM NoteBundle:Matiere c where c.nom_mat= $nom_mat  ");
        $user = $query->getResult();
        $ser = new Serializer([new JsonSerializableNormalizer()]);
        return new JsonResponse($ser->normalize($user));
    }

    /**
     * @Route("/TakeSeance/{num}/")
     * @param $num
     * @return JsonResponse
     */
    public function TakeSeanceAction($num)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery("
            SELECT c.id_seance FROM AbsenceBundle:sceance  c where c.num=$num ");
        $user = $query->getResult();
        $ser = new Serializer([new JsonSerializableNormalizer()]);
        return new JsonResponse($ser->normalize($user));
    }

    /**
     * @Route("/Takeclasse/{num_classe}/")
     * @param $num_classe
     * @return JsonResponse
     */
    public function TakeclasseAction($num_classe)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery("
            SELECT c.id FROM ClasseBundle:Classe  c where c.num_classe=$num_classe ");
        $user = $query->getResult();
        $ser = new Serializer([new JsonSerializableNormalizer()]);
        return new JsonResponse($ser->normalize($user));
    }

    /**
     * @Route("/TakeId/{CIN}/")
     * @param $CIN
     * @return JsonResponse
     */
    public function TakeIdAction($CIN)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery("
            SELECT c.id FROM AppBundle:User c where c.CIN=$CIN ");
        $user = $query->getResult();

        $ser = new Serializer([new JsonSerializableNormalizer()]);
        return new JsonResponse($ser->normalize($user));
    }

    /**
     * @Route("/updatemobileabs/{id}")
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function updatemobileabsAction(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();
        $absence = $em->getRepository(absence::class)->find($id);
        $absence->setAbsence($request->get('absence'));
        $em->flush();
        $ser = new Serializer([new JsonSerializableNormalizer()]);
        return new JsonResponse($ser->normalize($absence));

    }

    /**
     * @Route("/deleteM/{id}")
     * @param $id
     * @return JsonResponse
     */
    public function deleteMAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $absence = $em->getRepository(absence::class)->find($id);
        $em->remove($absence);
        $em->flush();
        $ser = new Serializer([new JsonSerializableNormalizer()]);
        return new JsonResponse($ser->normalize($absence));
    }

}
