<?php

namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\JsonSerializableNormalizer;
use Symfony\Component\Serializer\Serializer;

class EnseignantApiController extends Controller
{

    /**
     * @Route("/enseignants", methods={"GET"})
     */
    public function allAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $ens = $em->getRepository("ClasseBundle:Enseignant")->findAll();

        $ser = new Serializer([new JsonSerializableNormalizer()]);
        return new JsonResponse($ser->normalize($ens));
    }
}
